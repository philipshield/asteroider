Rymdstenar is a 2D physics-based revamp of Asteroids! By hooking onto asteroids with your rope, you swing them at your friends in a chaotic asteroid field!

Read more about the game on http://rymdstenar.com




Instructions:

* Platform: Windows
* Use Xbox controllers (up to 8)
* Press the "Back" button to open options menu + in game instructions.
* Put on some nice music when you play! We have three playlists we recommend:
	- Rymdstenar Prog: https://open.spotify.com/user/philipshield/playlist/6j4zERa4i9lYyqtEFc9wPS
	- Rymdstenar Electrojazz: https://open.spotify.com/user/philipshield/playlist/5yaz6L2iZ92apGXnQBuou1
	- Rymdstenar DUNK: https://open.spotify.com/user/philipshield/playlist/76tEPH7Xpt3aNawpBiYKkW





Group name: Subset Soup
Game Title: Rymdstenar
We are:
	Philip Sköld, phisko@kth.se, 0727272726
	Daniel Månsson, dmans@kth.se, 0735081234