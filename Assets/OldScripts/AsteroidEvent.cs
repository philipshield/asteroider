﻿using UnityEngine;
using System.Collections;

public abstract class AsteroidEvent : MonoBehaviour
{
    [System.Flags]
    public enum CategoryFlags
    {
        NothingFancy = 1 << 0,
        ControllingCamera = 1 << 1,
        FootballFriendly =  1 << 2
    }

    [SerializeField, EnumFlag]
    CategoryFlags m_categories;
    public CategoryFlags Categories { get { return m_categories; } }

    public abstract IEnumerator PerformEvent(NewAsteroidField.Context _context);
}
