﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameModeManager : MonoBehaviour
{
    [System.Serializable]
    public class GameModeEntry
    {
        public string name;
        public GameModeBase gameModePrefab;
    }

    [SerializeField]
    List<GameModeEntry> m_gameModes;

    public List<GameModeEntry> AvailableGameModes => m_gameModes;

    public GameModeEntry SelectedGameMode { get; private set; }

    /// <summary>
    /// Returns the current active game mode. Is only valid ingame.
    /// </summary>
    public GameModeBase CurrentGameMode { get; private set; }

    void Awake ()
    {
        string gameModeName = PlayerPrefs.GetString("gamemode", m_gameModes.First().name);
        SelectedGameMode = m_gameModes.Where((gm) => gm.name == gameModeName).Concat(new[] { m_gameModes.First() }).First();
	}

    public GameModeBase PrepareGameMode()
    {
        if (CurrentGameMode != null)
        {
            Debug.LogError("Trying to prepare a game mode when there is one already active");
            return null;
        }

        if (SelectedGameMode == null)
        {
            Debug.LogError("Does not have a game mode selected");
            return null;
        }

        CurrentGameMode = Instantiate(SelectedGameMode.gameModePrefab);
        CurrentGameMode.Config = CurrentGameMode.GetComponent<GameModeConfig>();
        CurrentGameMode.transform.parent = this.transform;

        return CurrentGameMode;
    }

    public void SelectGamemode(GameModeEntry _gamemode)
    {
        SelectedGameMode = _gamemode;
        PlayerPrefs.SetString("gamemode", _gamemode.name);
    }

    public void Reset()
    {
        if (CurrentGameMode != null)
        {
            CurrentGameMode.Shutdown();
            Destroy(CurrentGameMode.gameObject);
            CurrentGameMode = null;
        }
    }
}
