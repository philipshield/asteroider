﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameModeTime : GameModeBase
{
    class PlayerEntry
    {
        public Player player;
        public int score;
    }

    Dictionary<int, PlayerEntry> m_players;
    Model m_guiModel;
    float m_spawnDelay;
    bool m_gameOver = true;
    float m_startTimeStamp;
    float m_timeLimit;
    bool m_suddenDeath = false;

    #region Init
    void Start()
    {
        EventManager.RegisterEvent<EventGameStarted>(OnGameStarted);
        EventManager.RegisterEvent<EventShipDestroyed>(OnShipDestroyed);
    }

    void OnDestroy()
    {
        EventManager.UnregisterEvent<EventGameStarted>(OnGameStarted);
        EventManager.UnregisterEvent<EventShipDestroyed>(OnShipDestroyed);
    }

    public override void Init(List<Player> _players)
    {
        m_players = new Dictionary<int, PlayerEntry>();
        m_guiModel = new Model();

        m_spawnDelay = Config.Get<float>("spawn_delay", 2.0f);
        int timeLimit = Config.Get<int>("time_limit", 120);
        m_timeLimit = (float)timeLimit;

        foreach (var player in _players)
        {
            m_players.Add(player.Id, new PlayerEntry()
            {
                player = player,
                score = 0
            });

            m_guiModel.SetInt("score_" + player.Id, 0);
            m_guiModel.SetInt("time_limit", timeLimit);
            m_guiModel.SetObject<Color>("color_" + player.Id, player.m_color);
        }

        Systems.Instance.GuiManager.ShowHUD("timeHud", m_guiModel);
    }

    public override void Shutdown()
    {
        Systems.Instance.GuiManager.HideHUD("timeHud");
    }
    #endregion

    #region Event listeners
    void OnGameStarted(EventGameStarted _event)
    {
        m_gameOver = false;
        m_startTimeStamp = Time.time;

        foreach (var player in m_players.Values)
        {
            player.player.Spawn(Camera.main.transform.position, 15.0f, true);
        }
    }

    void OnShipDestroyed(EventShipDestroyed _event)
    {
        if (m_gameOver)
        {
            if (m_suddenDeath)
                EvaluateSuddenDeathWinner();

            return;
        }

        var scoringPlayers = _event.DestroyedBy.Where(playerId => playerId != _event.Player.Id).ToList();
        if (scoringPlayers.Count == 0)
        {
            var entry = m_players[_event.Player.Id];
            entry.score -= 1;
            m_guiModel.SetInt("score_" + _event.Player.Id, entry.score);
        }
        else
        {
            foreach (var playerId in scoringPlayers)
            {
                var entry = m_players[playerId];
                entry.score += 1;
                m_guiModel.SetInt("score_" + playerId, entry.score);
            }
        }

        StartCoroutine(RespawnPlayer(_event.Player));
    }
    #endregion

    void Update()
    {
        float timeLeft = m_timeLimit - (Time.time - m_startTimeStamp);
        if (!m_gameOver && timeLeft < 0.0f)
        {
            m_gameOver = true;

            int maxScore = m_players.Values.Max(p => p.score);
            var winners = m_players.Values.Where(p => p.score == maxScore).ToList();

            if (winners.Count == 1)
            {
                StartCoroutine(WinSequence(winners.FirstOrDefault()));
            }
            else
            {
                m_suddenDeath = true;
                StartCoroutine(ShowSuddenDeathPopup());

                foreach (var p in m_players)
                {
                    if (!winners.Contains(p.Value))
                    {
                        p.Value.player.Ship.DestroyShip(null);
                    }
                }
            }
        }
    }

    void EvaluateSuddenDeathWinner()
    {
        var playersAlive = m_players.Values.Where(p => !p.player.Dead()).ToList();

        if (playersAlive.Count == 1)
        {
            m_suddenDeath = false;
            StartCoroutine(WinSequence(playersAlive.FirstOrDefault()));
        }
    }

    IEnumerator ShowSuddenDeathPopup()
    {
        Model model = new Model();
        model.SetString("text", "Sudden death");
        Systems.Instance.GuiManager.ShowHUD("bigTextHud", model);
        yield return new WaitForSeconds(2.0f);
        Systems.Instance.GuiManager.HideHUD("bigTextHud");
    }

    IEnumerator RespawnPlayer(Player _player)
    {
        yield return new WaitForSeconds(m_spawnDelay);
        _player.Spawn(Camera.main.transform.position, 15.0f, false);
    }

    IEnumerator WinSequence(PlayerEntry _winner)
    {
        Model model = new Model();
        model.SetInt("winner", _winner.player.Id + 1);
        model.SetObject<Color>("color", _winner.player.m_color);

        Systems.Instance.GuiManager.ShowHUD("winnerHud", model);

        yield return new WaitForSeconds(3.0f);

        Systems.Instance.GuiManager.HideHUD("winnerHud");

        foreach (var player in m_players.Values)
        {
            if (player.player.Ship != null)
                player.player.Ship.SupressExplosion();
        }
        Application.LoadLevel(0);
    }
}
