﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class GameModeStock : GameModeBase
{
    class PlayerEntry
    {
        public Player player;
        public int numLives;
    }

    Dictionary<int, PlayerEntry> m_players;
    Model m_guiModel;
    float m_spawnDelay;
    bool m_gameOver = false;

    #region Init
    void Start()
    {
        EventManager.RegisterEvent<EventGameStarted>(OnGameStarted);
        EventManager.RegisterEvent<EventShipDestroyed>(OnShipDestroyed);
    }

    void OnDestroy()
    {
        EventManager.UnregisterEvent<EventGameStarted>(OnGameStarted);
        EventManager.UnregisterEvent<EventShipDestroyed>(OnShipDestroyed);
    }

    public override void Init(List<Player> _players)
    {
        m_players = new Dictionary<int, PlayerEntry>();
        m_guiModel = new Model();

        m_spawnDelay = Config.Get<float>("spawn_delay", 2.0f);
        int maxLives = Config.Get<int>("max_lives", 3);

        foreach (var player in _players)
        {
            m_players.Add(player.Id, new PlayerEntry()
            {
                player = player,
                numLives = maxLives
            });

            m_guiModel.SetInt("num_lives_" + player.Id, maxLives);
            m_guiModel.SetObject<Color>("color_" + player.Id, player.m_color);
        }

        Systems.Instance.GuiManager.ShowHUD("stockHud", m_guiModel);
    }

    public override void Shutdown()
    {
        Systems.Instance.GuiManager.HideHUD("stockHud");
    }
    #endregion

    void OnGameStarted(EventGameStarted _event)
    {
        foreach (var player in m_players.Values)
        {
            player.player.Spawn(Camera.main.transform.position, 15.0f, true);
        }
    }

    void OnShipDestroyed(EventShipDestroyed _event)
    {
        PlayerEntry entry = m_players[_event.Player.Id];

        entry.numLives--;
        if (entry.numLives > 0)
        {
            StartCoroutine(RespawnPlayer(_event.Player));
        }

        m_guiModel.SetInt("num_lives_" + entry.player.Id, entry.numLives);

        CheckWinConditions();
    }

    IEnumerator RespawnPlayer(Player _player)
    {
        yield return new WaitForSeconds(m_spawnDelay);
        _player.Spawn(Camera.main.transform.position, 15.0f, false);
    }

    void CheckWinConditions()
    {
        int numPlayersAlive = 0;
        PlayerEntry winnerEntry = null;

        foreach (var entry in m_players.Values)
        {
            if (entry.numLives > 0)
            {
                numPlayersAlive++;
                winnerEntry = entry;
            }
        }

        if (numPlayersAlive <= 1 && winnerEntry != null)
        {
            OnGameWon(winnerEntry);
        }
    }

    void OnGameWon(PlayerEntry _winner)
    {
        if (!m_gameOver)
        {
            Debug.Log("We have a winner! " + _winner.player.Id);
            m_gameOver = true;
            StartCoroutine(WinnerSequence(_winner));
        }
    }

    IEnumerator WinnerSequence(PlayerEntry _winner)
    {
        Model model = new Model();
        model.SetInt("winner", _winner.player.Id + 1);
        model.SetObject<Color>("color", _winner.player.m_color);

        Systems.Instance.GuiManager.ShowHUD("winnerHud", model);

        yield return new WaitForSeconds(3.0f);

        Systems.Instance.GuiManager.HideHUD("winnerHud");

        foreach (var player in m_players.Values)
        {
            if (player.player.Ship != null)
                player.player.Ship.SupressExplosion();
        }
        Application.LoadLevel(0);
    }
}
