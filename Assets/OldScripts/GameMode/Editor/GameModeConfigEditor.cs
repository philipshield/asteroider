﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(GameModeConfig))]
public class GameModeConfigEditor : Editor
{
    class GameModeEditorEntry
    {
        public GameModeConfig.Entry entry;
        public bool foldout = false;
    }

    bool toggle = false;
    List<GameModeEditorEntry> m_entries;
    TextAsset m_lastLoadedConfigJson;

    public override void OnInspectorGUI()
    {
        GameModeConfig config = (GameModeConfig)target;

        config.m_gameConfigJson = (TextAsset)EditorGUILayout.ObjectField("Config file", config.m_gameConfigJson, typeof(TextAsset), false);
        if (config.m_gameConfigJson != m_lastLoadedConfigJson)
        {
            m_entries = null;
        }

        if (config.m_gameConfigJson == null)
        {
            EditorGUILayout.LabelField("Config file is null");
        }
        else
        {
            if (m_entries == null)
            {
                Load(config);
            }

            if (m_entries == null)
            {
                EditorGUILayout.LabelField("Failed to load");
            }
            else
            {
                toggle = EditorGUILayout.Foldout(toggle, "Entries");
                if (toggle)
                {
                    EditorGUI.indentLevel++;
                    foreach (var entry in m_entries)
                    {
                        entry.foldout = EditorGUILayout.Foldout(entry.foldout, entry.entry.key);
                        if (entry.foldout)
                        {
                            EditorGUI.indentLevel++;
                            entry.entry.key = EditorGUILayout.TextField("Key", entry.entry.key);
                            entry.entry.type = (GameModeConfig.Entry.Type)EditorGUILayout.EnumPopup("Data type", entry.entry.type);
                            switch (entry.entry.type)
                            {
                                case GameModeConfig.Entry.Type.Int:
                                    {
                                        int value = (int)config.Parse(entry.entry.data, GameModeConfig.Entry.Type.Int);
                                        value = EditorGUILayout.IntField("Value", value);
                                        entry.entry.data = value.ToString();
                                    }
                                    break;
                                case GameModeConfig.Entry.Type.Float:
                                    {
                                        float value = (int)config.Parse(entry.entry.data, GameModeConfig.Entry.Type.Int);
                                        value = EditorGUILayout.FloatField("Value", value);
                                        entry.entry.data = value.ToString();
                                    }
                                    break;
                                case GameModeConfig.Entry.Type.IntRange:
                                    EditorGUILayout.LabelField("TODO INTRANGE");
                                    break;
                                case GameModeConfig.Entry.Type.FloatRange:
                                    EditorGUILayout.LabelField("TODO FLOATRANGE");
                                    break;
                                case GameModeConfig.Entry.Type.String:
                                    {
                                        entry.entry.data = EditorGUILayout.TextField("Value", entry.entry.data);
                                    }
                                    EditorGUILayout.LabelField("TODO STRING");
                                    break;
                                default:
                                    EditorGUILayout.LabelField("Error");
                                    break;
                            }
                            EditorGUI.indentLevel--;
                        }
                    }
                    EditorGUI.indentLevel--;

                    if (GUILayout.Button("Add entry"))
                    {
                        var entry = new GameModeEditorEntry();

                        entry.entry = new GameModeConfig.Entry();

                        entry.entry.type = GameModeConfig.Entry.Type.Int;
                        entry.entry.data = "";
                        entry.foldout = true;

                        m_entries.Add(entry);
                    }
                }
            }

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Load"))
            {
                Load(config);
            }
            if (GUILayout.Button("Save"))
            {
                Save(config);
            }
            EditorGUILayout.EndHorizontal();
        }
        //EditorGUILayout.EndToggleGroup();
    }


    void Load(GameModeConfig _config)
    {
        m_entries = null;
        var gameData = LitJson.JsonMapper.ToObject<GameModeConfig.EntryData>(_config.m_gameConfigJson.text);

        if (gameData != null)
        {
            m_entries = new List<GameModeEditorEntry>();
            foreach (var entry in gameData.entries)
                m_entries.Add(new GameModeEditorEntry() { entry = entry });

            m_lastLoadedConfigJson = _config.m_gameConfigJson;
        }
        else if(_config.m_gameConfigJson != null)
        {
            m_entries = new List<GameModeEditorEntry>();
            m_lastLoadedConfigJson = _config.m_gameConfigJson;
        }
    }

    void Save(GameModeConfig _config)
    {
        _config.GameEntries.Clear();

        foreach (var entry in m_entries)
            _config.GameEntries.Add(entry.entry.key, entry.entry);

        string path = AssetDatabase.GetAssetPath(_config.m_gameConfigJson);
        string jsonText = _config.SerializeToJson();

        System.IO.File.WriteAllText(path, jsonText);
        AssetDatabase.Refresh();
    }
}
