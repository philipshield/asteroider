﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class GameModeFootball : GameModeBase
{
    [SerializeField]
    GameObject m_footballPrefab;

    [SerializeField]
    GameObject m_borderPrefab;

    class PlayerEntry
    {
        public Player player;
        public int score;
    }

    Dictionary<int, PlayerEntry> m_players;
    Model m_guiModel;

    int m_scoreTeam0 = 0;
    int m_scoreTeam1 = 0;

    bool m_gameOver = false;
    int m_scoreLimit;
    GameObject m_football;
    float m_spawnDelay;
    float m_ballTimer = 0.3f;

    #region Init
    void Start()
    {
        EventManager.RegisterEvent<EventGameStarted>(OnGameStarted);
        EventManager.RegisterEvent<EventShipDestroyed>(OnShipDestroyed);
    }

    void OnDestroy()
    {
        EventManager.UnregisterEvent<EventGameStarted>(OnGameStarted);
        EventManager.UnregisterEvent<EventShipDestroyed>(OnShipDestroyed);
    }

    public override void Init(List<Player> _players)
    {
        m_spawnDelay = Config.Get<float>("spawn_delay", 3.0f);
        m_scoreLimit = Config.Get<int>("score_limit", 5);

        m_players = new Dictionary<int, PlayerEntry>();
        foreach (var player in _players)
        {
            m_players.Add(player.Id, new PlayerEntry()
            {
                player = player,
                score = 0
            });
        }

        m_guiModel = new Model();
        RefreshGuiModel();

        Systems.Instance.GuiManager.ShowHUD("footballScoreHud", m_guiModel);
    }

    public override void Shutdown()
    {
        Systems.Instance.GuiManager.HideHUD("footballScoreHud");
    }
    #endregion

    #region Event listeners
    void OnGameStarted(EventGameStarted _event)
    {
        foreach (var player in m_players.Values)
        {
            player.player.Spawn(Camera.main.transform.position, 15.0f, true);
        }

        Instantiate(m_borderPrefab, Vector3.zero, Quaternion.identity);

        Vector2 pos = Camera.main.transform.position + (Vector3)UnityEngine.Random.insideUnitCircle * 5.0f;
        m_football = (GameObject)Instantiate(m_footballPrefab, pos, Quaternion.identity);

        Camera.main.GetComponent<SmoothCamera>().m_overrideControls = true;
    }

    void OnShipDestroyed(EventShipDestroyed _event)
    {
        StartCoroutine(RespawnPlayer(_event.Player));
    }
    #endregion

    void RefreshGuiModel()
    {
        m_guiModel.SetInt("score_0", m_scoreTeam0);
        m_guiModel.SetInt("score_1", m_scoreTeam1);
    }

    void Update()
    {
        if (m_football != null)
        {
            var leftEdgeViewportPos = Camera.main.WorldToViewportPoint(m_football.transform.position + Vector3.right * m_football.transform.localScale.x);
            if (leftEdgeViewportPos.x < 0.0f)
            {
                StartCoroutine(GoalSequence(1));
                return;
            }

            var rightEdgeViewportPos = Camera.main.WorldToViewportPoint(m_football.transform.position - Vector3.right * m_football.transform.localScale.x);
            if (rightEdgeViewportPos.x > 1.0f)
            {
                StartCoroutine(GoalSequence(0));
                return;
            }
        }
    }

    void CheckWinConditions()
    {
        if (m_scoreTeam0 >= m_scoreLimit)
        {
            OnGameWon(0);
        }
        else if(m_scoreTeam1 >= m_scoreLimit)
        {
            OnGameWon(1);
        }
    }

    void OnGameWon(int _team)
    {
        if (!m_gameOver)
        {
            Debug.Log("We have a winner! " + _team);
            m_gameOver = true;
            StartCoroutine(WinnerSequence(_team));
        }
    }

    IEnumerator WinnerSequence(int _winnerTeam)
    {
        Model model = new Model();
        model.SetString("text", (_winnerTeam == 0 ? "Left" : "Right") + " team won");
        Systems.Instance.GuiManager.ShowHUD("bigTextHud", model);
        yield return new WaitForSeconds(4.0f);
        Systems.Instance.GuiManager.HideHUD("bigTextHud");

        Application.LoadLevel(0);
    }

    IEnumerator GoalSequence(int _teamScored)
    {
        Destroy(m_football);
        m_football = null;

        if (_teamScored == 0)
            m_scoreTeam0++;
        else
            m_scoreTeam1++;

        RefreshGuiModel();

        Model model = new Model();
        model.SetString("text", "Goal!");
        Systems.Instance.GuiManager.ShowHUD("bigTextHud", model);
        yield return new WaitForSeconds(2.0f);
        Systems.Instance.GuiManager.HideHUD("bigTextHud");
        yield return new WaitForSeconds(0.3f);

        CheckWinConditions();

        yield return new WaitForSeconds(0.3f);

        Vector2 pos = Camera.main.transform.position + (Vector3)UnityEngine.Random.insideUnitCircle * 5.0f;
        m_football = (GameObject)Instantiate(m_footballPrefab, pos, Quaternion.identity);
    }

    IEnumerator RespawnPlayer(Player _player)
    {
        yield return new WaitForSeconds(m_spawnDelay);
        _player.Spawn(Camera.main.transform.position, 15.0f, false);
    }
}
