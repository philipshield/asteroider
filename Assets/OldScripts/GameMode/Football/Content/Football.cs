﻿using UnityEngine;
using System.Collections;

public class Football : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D _collision)
    {
        Spaceship otherShip = _collision.gameObject.GetComponent<Spaceship>();
        if (otherShip != null)
        {
            otherShip.DestroyShip(null);
        }
    }
}
