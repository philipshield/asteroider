﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(GameModeConfig))]
public abstract class GameModeBase : MonoBehaviour
{
    [SerializeField, EnumFlag]
    AsteroidEvent.CategoryFlags m_allowedAsteroidEventCategories;
    public AsteroidEvent.CategoryFlags AllowedAsteroidEventCategories { get { return m_allowedAsteroidEventCategories; } }

    public GameModeConfig Config { get; set; }

    public abstract void Init(List<Player> _players);
    public abstract void Shutdown();
}
