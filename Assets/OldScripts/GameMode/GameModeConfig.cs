﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameModeConfig : MonoBehaviour
{
    [SerializeField]
    public TextAsset m_gameConfigJson;

    [SerializeField]
    TextAsset m_playerConfigJson;

    public class Entry
    {
        public enum Type
        {
            Int,
            Float,
            IntRange,
            FloatRange,
            String,
        }

        public string key;
        public string data;
        public Type type;
        public bool adjustableByPlayer;
        public string constraint;
    }

    public class IntRange
    {
        public int low;
        public int high;

        public override string ToString()
        {
            return low + ":" + high;
        }
    }

    public class FloatRange
    {
        public float low;
        public float high;

        public override string ToString()
        {
            return low + ":" + high;
        }
    }

    public class PlayerEntry
    {
        public string key;
        public string data;
    }

    public class EntryData
    {
        public Entry[] entries;
    }

    public class PlayerEntryData
    {
        public PlayerEntry[] entries;
    }

    Dictionary<string, Entry> m_gameEntries = new Dictionary<string, Entry>();
    Dictionary<string, PlayerEntry> m_playerEntries = new Dictionary<string, PlayerEntry>();

    public Dictionary<string, Entry> GameEntries
    {
        get { return m_gameEntries; }
    }

    public Dictionary<string, PlayerEntry> PlayerEntries
    {
        get { return m_playerEntries; }
    }

    void Awake()
    {
        Init();
    }

    public void Init()
    {
        var gameData = LitJson.JsonMapper.ToObject<EntryData>(m_gameConfigJson.text);
      //  var playerData = LitJson.JsonMapper.ToObject<PlayerEntryData>(m_playerConfigJson.text);

        m_gameEntries = new Dictionary<string, Entry>();
        foreach (var data in gameData.entries)
            m_gameEntries.Add(data.key, data);

        m_playerEntries = new Dictionary<string, PlayerEntry>();
      //  foreach (var data in playerData.entries)
        //    m_playerEntries.Add(data.key, data);
    }

    public string SerializeToJson()
    {
        var gameData = new EntryData();
        gameData.entries = new Entry[m_gameEntries.Count];

        int i = 0;
        foreach (var entry in m_gameEntries.Values)
        {
            gameData.entries[i] = entry;
            ++i;
        }

        return LitJson.JsonMapper.ToJson(gameData);
    }

    public T Get<T>(string _key, T _default = default(T)) 
    {
        if (m_gameEntries.ContainsKey(_key))
        {
            var gameEntry = m_gameEntries[_key];

            if (m_playerEntries.ContainsKey(_key))
            {
                var playerEntry = m_playerEntries[_key];
                return (T)Parse(playerEntry.data, gameEntry.type);
            }
            else
            {
                return (T)Parse(gameEntry.data, gameEntry.type);
            }
        }
       
        return _default;
    }

    public void SetPlayerEntry<T>(string _key, T _value) where T : class
    {
        if (m_playerEntries.ContainsKey(_key))
        {
            m_playerEntries[_key].data = _value.ToString();
        }
        else
        {
            m_playerEntries.Add(_key, new PlayerEntry()
            {
                key = _key,
                data = _value.ToString()
            });
        }
    }   

    public object Parse(string _data, Entry.Type _type)
    {
        switch (_type)
        {
            case Entry.Type.Int:
                {
                    int value;
                    int.TryParse(_data, out value);
                    return value;
                }
            case Entry.Type.Float:
                {
                    float value;
                    float.TryParse(_data, out value);
                    return value;
                }
            case Entry.Type.IntRange:
                {
                    var data = _data.Split(':');
                    var result = new IntRange();
                    int.TryParse(data[0], out result.low);
                    int.TryParse(data[1], out result.high);
                    return result;
                }
            case Entry.Type.FloatRange:
                {
                    var data = _data.Split(':');
                    var result = new FloatRange();
                    float.TryParse(data[0], out result.low);
                    float.TryParse(data[1], out result.high);
                    return result;
                }
            case Entry.Type.String:
                return _data;
        }

        return null;
    }
}
