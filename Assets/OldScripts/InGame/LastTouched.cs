﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LastTouched : MonoBehaviour
{
    List<int> m_currentlyTouching = new List<int>();
    List<int> m_lastTouched = new List<int>();

    public void StartTouch(int _id)
    {
        m_currentlyTouching.Add(_id);

        EvaluateLastTouched();
    }

    public void EndTouch(int _id)
    {
        m_currentlyTouching.Remove(_id);

        EvaluateLastTouched();
    }

    void EvaluateLastTouched()
    {
        if (m_currentlyTouching.Count != 0)
        {
            m_lastTouched.Clear();
            m_lastTouched.AddRange(m_currentlyTouching);
        }
    }

    public List<int> GetLastTouched()
    {
        return m_lastTouched;
    }

    public void SetTouchStateFromOther(LastTouched _other)
    {
        m_lastTouched.Clear();
        m_lastTouched.AddRange(_other.m_lastTouched);
    }
}
