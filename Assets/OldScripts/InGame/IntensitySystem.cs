﻿using UnityEngine;
using System.Collections.Generic;




public class IntensitySystem : MonoBehaviour
{
    
    public enum IntensityLevel
    {
        Easy,
        Normal,
        Hard,
        Eleven
    }

    [System.Serializable]
    public class IntensitySetting
    {
        public IntensityLevel Level;
        public float Lower;
        public float Upper;
    }

    public float INTENSE_THRESHOLD = 0.85f;
    public float CALM_THRESHOLD = 0.15f;
    
    public List<IntensitySetting> IntensitySettings;
    private Dictionary<IntensityLevel, IntensitySetting> m_settings = new Dictionary<IntensityLevel, IntensitySetting>();

    public IntensityLevel CurrentLevel = IntensityLevel.Hard;
    private float m_current;

    public bool DrawDebug = true;

	void Start ()
    {
        foreach (IntensitySetting setting in IntensitySettings)
            m_settings.Add(setting.Level, setting);
    }

    void Update()
    {
        const float wavelength = 30;
        const float phase = Mathf.PI; // start in middle
        float rel = (Mathf.Sin(2 * Mathf.PI * Time.time / wavelength + phase) + 1) / 2;
        m_current = GetMin() + rel * (GetMax() - GetMin());

        //Debug.Log(m_current);
    }

    void OnDrawGizmos()
    {
        if (!Application.isPlaying || !DrawDebug) return;

        Gizmos.color = Color.red;
        Vector3 origo = new Vector3(-40, -30, 0);
        float height = 40;
        float markeroffset = 5;
        Gizmos.DrawLine(Vector3.zero + origo, new Vector3(0, height, 0) + origo);

        Gizmos.color = Color.green;
        float top = height * GetMax();
        float low = height * GetMin();
        Gizmos.DrawLine(origo + new Vector3(0, top, 0), origo + new Vector3(10, top, 0));
        Gizmos.DrawLine(origo + new Vector3(0, low, 0), origo + new Vector3(10, low, 0));


        float currheight = height*m_current;
        //Debug.Log(m_current);
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(origo + new Vector3(markeroffset, currheight, 0), 1);
    }


    //
    // Interface
    //

    public float GetMax()
    {
        return m_settings[CurrentLevel].Upper;
    }

    public float GetMin()
    {
        return m_settings[CurrentLevel].Lower;
    }

    public float GetCurrent()
    {
        return m_current;
    }

    public float GetCurrentRel()
    {
        return (m_current - GetMin()) / (GetMax() - GetMin());
    }

    public bool IsRelIntense()
    {
        return m_current > INTENSE_THRESHOLD;
    }

    public bool IsCalm()
    {
        return m_current < CALM_THRESHOLD;
    }

    /*
     * >Linearly< interpolate according to >absolute intensity<.
     * This means that calm will always be closer to 0 and Eleven closer to 1.
     *
     * The return intensity is [0, 1]
     */
    public float Linear(float min, float max)
    {
        return Mathf.Lerp(min, max, GetCurrent());
    }

    /*
     * >Linearly< interpolate according to >relative intensity<.
     * This means linearly harder for intense sections, regardless of IntensityLevel
     *
     * The return intensity is [0, 1]
     */
    public float LinearRel(float min, float max)
    {
        return Mathf.Lerp(min, max, GetCurrentRel());
    }

    /*
     * >Exponentially< interpolate according to >absolute intensity<.
     * This means that calm will always be closer to 0 and Eleven closer to 1.
     * If it gets a LOT harder with the intensity, the exp term can be changed.
     *
     * The return intensity is [0, 1]
     */
    public float Exp(float min, float max, float exp = 2)
    {
        return Mathf.Lerp(min, max, Mathf.Pow(GetCurrent(), exp));
    }

    /*
     * >Exponentially< interpolate according to >relative intensity<.
     * This means exponentially harder for intense sections, regardless of IntensityLevel
     *
     * The return intensity is [0, 1]
     */
    public float ExpRel(float min, float max, float exp = 2)
    {
        return Mathf.Lerp(min, max, Mathf.Pow(GetCurrentRel(), exp));
    }

}
