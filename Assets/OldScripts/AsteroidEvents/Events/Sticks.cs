﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sticks : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var intensity = Systems.Instance.IntensitySystem;
        float radius = _context.radius;

        yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 2.0f));

        float vel = UnityEngine.Random.Range(10.0f, 25.0f);

        // Fewer passes is harder because different stuff happens sooner.
        int count = 8 - Mathf.RoundToInt(intensity.Exp(0, 7));

        for (int i = 0; i < count; i++)
        {
            Vector2 target = UnityEngine.Random.insideUnitCircle * radius * 0.1f + _context.center;
            Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
            var pos = target + dir * radius;
            float angle = Mathf.Atan2(dir.y, dir.x) * 180.0f / Mathf.PI;

            {
                GameObject asteroid = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, Quaternion.Euler(0, 0, angle));
                AsteroidHelper.CreateRandomAsteroid(asteroid, AsteroidHelper.AsteroidType.Stick);
                asteroid.GetComponent<Shatter>().SetMassFromDensity(0.5f);
                var body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = vel * (target - pos).normalized + _context.camera.GetDesiredVelocity();
                body.angularVelocity = UnityEngine.Random.Range(-3f, 3f);
            }

            if (UnityEngine.Random.Range(0, 2) == 1)
            {
                pos = target - dir * radius;
                GameObject asteroid = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, Quaternion.Euler(0, 0, angle));
                AsteroidHelper.CreateRandomAsteroid(asteroid, AsteroidHelper.AsteroidType.Stick);
                asteroid.GetComponent<Shatter>().SetMassFromDensity(0.5f);
                var body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = vel * (target - pos).normalized + _context.camera.GetDesiredVelocity();
                body.angularVelocity = UnityEngine.Random.Range(-3f, 3f);
            }

            float waitTime = 4.0f - intensity.Exp(0.0f, 4.0f);
            yield return new WaitForSeconds(waitTime);
        }

        yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 2.0f));

    }
}
