﻿using UnityEngine;
using System.Collections;

public class RandomDirections : AsteroidEvent
{
    [Header("Count")]
    [SerializeField]
    int m_calmMinCount = 8;
    [SerializeField]
    int m_intenseMinCount = 3;
    [SerializeField]
    int m_calmMaxCount = 4;
    [SerializeField]
    int m_intenseMaxCount = 1;

    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var intensity = Systems.Instance.IntensitySystem;

        //Camera Control
        _context.camera.SetState(SmoothCamera.CameraState.Still);

        // count (lower is harder, faster until next event)
        int minCount = (int)intensity.LinearRel(m_calmMinCount, m_intenseMinCount);
        int maxCount = (int)intensity.LinearRel(m_calmMaxCount, m_intenseMaxCount);
        int count = UnityEngine.Random.Range(minCount, maxCount);

        for (int i = 0; i < count; i++)
        {
            Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
            Vector2 pos = (-dir * _context.radius);
            GameObject obj = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.PartiallyBreakable], _context.center + pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
            AsteroidHelper.CreateRandomAsteroid(obj, AsteroidHelper.AsteroidType.Simple);

            // velocity
            float minSpeed = intensity.Exp(3, 30);
            float maxSpeed = intensity.Exp(8, 50);
            Vector2 velocity = UnityEngine.Random.Range(minSpeed, maxSpeed) * (dir + UnityEngine.Random.insideUnitCircle * 0.5f);
            obj.GetComponent<Rigidbody2D>().velocity = velocity;

            // angular velocity
            minSpeed = intensity.Exp(-150, -300);
            maxSpeed = intensity.Exp(150, 300);
            float angularVelocity = UnityEngine.Random.Range(minSpeed, maxSpeed);

            if (intensity.IsRelIntense())
                angularVelocity = Mathf.Sign(angularVelocity) * Mathf.Clamp(maxSpeed - 50, maxSpeed, Mathf.Abs(angularVelocity));
            obj.GetComponent<Rigidbody2D>().angularVelocity = angularVelocity;

            // wait until next
            float minTime = intensity.Exp(1.5f, 0.3f);
            float maxTime = intensity.LinearRel(4.5f, 2.0f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(minTime, maxTime));
        }
    }
}
