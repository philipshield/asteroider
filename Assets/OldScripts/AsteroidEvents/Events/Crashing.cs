﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Crashing : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var cameraControl = _context.camera;

        // slow down before start
        cameraControl.SetState(SmoothCamera.CameraState.Still);
        float currentvel = cameraControl.GetVelocity().magnitude;
        yield return new WaitForSeconds(currentvel * 0.3f); //ish

        //smaller blocks
        float boxwidth = 40;
        float boxheight = 100;
        float spawnmargin = 10;

        //create two big boxes
        Vector2[] boxpoints = new Vector2[4];
        boxpoints[0] = new Vector2(boxwidth / 2, boxheight / 2);
        boxpoints[1] = new Vector2(boxwidth / 2, -boxheight / 2);
        boxpoints[2] = new Vector2(-boxwidth / 2, -boxheight / 2);
        boxpoints[3] = new Vector2(-boxwidth / 2, boxheight / 2);

        // from the right, tunnelwidth apart
        float angle = UnityEngine.Random.Range(0, 360);
        Quaternion rot = Quaternion.Euler(0, 0, angle);
        Vector2 dir = rot * new Vector2(1, 0);

        Vector2 odir = new Vector2(-dir.y, dir.x);
        Vector2 pos = _context.center + -dir * (boxheight + spawnmargin);

        Vector2 vel = dir * UnityEngine.Random.Range(16.0f, 19.8f);
        Vector2 cameravel = vel * 1.05f;


        // The event:
        cameraControl.SetState(SmoothCamera.CameraState.ExternalControl);

        //Begin Fall
        Debug.Log("Begin Fall");
        cameraControl.SetDesiredVelocity(cameravel);
        yield return new WaitForSeconds(2.0f); //get up to speed

        // Fall 
        Debug.Log("Falling");
        float tiltangle = 0;
        GameObject box = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, rot);
        Quaternion tilt = Quaternion.Euler(0, 0, -tiltangle);
        box.transform.rotation *= tilt;
        var poly = box.AddComponent<PolygonCollider2D>();
        poly.SetPath(0, boxpoints);
        box.GetComponent<Rigidbody2D>().velocity = vel;
        box.GetComponent<Rigidbody2D>().angularVelocity = tiltangle * 0.1f;
        var body = poly.GetComponent<Rigidbody2D>();
        poly.GetComponent<Shatter>().SetMassFromDensity(5.0f);

        yield return new WaitForSeconds(10.0f);

        // Escape 
        Debug.Log("Escape");
        cameravel *= 0.9f;                          // slow down
        Vector2 escapedir = UnityEngine.Random.Range(0, 2) == 0 ? odir : -odir;
        cameravel += escapedir * cameravel.magnitude;    // move camera to side
        cameraControl.SetDesiredVelocity(cameravel);
        yield return new WaitForSeconds(10.0f);

        // Calm down
        Debug.Log("Calm down");
        cameraControl.SetState(SmoothCamera.CameraState.Still);
        yield return new WaitForSeconds(3.0f);

    }
}
