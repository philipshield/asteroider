﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HugeAsteroid : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var intensity = Systems.Instance.IntensitySystem;

        // spawn
        Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 pos = (-dir * _context.radius * 1.9f);
        GameObject obj = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], _context.center + pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
        AsteroidHelper.CreateRandomAsteroid(obj, AsteroidHelper.AsteroidType.SimpleHuge);

        // velocity 
        float minSpeed = intensity.Exp(3, 20);
        float maxSpeed = intensity.Exp(10, 60);
        obj.GetComponent<Rigidbody2D>().velocity = UnityEngine.Random.Range(minSpeed, maxSpeed) * (dir + UnityEngine.Random.insideUnitCircle * 0.5f);

        // angular velocity
        minSpeed = intensity.Exp(-150, -220);
        maxSpeed = intensity.Exp(150, 220);
        float angularVelocity = UnityEngine.Random.Range(minSpeed, maxSpeed);

        if (intensity.IsRelIntense())
            angularVelocity = Mathf.Sign(angularVelocity) * Mathf.Clamp(maxSpeed - 50, maxSpeed, Mathf.Abs(angularVelocity));
        obj.GetComponent<Rigidbody2D>().angularVelocity = angularVelocity;

        // wait 
        float minTime = intensity.Exp(1.0f, 0.3f);
        float maxTime = intensity.LinearRel(3.5f, 2.0f);
        yield return new WaitForSeconds(UnityEngine.Random.Range(minTime, maxTime));

    }
}
