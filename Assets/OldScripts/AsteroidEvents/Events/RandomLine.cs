﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomLine : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var intensity = Systems.Instance.IntensitySystem;

        // count (higher is harder)
        int minCount = (int)intensity.Exp(6, 25);
        int maxCount = (int)intensity.Exp(10, 40);
        int count = UnityEngine.Random.Range(minCount, maxCount);

        // velocity (closer and faster is harder)
        float minSpeed = intensity.Exp(10, 30);
        float maxSpeed = intensity.Exp(14, 30);

        // velocity innaccuracy (lower is harder)
        float targetdirinacc = intensity.Exp(0.15f, 0.05f);

        // angular velocity
        float minAngSpeed = intensity.Exp(-150, -300);
        float maxAngSpeed = intensity.Exp(150, 300);

        // spacing (lower is harder) 
        // TODO: make sure not too close
        float minSpacing = intensity.Linear(4, 1.5f);
        float maxSpacing = intensity.Linear(2, 0.5f);
        float spacing = UnityEngine.Random.Range(minSpacing, maxSpacing);

        // line skew
        // TODO: harder with more variation here?
        float linedirinnac = 0.2f;

        // mass
        float mass = intensity.Exp(0.7f, 0.9f);

        Vector2 targetdir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 linedir = (new Vector2(-targetdir.y, targetdir.x).normalized +
            (UnityEngine.Random.insideUnitCircle.normalized * linedirinnac)).normalized;
        Vector2 step = linedir * spacing;

        Vector2 pos = _context.center + (-targetdir * _context.radius);
        pos -= step * ((count / 2) + 1); // go back half of count

        for (int i = 0; i < count; i++)
        {
            GameObject asteroid = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
            AsteroidHelper.CreateRandomAsteroid(asteroid, AsteroidHelper.AsteroidType.SimpleSmall);

            asteroid.GetComponent<Rigidbody2D>().velocity = UnityEngine.Random.Range(minSpeed, maxSpeed) * (targetdir + UnityEngine.Random.insideUnitCircle * targetdirinacc);
            float angularVelocity = UnityEngine.Random.Range(minAngSpeed, maxAngSpeed);

            asteroid.GetComponent<Shatter>().SetMassFromDensity(mass);

            pos += step;
        }

        // wait
        float minTime = intensity.Exp(5.0f, 3.0f);
        float maxTime = intensity.Exp(7.5f, 5.0f);
        yield return new WaitForSeconds(UnityEngine.Random.Range(minTime, maxTime));
    }
}
