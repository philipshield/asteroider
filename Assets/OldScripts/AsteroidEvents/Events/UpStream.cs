﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpStream : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        yield return null;
        var intensity = Systems.Instance.IntensitySystem;

        bool downstream = UnityEngine.Random.Range(0, 3) == 0;
        Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 cameradir = downstream ? -dir : dir;

        float camMinSpeed = intensity.Exp(10.0f, 18.0f);
        float camMaxSpeed = intensity.Exp(16.0f, 22.0f);
        float camSpeed = UnityEngine.Random.Range(camMinSpeed, camMaxSpeed);

        _context.camera.SetState(SmoothCamera.CameraState.ExternalControl);
        float prevSpeed = _context.camera.m_speed;
        _context.camera.SetSpeed(prevSpeed * intensity.Exp(1.0f, 20.0f));
        _context.camera.SetDesiredVelocity(cameradir * camSpeed);

        yield return new WaitForSeconds(4.0f);

        Vector2 perp = new Vector2(-dir.y, dir.x);
        float width = _context.radius / (2.3f * Mathf.PI);
        int count = UnityEngine.Random.Range(9, 13);

        for (int i = 0; i < count; i++)
        {
            int asteroidCount = Mathf.RoundToInt(intensity.Linear(1, 3));
            for (int j = 0; j < asteroidCount; j++)
            {
                float side = UnityEngine.Random.Range(-1.0f, 1.0f);
                Vector2 pos = _context.center + dir * _context.radius + UnityEngine.Random.Range(-1.0f, 1.0f) * _context.radius * perp;
                GameObject asteroid = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
                AsteroidHelper.CreateRandomAsteroid(asteroid, UnityEngine.Random.Range(0, 2) == 0 ? AsteroidHelper.AsteroidType.Simple : AsteroidHelper.AsteroidType.SimpleSmall);

                Rigidbody2D body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = dir * UnityEngine.Random.Range(-23, -10.0f) + perp * 2.0f * (UnityEngine.Random.Range(-3.0f, 3.0f) - side);
                body.angularVelocity = UnityEngine.Random.Range(-50.0f, 50.0f);
                if (downstream) body.velocity *= 2;
            }

            yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f, 2.0f));
        }

        _context.camera.SetSpeed(prevSpeed);
        _context.camera.SetDesiredVelocity(Vector2.zero);
        yield return new WaitForSeconds(2.0f);

    }
}
