﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TargetPlayers : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var intensity = Systems.Instance.IntensitySystem;
        var players = Game.Instance.players;

        //wait before event
        float minWait = intensity.Linear(5.0f, 0.0f);
        float maxWait = intensity.Linear(10.0f, 2.0f);
        yield return new WaitForSeconds(UnityEngine.Random.Range(minWait, maxWait));

        float minVel = intensity.Exp(20.0f, 40.0f);
        float maxVel = intensity.Exp(30.0f, 60.0f);
        float vel = UnityEngine.Random.Range(minVel, maxVel);

        float skipProbability = Mathf.Clamp01((float)(players.Count(p => p.Value.Alive()) - 4) / 10.0f);

        foreach (var player in players.Values)
        {
            if (player.Alive() && UnityEngine.Random.Range(0.0f, 1.0f) > skipProbability)
            {
                var ppos = player.GetPosition();

                Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
                var pos = _context.center + dir * _context.radius;

                GameObject asteroid = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
                AsteroidHelper.CreateRandomAsteroid(asteroid, AsteroidHelper.AsteroidType.Simple);

                asteroid.GetComponent<Shatter>().SetMassFromDensity(0.5f);
                var body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = vel * (ppos - pos).normalized + _context.camera.GetDesiredVelocity();


                float minAngSpeed = intensity.Linear(0.0f, 500.0f);
                float maxAngSpeed = intensity.Linear(100.0f, 900.0f);
                body.angularVelocity = UnityEngine.Random.Range(minAngSpeed, maxAngSpeed);
            }
        }

        yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 2.0f));
    }
}
