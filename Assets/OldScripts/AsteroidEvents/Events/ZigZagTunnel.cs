﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZigZagTunnel : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        float min_thick = 20f;
        float y_top = 70f;
        float y_bottom = 70f;

        float x_spacing = 2f;
        float gap = 15f;
        float margin = gap / 2f;

        float max_y = y_top - min_thick - margin;
        float min_y = -y_bottom + min_thick + margin;

        // Spawn all the things!
        Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 odir = new Vector2(-dir.y, dir.x);
        Vector2 vel = -dir * 30f;
        Vector2 x_pos = _context.center + dir * _context.radius;

        float event_camera_speed = 18f;
        _context.camera.SetState(SmoothCamera.CameraState.ExternalControl);
        _context.camera.SetDesiredVelocity(dir * event_camera_speed);
        yield return new WaitForSeconds(1.3f); // get some speed

        float max_w = 10f;
        float min_w = 2f;
        float avg_w = (min_w + min_w) / 2.0f;

        float A = UnityEngine.Random.Range(10f, 20f);
        float phase = UnityEngine.Random.Range(0, 2 * Mathf.PI);

        float wavelenth = 60f;
        float sin_step = (2 * Mathf.PI) / (wavelenth / avg_w);
        float t = phase;
        int N = 10;

        for (int i = 0; i < N; i++)
        {
            float curr_y = A * Mathf.Sin(t);
            float next_y = A * Mathf.Sin(t + sin_step);
            t += sin_step;

            float w = UnityEngine.Random.Range(min_w, max_w);
            Vector2 y_pos = odir * curr_y;
            Vector2 y_pos_next = odir * next_y;
            Vector2[] top_box = { y_pos + odir*margin,
                                  y_pos + odir*y_top,
                                  y_pos + odir*y_top + dir * w,
                                  y_pos_next + odir*margin + dir * w };

            Vector2[] bottom_box = { y_pos - odir*margin,
                                    y_pos - odir*y_bottom,
                                    y_pos - odir*y_bottom + dir * w,
                                    y_pos_next - odir*margin + dir * w };

            // spawn boxes
            List<Vector2[]> boxes = new List<Vector2[]> { top_box, bottom_box };
            foreach (var vertexgroup in boxes)
            {
                GameObject box = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], x_pos, Quaternion.identity);
                var poly = box.AddComponent<PolygonCollider2D>();
                poly.SetPath(0, vertexgroup);

                box.GetComponent<Rigidbody2D>().velocity = vel;
                //vel += odir * UnityEngine.Random.Range(0.01f, 0.1f);

                var body = poly.GetComponent<Rigidbody2D>();
                poly.GetComponent<Shatter>().SetMassFromDensity(5.0f);
            }

            x_pos += dir * (w);
        }

        _context.camera.SetState(SmoothCamera.CameraState.Still);
        yield return new WaitForSeconds(5f);
    }
}
