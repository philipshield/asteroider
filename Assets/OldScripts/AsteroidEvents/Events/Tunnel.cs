﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tunnel : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var intensity = Systems.Instance.IntensitySystem;

        //slow down before start
        _context.camera.SetState(SmoothCamera.CameraState.Still);
        yield return new WaitForSeconds(4.0f);

        float tunnelwidth = 50 - intensity.Linear(0, 30);
        float boxwidth = 100;
        float boxheight = 40;
        float spawnmargin = _context.radius * 1.6f;

        //create two big boxes
        List<Vector2[]> boxes = new List<Vector2[]>();
        for (int i = 0; i < 2; i++)
        {
            // 2 ----- 1
            // |   x   |
            // 3 ----- 0

            boxes.Add(new Vector2[4]);
            boxes[i][0] = new Vector2(boxwidth / 2, boxheight / 2);
            boxes[i][1] = new Vector2(boxwidth / 2, -boxheight / 2);
            boxes[i][2] = new Vector2(-boxwidth / 2, -boxheight / 2);
            boxes[i][3] = new Vector2(-boxwidth / 2, boxheight / 2);
        }

        // from the right, tunnelwidth apart
        float angle = UnityEngine.Random.Range(0, 360);
        Quaternion rot = Quaternion.Euler(0, 0, angle);
        Vector2 dir = rot * new Vector2(1, 0);

        Vector2 odir = new Vector2(-dir.y, dir.x);
        Vector2 pos = _context.center + -dir * (spawnmargin) - odir * (boxheight / 2 + tunnelwidth / 2);
        Vector2 vel = dir * UnityEngine.Random.Range(16.0f, 23.3f);


        //control camera
        _context.camera.SetState(SmoothCamera.CameraState.ExternalControl);
        _context.camera.SetDesiredVelocity(vel * UnityEngine.Random.Range(-1.0f, 1.0f) * 0.9f);
        yield return new WaitForSeconds(1.3f); //more camera just before the tunnel appears

        float closinginspeed = intensity.Linear(1.3f, 1.7f);
        vel += odir * closinginspeed;

        float maxTilt = intensity.Linear(1f, 4f);
        float tiltangle = UnityEngine.Random.Range(-maxTilt, maxTilt);

        Vector2 tunneloffset = odir * (boxheight / 2 + tunnelwidth + boxheight / 2);
        foreach (Vector2[] path in boxes)
        {
            GameObject box = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, rot);
            Quaternion tilt = Quaternion.Euler(0, 0, -tiltangle);
            box.transform.rotation *= tilt;

            var poly = box.AddComponent<PolygonCollider2D>();
            poly.SetPath(0, boxes[0]);

            box.GetComponent<Rigidbody2D>().velocity = vel;
            box.GetComponent<Rigidbody2D>().angularVelocity =
                Mathf.Sign(tiltangle) * intensity.Linear(0f, 2f) * UnityEngine.Random.Range(0.07f, 0.3f);

            pos += tunneloffset;
            tiltangle = -tiltangle;
            vel -= 2 * odir * closinginspeed;

            //add mass
            var body = poly.GetComponent<Rigidbody2D>();
            poly.GetComponent<Shatter>().SetMassFromDensity(5.0f);
        }

        //during the epic "fall", create some more events:

        yield return new WaitForSeconds(3.0f);
        _context.camera.SetState(SmoothCamera.CameraState.Still);

        yield return new WaitForSeconds(10f); // "idle" before next event        

    }
}
