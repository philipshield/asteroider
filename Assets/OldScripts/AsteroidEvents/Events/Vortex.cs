﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Vortex : AsteroidEvent
{
    public override IEnumerator PerformEvent(NewAsteroidField.Context _context)
    {
        var intensity = Systems.Instance.IntensitySystem;

        // wait for things to clear a bit (harder if fast but still pretty clear?)
        // TODO: add a "wait until calm thing?"
        yield return new WaitForSeconds(8.0f);

        // count 
        int mincount = (int)intensity.Exp(5.0f, 30.0f);
        int maxcount = (int)(intensity.Exp(10.0f, 50.0f));
        int N = UnityEngine.Random.Range(mincount, maxcount);

        float angle = UnityEngine.Random.Range(0, 360);

        // radial span
        float minradspan = intensity.Linear(180, 360);
        float maxradspan = intensity.Linear(270, 360 * 1.75f);
        float radialspan = UnityEngine.Random.Range(minradspan, maxradspan);
        float step = radialspan / N;

        // spacing
        float seconds = intensity.Linear(2.0f, 0.7f); // total time for vortex wave
        float pause = (seconds / N);

        // speed
        float speed = UnityEngine.Random.Range(13.0f, 20.0f);

        for (int i = 0; i < N; i++)
        {
            Quaternion radialrot = Quaternion.Euler(0, 0, angle);
            Vector2 dir = radialrot * Vector2.up;
            Vector2 pos = _context.center + dir * _context.radius;
            Vector2 vel = -dir * speed;
            float angvel = UnityEngine.Random.Range(0.1f, 6.0f);

            GameObject asteroid = (GameObject)Instantiate(_context.asteroidPrefabs[AsteroidPrefabType.FullyBreakable], pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
            AsteroidHelper.CreateRandomAsteroid(asteroid, AsteroidHelper.AsteroidType.SimpleSmall);


            asteroid.GetComponent<Rigidbody2D>().velocity = vel;
            asteroid.GetComponent<Rigidbody2D>().angularVelocity = angvel;
            asteroid.GetComponent<Shatter>().SetMassFromDensity(0.8f);

            angle += step;
            yield return new WaitForSeconds(pause);
        }

        yield return new WaitForSeconds(UnityEngine.Random.Range(intensity.Exp(4.0f, 0.0f), 5.0f));
    }
}
