﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(ProbabilityWeight))]
public class ProbabilityWeightEditor : Editor
{
    enum View
    {
        Event,
        Intensity
    }

    View m_view;

    public override void OnInspectorGUI()
    {
        ProbabilityWeight probWeight = (ProbabilityWeight)target;

        var weights = probWeight.Weights;
        var events = probWeight.GetComponents<AsteroidEvent>();

        for (int i = 0; i < events.Length && i < weights.Count;)
        {
            if (weights[i].asteroidEvent != events[i])
            {
                weights.RemoveAt(i);
            }
            else
            {
                i++;
            }
        }

        for (int i = 0; i < weights.Count - events.Length; i++)
        {
            weights.RemoveAt(weights.Count - 1);
        }

        for (int i = weights.Count; i < events.Length; i++)
        {
            weights.Add(new ProbabilityWeight.Entry()
            {
                asteroidEvent = events[i],
                weights = new List<ProbabilityWeight.Entry.Weight>()
            });
        }

        m_view = (View)EditorGUILayout.EnumPopup("Group by", m_view);
        var values = System.Enum.GetValues(typeof(IntensitySystem.IntensityLevel));
        var bold = GUI.skin.label;
        bold.fontStyle = FontStyle.Bold;

        if (m_view == View.Intensity)
        {
            foreach (var value in values)
            {
                IntensitySystem.IntensityLevel intensity = (IntensitySystem.IntensityLevel)value;

                EditorGUILayout.LabelField(value.ToString(), bold);
                foreach (var entry in weights)
                {
                    var w = entry.weights.FirstOrDefault(e => e.intensity == intensity);
                    if (w == null)
                    {
                        w = new ProbabilityWeight.Entry.Weight()
                        {
                            intensity = intensity,
                            weight = 1
                        };

                        entry.weights.Add(w);
                    }

                    w.weight = EditorGUILayout.IntField(entry.asteroidEvent.GetType().Name, w.weight);
                }
            }
        }
        else
        {
            foreach (var entry in weights)
            {
                EditorGUILayout.LabelField(entry.asteroidEvent.GetType().Name, bold);

                foreach (var value in values)
                {
                    IntensitySystem.IntensityLevel intensity = (IntensitySystem.IntensityLevel)value;

                    var w = entry.weights.FirstOrDefault(e => e.intensity == intensity);
                    if (w == null)
                    {
                        w = new ProbabilityWeight.Entry.Weight()
                        {
                            intensity = intensity,
                            weight = 1
                        };

                        entry.weights.Add(w);
                    }

                    w.weight = EditorGUILayout.IntField(intensity.ToString(), w.weight);
                }
            }
        }
    }
}

