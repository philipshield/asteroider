﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

enum PointOrientation { COLINEAR, CLOCKWISE, COUNTERCLOCKWISE };

public class AsteroidHelper
{
    public enum AsteroidType
    {
        Simple,
        SimpleSmall,
        SimpleHuge,
        RandomConvex,
        Weird,
        Stick,
        Concave
    }

    static public Vector2[] SimplePolygonPath(float minsize, float maxsize, Vector2? scale = null)
    {
        if (scale == null) scale = Vector2.one;

        int count = UnityEngine.Random.Range(5, 10);
        float size = UnityEngine.Random.Range(minsize, maxsize);
        Vector2[] path = new Vector2[count];

        float anglerand = (((2.0f * Mathf.PI) / count) / 2.0f) * 0.8f;
        for (int i = 0; i < count; i++)
        {
            float angle = 2.0f * Mathf.PI * i / (float)count;
            angle += UnityEngine.Random.Range(-anglerand, anglerand);

            path[i] = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * size;
        }

        for (int i = 0; i < count; i++)
        {
            path[i].Scale(scale.Value);
        }

        return path;
    }

    static public List<Vector2> ConvexHull(List<Vector2> P)
    {
        int N = P.Count;
        List<Vector2> hull = new List<Vector2>();
        if (N == 1)
        {
            return new List<Vector2>() { P[0] };
        }
        if (N == 2)
        {
            if (P[0] == P[1])
            {
                hull.Add(P[0]);
                return hull;
            }
            hull.Add(P[0]);
            hull.Add(P[1]);
            return hull;
        }

        //find lowest point
        Vector2 start = new Vector2(0, float.MaxValue / 2);
        List<Vector2> points = new List<Vector2>();
        for (int i = 0; i < N; i++)
        {
            points.Add(P[i]);
            if (P[i].y < start.y)
            {
                start = P[i];
                continue;
            }
            if ((P[i]).y == start.y)
            {
                if ((P[i]).x < start.x)
                {
                    start = P[i];
                    continue;
                }
            }
        }

        // Sort by angle (or distance)
        points.Sort(new FuncComparer<Vector2>((a, b) =>
        {
            return Vector2.Angle(start, a).CompareTo(Vector2.Angle(start, b));
            //if (Vector2.Angle(start, a) == Vector2.Angle(start,b))
            //{
            //    return Vector2.Distance(start, a).CompareTo(Vector2.Distance(start, b));
            //}
            //return Vector2.Angle(start, a).CompareTo(Vector2.Angle(start, b));
        }));

        //helper. get secondmost top item
        Func<Stack<Vector2>, Vector2> top2 = (stack) =>
        {
            Vector2 first = stack.Pop();
            Vector2 second = stack.Peek();
            stack.Push(first);
            return second;
        };

        Func<Vector2, Vector2, Vector2, PointOrientation> orientation = (p, q, r) =>
        {
            int orien = (int)((q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y));
            if (orien == 0) return PointOrientation.COLINEAR;
            return (orien > 0) ? PointOrientation.CLOCKWISE : PointOrientation.COUNTERCLOCKWISE; // clockwise or counterclockwise
        };

        //Find convex hull
        Stack<Vector2> S = new Stack<Vector2>();
        S.Push(points[0]);
        S.Push(points[1]);
        S.Push(points[2]);

        for (int i = 3; i < N; i++)
        {
            while (orientation(top2(S), S.Peek(), points[i]) != PointOrientation.COUNTERCLOCKWISE)
            {
                if (S.Count != 0)
                {
                    S.Pop();
                }
            }
            S.Push(points[i]);
        }

        //stack, S, holds the hull
        return S.ToList();
    }

    static public void CreateAsteroid(GameObject obj, List<Vector2[]> paths)
    {
        foreach (var vertices in paths)
        {
            var poly = obj.AddComponent<PolygonCollider2D>();
            poly.SetPath(0, vertices);
        }

        obj.GetComponent<Shatter>().SetMassFromDensity(0.2f);
    }

    static public void CreateRandomAsteroid(GameObject obj, AsteroidType type)
    {
        switch (type)
        {
            case AsteroidType.Concave:
                {
                    List<Vector2[]> paths = CreateVoronoiAsteroid(UnityEngine.Random.Range(3, 7), UnityEngine.Random.Range(5.0f, 15.0f));
                    CreateAsteroid(obj, paths);
                }
                break;
            case AsteroidType.Simple:
                {
                    Vector2[] path = SimplePolygonPath(2.0f, 7.0f);
                    CreateAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            case AsteroidType.SimpleSmall:
                {
                    Vector2[] path = SimplePolygonPath(0.6f, 1.7f);
                    CreateAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            case AsteroidType.RandomConvex:
                {
                    float maxR = 5.0f;
                    int N = 5; //UnityEngine.Random.Range(5, 24);
                    List<Vector2> P = new List<Vector2>();
                    for (int i = 0; i < N; i++)
                        P.Add(UnityEngine.Random.insideUnitCircle * maxR);

                    //find lowest point
                    Vector2 start = new Vector2(0, float.MaxValue / 2);
                    for (int i = 0; i < N; i++)
                    {
                        if (P[i].y < start.y)
                        {
                            start = P[i];
                            continue;
                        }
                        if ((P[i]).y == start.y)
                        {
                            if ((P[i]).x < start.x)
                            {
                                start = P[i];
                                continue;
                            }
                        }
                    }

                    P.Sort(new FuncComparer<Vector2>((a, b) =>
                    {
                        return Vector2.Angle(start, a).CompareTo(Vector2.Angle(start, b));
                    }));


                    //List<Vector2> hull = convexHull();
                    //createAsteroid(obj, P.ToArray());
                }
                break;
            case AsteroidType.SimpleHuge:
                {
                    Vector2[] path = SimplePolygonPath(15.0f, 25.0f);
                    CreateAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            case AsteroidType.Stick:
                {
                    float w = 0.5f * UnityEngine.Random.Range(7.0f, 16.0f);
                    float h = 0.5f * UnityEngine.Random.Range(2.0f, 4.0f);
                    //Vector2[] path = new Vector2[4];
                    //path[0].x = -w;
                    //path[0].y = -h;
                    //path[1].x = w;
                    //path[1].y = -h;
                    //path[2].x = w;
                    //path[2].y = h;
                    //path[3].x = -w;
                    //path[3].y = h;
                    //
                    //for (int i = 0; i < path.Length; i++)
                    //{
                    //    path[i] += UnityEngine.Random.insideUnitCircle * h * 0.45f;
                    //}



                    Vector2[] path = SimplePolygonPath(1, 1, new Vector2(w, h));

                    CreateAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            default:
                {
                    throw new NotImplementedException();
                }
        }
    }

    static List<Voronoi2.GraphEdge> edges;
    static Dictionary<int, List<Vector2>> sites = new Dictionary<int, List<Vector2>>();
    static SortedDictionary<float, List<Vector2>> sortedSites = new SortedDictionary<float, List<Vector2>>();
    static public List<Vector2[]> CreateVoronoiAsteroid(int numParts, float size)
    {
        Voronoi2.Voronoi voronoi = new Voronoi2.Voronoi(0.1);

        int numPoints = numParts * 10;

        double[] x = new double[numPoints];
        double[] y = new double[numPoints];

        for (int i = 0; i < numPoints; i++)
        {
            x[i] = UnityEngine.Random.Range(-size, size);
            y[i] = UnityEngine.Random.Range(-size, size);
        }

        sortedSites.Clear();
        sites.Clear();
        edges = voronoi.generateVoronoi(x, y, -size * 1.4f, size * 1.4f, -size * 1.4f, size * 1.4f);
        List<Vector2> points = new List<Vector2>();
        foreach (var edge in edges)
        {

            List<Vector2> l1;
            if (!sites.TryGetValue(edge.site1, out l1))
            {
                l1 = new List<Vector2>();
                sites[edge.site1] = l1;
            }
            l1.Add(new Vector2((float)edge.x1, (float)edge.y1));
            l1.Add(new Vector2((float)edge.x2, (float)edge.y2));

            List<Vector2> l2;
            if (!sites.TryGetValue(edge.site2, out l2))
            {
                l2 = new List<Vector2>();
                sites[edge.site2] = l2;
            }
            l2.Add(new Vector2((float)edge.x1, (float)edge.y1));
            l2.Add(new Vector2((float)edge.x2, (float)edge.y2));
        }

        foreach (var entry in sites)
        {
            var list = entry.Value;
            Vector2 center = Vector2.zero;
            int centerCount = 0;
            List<Vector2> newList = new List<Vector2>();

            for (int i = 0; i < list.Count; i++)
            {
                bool exists = false;
                for (int j = 0; j < i; j++)
                {
                    if (Mathf.Approximately(list[i].x, list[j].x) && Mathf.Approximately(list[i].x, list[j].x))
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists)
                    continue;

                newList.Add(list[i]);
                center += list[i];
                centerCount++;
            }

            center /= centerCount;
            float distSq = center.magnitude;
            sortedSites.Add(distSq, newList);
        }

        List<Vector2[]> result = new List<Vector2[]>();
        {
            int i = 0;
            foreach (var site in sortedSites)
            {
                if (i >= numParts)
                    break;

                SortCCW(site.Value);
                result.Add(site.Value.ToArray());


                ++i;
            }
        }
        return result;
    }

    static public void SortCCW(List<Vector2> vertices)
    {
        Vector2 center = Vector2.zero;

        foreach (var v in vertices)
            center += v;

        center /= vertices.Count;

        vertices.Sort((v1, v2) =>
        {
            Vector2 v = v1 - center;
            float a = Mathf.Atan2(v.y, v.x);
            v = v2 - center;
            float b = Mathf.Atan2(v.y, v.x);

            return a.CompareTo(b);
        });
    }

    public class FuncComparer<T> : IComparer<T>
    {
        private readonly Func<T, T, int> func;
        public FuncComparer(Func<T, T, int> comparerFunc)
        {
            this.func = comparerFunc;
        }

        public int Compare(T x, T y)
        {
            return this.func(x, y);
        }
    }
}
