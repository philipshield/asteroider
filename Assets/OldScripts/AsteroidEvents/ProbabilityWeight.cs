﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ProbabilityWeight : MonoBehaviour
{
    [System.Serializable]
    public class Entry
    {
        [System.Serializable]
        public class Weight
        {
            public IntensitySystem.IntensityLevel intensity;
            public int weight;
        }

        public AsteroidEvent asteroidEvent;
        public List<Weight> weights;
    }

    [SerializeField]
    List<Entry> m_weights;

    public List<Entry> Weights { get { return m_weights; } }

    /// <summary>
    /// Gets the probability weight for the given event and intensity level. Returns 0 if there is no valid entry.
    /// </summary>
    /// <param name="_event">The asteroid event</param>
    /// <param name="_intensity">The intensity level</param>
    /// <returns>An integer with the probability weight. 0 if there is no valid entry.</returns>
    public int GetWeight(AsteroidEvent _event, IntensitySystem.IntensityLevel _intensity)
    {
        var entry = m_weights.FirstOrDefault(e => e.asteroidEvent == _event);

        if(entry != null)
        {
            var weight = entry.weights.FirstOrDefault(w => w.intensity == _intensity);
            if (weight != null)
                return weight.weight;
        }

        return 0;
    } 
}
