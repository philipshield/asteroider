﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class KeyboardPlayerInput : PlayerInput
{
    public enum KeyboardInputKey
    {
        Left,
        Right,
        Up,
        Down,
        Fire,
        Start,
        Join,
        Unjoin,
        Menu,
        Special,
        ChangeColor,
        Boost
    }

    [Serializable]
    public struct MappingEntry
    {
        public KeyboardInputKey key;
        public KeyCode keyCode;
    }

    [SerializeField]
    MappingEntry[] m_mappingData;

    Dictionary<KeyboardInputKey, KeyCode> m_mapping;

    void Start()
    {
		SetUniqueId();
        m_mapping = new Dictionary<KeyboardInputKey, KeyCode>();

        foreach (var entry in m_mappingData)
        {
            m_mapping[entry.key] = entry.keyCode;
        }
    }

    bool GetKeyDown(KeyboardInputKey key)
    {
        KeyCode keyCode;
        if (m_mapping.TryGetValue(key, out keyCode))
        {
            return Input.GetKeyDown(keyCode);
        }
        else
        {
            return false;
        }
    }

    bool GetKeyUp(KeyboardInputKey key)
    {
        KeyCode keyCode;
        if (m_mapping.TryGetValue(key, out keyCode))
        {
            return Input.GetKeyUp(keyCode);
        }
        else
        {
            return false;
        }
    }

    bool GetKey(KeyboardInputKey key)
    {
        KeyCode keyCode;
        if (m_mapping.TryGetValue(key, out keyCode))
        {
            return Input.GetKey(keyCode);
        }
        else
        {
            return false;
        }
    }

    public void Init(Dictionary<KeyboardInputKey, KeyCode> mapping)
    {
        m_mapping = mapping;
    }

    public override Vector2 Throttle
    {
        get
        {
            Vector2 result = Vector2.zero;

            if (GetKey(KeyboardInputKey.Left))
                result -= Vector2.right;
            if (GetKey(KeyboardInputKey.Right))
                result += Vector2.right;
            if (GetKey(KeyboardInputKey.Up))
                result += Vector2.up;
            if (GetKey(KeyboardInputKey.Down))
                result -= Vector2.up;

            return result.normalized;
        }
    }

    public override Vector2 Aim
    {
        get
        {
            return Vector2.zero;
        }
    }

    public override float Boost
    {
        get
        {
            return GetKey(KeyboardInputKey.Boost) ? 1.0f : 0.0f;
        }
    }
	
	public override event Action<PlayerInput, Vector2> OnFireRope;
	public override event Action<PlayerInput> OnReleaseRope;
	public override event Action<PlayerInput> OnJoin;
	public override event Action<PlayerInput> OnUnjoin;
	public override event Action<PlayerInput> OnStart;
    public override event Action<PlayerInput> OnMenu;
    public override event Action<PlayerInput> OnSpecial;
    public override event Action<PlayerInput> OnChangeColor;
    public override event Action<PlayerInput, bool> OnToggleZoom;
    public override event Action<PlayerInput, MenuDirection> OnMenuNavigation;

    void Update()
    {
        if (GetKeyDown(KeyboardInputKey.Fire))
        {
            if (OnFireRope != null)
				OnFireRope(this, Aim);
        }

        if (GetKeyUp(KeyboardInputKey.Fire))
        {
            if (OnReleaseRope != null)
				OnReleaseRope(this);
        }

        if (GetKeyDown(KeyboardInputKey.Join))
        {
            if (OnJoin != null)
				OnJoin(this);
        }

        if (GetKeyDown(KeyboardInputKey.Unjoin))
        {
            if (OnUnjoin != null)
				OnUnjoin(this);
        }

        if (GetKeyDown(KeyboardInputKey.Start))
        {
            if (OnStart != null)
				OnStart(this);
        }

        if (GetKeyDown(KeyboardInputKey.Menu))
        {
            if (OnMenu != null)
				OnMenu(this);
        }

        if (GetKeyDown(KeyboardInputKey.Right))
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Right);
        }
        if (GetKeyDown(KeyboardInputKey.Left))
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Left);
        }
        if (GetKeyDown(KeyboardInputKey.Up))
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Up);
        }
        if (GetKeyDown(KeyboardInputKey.Down))
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Down);
        }

    }
}
