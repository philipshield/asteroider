﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameInput : MonoBehaviour 
{
    [SerializeField]
    InputManager m_inputManager;

    [SerializeField]
    bool m_controllerDebugView = false;

    List<PlayerInput> m_inputs = new List<PlayerInput>();

	void Start () 
    {
        var predefInputs = GetComponentsInChildren<PlayerInput>();

        int count = m_inputManager.GetControllerCount();
        for (int i = 0; i < count; i++)
        {
            var controller = m_inputManager.GetController(i);
            GameObject controllerGO = new GameObject();
            controllerGO.name = "Player Controller Input " + (i + 1);
            controllerGO.transform.parent = transform;
            var input = controllerGO.AddComponent<ControllerPlayerInput>();
            input.Init(controller);

            if (m_controllerDebugView)
                controllerGO.AddComponent<ControllerVisualizer>();

            m_inputs.Add(input);
        }
		
		foreach (var input in predefInputs)
		{
			m_inputs.Add(input);
		}
	}

    public List<PlayerInput> PlayerInputs { get { return m_inputs; } }
}
