﻿using UnityEngine;
using System.Collections;

public class ControllerVisualizer : MonoBehaviour
{
    IController m_controller;
    ControllerPlayerInput m_input;

    void Start()
    {
        m_input = GetComponent<ControllerPlayerInput>();
        m_controller = m_input.GetRawController();
    }

    void Update()
    {

    }

    void OnGUI()
    {
        Vector2 offset = Vector2.right * m_input.Id * 100 ;

        for (int i = 0; i < 10; i++)
        {
            bool status = m_controller.GetButton(i);

            Rect r = new Rect(offset.x, offset.y, 100, 40);
            offset.y += 20;
            GUI.Label(r, "B "+i + ": " + (status ? "Down" : "Up"));
        }

        for (int i = 0; i < 3; i++)
        {
            Rect r = new Rect(offset.x, offset.y, 100, 40);
            offset.y += 20;
            GUI.Label(r, "J " + i + ": " + m_controller.GetJoystick(i));
        }

        for (int i = 0; i < 3; i++)
        {
            Rect r = new Rect(offset.x, offset.y, 100, 40);
            offset.y += 20;
            GUI.Label(r, "T " + i + ": " + m_controller.GetTrigger(i));
        }
    }
}
