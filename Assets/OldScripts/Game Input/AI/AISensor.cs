﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AISensor : MonoBehaviour
{
    #region Singleton implementation
    public static AISensor Instance
    {
        get
        {
            if (s_instance == null)
            {
                GameObject gobj = new GameObject("AISensor");
                s_instance = gobj.AddComponent<AISensor>();
            }

            return s_instance;
        }
    }

    static AISensor s_instance;

    void Awake()
    {
        if (s_instance == null)
        {
            s_instance = this;
            GameObject.DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    #endregion

    void Start()
    {
        EventManager.RegisterEvent<GameStartedEventArgs>(OnGameStarted);
        EventManager.RegisterEvent<GameEndedEventArgs>(OnGameEnded);

        AsteroidUpdateFrequency = 0.2f;
        AsteroidList = new List<AsteroidInfo>();
        Players = new Dictionary<int, Player>();
    }

    void OnDestroy()
    {
        EventManager.UnregisterEvent<GameStartedEventArgs>(OnGameStarted);
        EventManager.UnregisterEvent<GameEndedEventArgs>(OnGameEnded);
    }

    public class AsteroidInfo
    {
        public GameObject m_gameObject;
        public Vector2 m_position;
        public Vector2 m_velocity;
        public float m_radius;
        public float m_angularVel;
        public Vector2 m_predictedPos;
        public float m_mass;
    }

    public float AsteroidUpdateFrequency { get; set; }

    public Dictionary<int, Player> Players { get; private set; }

    float m_asteroidUpdateTimer;
    bool m_isInGame = false;

    public List<AsteroidInfo> AsteroidList { get; private set; }
    public float TimeSinceLastAsteroidUpdate { get { return m_asteroidUpdateTimer; } }

    void OnGameStarted(GameStartedEventArgs args)
    {
        Players = args.Players;

        m_isInGame = true;
        m_asteroidUpdateTimer = 0.0f;
        AsteroidList.Clear();
    }

    void OnGameEnded(GameEndedEventArgs args)
    {
        m_isInGame = false;
        AsteroidList.Clear();
    }

    void FixedUpdate()
    {
        if (!m_isInGame)
            return;

        m_asteroidUpdateTimer += Time.deltaTime;
        if (m_asteroidUpdateTimer > AsteroidUpdateFrequency)
        {
            m_asteroidUpdateTimer = 0.0f;
            UpdateAsteroids();
        }

        foreach (var asteroid in AsteroidList)
        {
            asteroid.m_predictedPos = asteroid.m_position + m_asteroidUpdateTimer * asteroid.m_velocity;
        }
    }

    void UpdateAsteroids()
    {
        AsteroidList.Clear();

        var shatterObjs = GameObject.FindObjectsOfType<Shatter>();
        foreach (var shatter in shatterObjs)
        {
            if (!shatter.IsDangerous)
                continue;

            if (shatter.GetComponent<Spaceship>() == null)
            {
                AsteroidInfo info = new AsteroidInfo();
                info.m_gameObject = shatter.gameObject;

                var body = shatter.GetComponent<Rigidbody2D>();
                info.m_velocity = body.velocity;
                info.m_angularVel = body.angularVelocity;
                info.m_mass = body.mass;

                var polys = shatter.GetComponents<PolygonCollider2D>();

                Vector2 center = Vector2.zero;
                int centerCount = 0;
                foreach (var poly in polys)
                {
                    foreach (Vector2 p in poly.GetPath(0))
                    {
                        center += p;
                        centerCount += 1;
                    }
                }

                if (centerCount == 0)
                {
                    info.m_position = body.position;
                }
                else
                {
                    center /= centerCount;
                    info.m_position = body.GetRelativePoint(center);
                }

                float maxRadiusSq = 0.0f;
                foreach (var poly in polys)
                {
                    foreach (Vector2 p in poly.GetPath(0))
                    {
                        float radiusSq = (center - p).sqrMagnitude;
                        if (radiusSq > maxRadiusSq)
                            maxRadiusSq = radiusSq;
                    }
                }
                info.m_radius = Mathf.Sqrt(maxRadiusSq);

                AsteroidList.Add(info);
            }
        }
    }

    void OnDrawGizmos()
    {
        if (AsteroidList == null)
            return;


        foreach (var asteroid in AsteroidList)
        {
            Gizmos.color = new Color(1, 1, 1, 0.4f);
            Gizmos.DrawWireSphere(asteroid.m_position + asteroid.m_velocity * m_asteroidUpdateTimer, asteroid.m_radius);
            Gizmos.color = new Color(1, 1, 0, 0.4f);
            Gizmos.DrawWireSphere(asteroid.m_position + asteroid.m_velocity * (0.5f + m_asteroidUpdateTimer), asteroid.m_radius);
        }
    }
}
