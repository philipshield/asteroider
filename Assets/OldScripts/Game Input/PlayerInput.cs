﻿using UnityEngine;
using System.Collections;
using System;

public abstract class PlayerInput : MonoBehaviour
{
    public enum MenuDirection
    { 
        Left, Right, Up, Down
    }

	private static int s_uniqueId = 0;
	protected void SetUniqueId()
	{
		Id = s_uniqueId++;
	}

	public int Id { get; private set; }

    public abstract Vector2 Throttle { get; }
    public abstract Vector2 Aim { get; }
    public abstract float Boost { get; }

	public abstract event Action<PlayerInput, Vector2> OnFireRope;
    public abstract event Action<PlayerInput> OnReleaseRope;
	public abstract event Action<PlayerInput> OnJoin;
	public abstract event Action<PlayerInput> OnUnjoin;
	public abstract event Action<PlayerInput> OnStart;
	public abstract event Action<PlayerInput> OnMenu;
    public abstract event Action<PlayerInput> OnSpecial;
    public abstract event Action<PlayerInput> OnChangeColor;
    public abstract event Action<PlayerInput, bool> OnToggleZoom;
    public abstract event Action<PlayerInput, MenuDirection> OnMenuNavigation;

}