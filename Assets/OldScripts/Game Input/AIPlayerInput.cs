﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class AIPlayerInput : PlayerInput
{
#region Tweak fields
    [SerializeField]
    int m_AIId = 1;

    [Header("Border avoidance")]
    [SerializeField]
    float m_borderDistance = 15.0f;
    [SerializeField]
    float m_borderStrength = 5.0f;

    [Header("Camera following")]
    [SerializeField]
    float m_cameraVelDistance = 10.0f;
    [SerializeField]
    float m_cameraVelBorderStrength = 0.002f;

    [Header("Random steering")]
    [SerializeField]
    float m_randomSteeringFactor = 0.02f;
    [SerializeField]
    float m_randomSteeringDistanceFromCenter = 10.0f;
    [SerializeField]
    float m_randomSteeringMaxThrottle = 0.5f;
    [SerializeField]
    float m_randomSteeringWeightUpdateMinFrequency = 1.5f;
    [SerializeField]
    float m_randomSteeringWeightUpdateMaxFrequency = 4.5f;

    [Header("Asteroid avoidance")]
    [SerializeField]
    float m_asteroidAvoidanceRadiusOffset = 5.0f;
    [SerializeField]
    float m_asteroidAvoidanceStrength = 1.0f;
    [SerializeField]
    float m_asteroidAvoidanceMaxThrottle = 1.0f;

    [Header("Predicted asteroid avoidance")]
    [SerializeField]
    float m_predictedAsteroidAvoidanceRadiusOffset = 5.0f;
    [SerializeField]
    float m_predictedAsteroidAvoidanceStrength = 1.0f;
    [SerializeField]
    float m_predictedAsteroidAvoidanceMaxThrottle = 0.7f;
    [SerializeField]
    float m_predictedAsteroidAvoidanceTime = 0.4f;
#endregion


#region Private members
    Player m_player;
    Vector2 m_throttle;
    AISensor m_sensor;

    bool m_joinToggle = true;
    bool m_isInGame = false;
    SmoothCamera m_camera;
    Vector2 m_steering;
    Vector2 m_steeringWeight;
    float m_steeringTimer = 0.0f;
#endregion

    #region PlayerInput implementation
    public override Vector2 Throttle
    {
        get
        {
            return m_throttle;
        }
    }

    public override Vector2 Aim
    {
        get
        {
            return Vector2.zero;
        }
    }

    public override float Boost
    {
        get
        {
            return 0.0f;
        }
    }

    public override event Action<PlayerInput, Vector2> OnFireRope;
    public override event Action<PlayerInput> OnReleaseRope;
    public override event Action<PlayerInput> OnJoin;
    public override event Action<PlayerInput> OnUnjoin;
    public override event Action<PlayerInput> OnStart;
    public override event Action<PlayerInput> OnMenu;
    public override event Action<PlayerInput> OnSpecial;
    public override event Action<PlayerInput> OnChangeColor;
    public override event Action<PlayerInput, bool> OnToggleZoom;
    public override event Action<PlayerInput, MenuDirection> OnMenuNavigation;
    
    #endregion

    void Start () 
    {
        SetUniqueId();

        EventManager.RegisterEvent<GameStartedEventArgs>(OnGameStarted);
        EventManager.RegisterEvent<GameEndedEventArgs>(OnGameEnded);
	}

    void OnDestroy()
    {
        EventManager.UnregisterEvent<GameStartedEventArgs>(OnGameStarted);
        EventManager.UnregisterEvent<GameEndedEventArgs>(OnGameEnded);
    }

    void OnGameStarted(GameStartedEventArgs args)
    {
        if(!args.Players.TryGetValue(Id, out m_player))
            return;

        m_isInGame = true;

        m_camera = GameObject.FindObjectOfType<SmoothCamera>();
    }

    void OnGameEnded(GameEndedEventArgs args)
    {
        m_isInGame = false;
    }
	
	void Update () 
    {
        if (Input.GetKeyDown((KeyCode)((int)KeyCode.Alpha0 + m_AIId)))
        {
            m_sensor = AISensor.Instance;

            if (m_joinToggle)
            {
                OnJoin(this);
            }
            else
            {
                OnUnjoin(this);
            }

            m_joinToggle = !m_joinToggle;
        }
	}

    void FixedUpdate()
    {
        if (!m_isInGame || m_player == null)
            return;

        var ship = m_player.Ship;
        if (ship == null)
            return;

        Vector2 throttleForce = Vector2.zero;

        //Stay inside screen
        {
            Vector2 topLeft = Camera.main.ViewportToWorldPoint(Vector3.zero);
            Vector2 bottomRight = Camera.main.ViewportToWorldPoint(Vector3.one);

            Vector2 delta = topLeft - ship.m_rigidBody2d.position;
            if (delta.x > -m_borderDistance)
                throttleForce.x += (delta.x + m_borderDistance) * m_borderStrength;
            if (delta.y > -m_borderDistance)
                throttleForce.y += (delta.y + m_borderDistance) * m_borderStrength;

            delta = bottomRight - ship.m_rigidBody2d.position;
            if (delta.x < m_borderDistance)
                throttleForce.x += (delta.x - m_borderDistance) * m_borderStrength;
            if (delta.y < m_borderDistance)
                throttleForce.y += (delta.y - m_borderDistance) * m_borderStrength;

        }

        //Kind of follow camera
        {
            Vector2 fromCenter = ship.m_rigidBody2d.position - (Vector2)Camera.main.ViewportToWorldPoint(Vector3.one * 0.5f);

            var vel = m_camera.GetVelocity();
            float d = Vector2.Dot(vel.normalized, fromCenter);

            if (d < m_cameraVelDistance)
            {
                Vector2 force = vel * (m_cameraVelDistance - d) * m_cameraVelBorderStrength;
                throttleForce += force;

                m_steering = Vector2.Lerp(m_steering, Vector2.ClampMagnitude(force, m_randomSteeringMaxThrottle), Time.fixedDeltaTime);
                m_steering = Vector2.ClampMagnitude(m_steering, m_randomSteeringMaxThrottle);
            }
        }

        //Fly around randomly
        {
            m_steeringTimer -= Time.fixedDeltaTime;
            if (m_steeringTimer < 0.0f)
            {
                m_steeringTimer = UnityEngine.Random.Range(m_randomSteeringWeightUpdateMinFrequency, m_randomSteeringWeightUpdateMaxFrequency);
                m_steeringWeight = UnityEngine.Random.insideUnitCircle;
            }

            Vector2 fromCenter = ship.m_rigidBody2d.position - (Vector2)Camera.main.ViewportToWorldPoint(Vector3.one * 0.5f);
            Vector2 vec = Vector2.zero;

            float mag = fromCenter.magnitude - m_randomSteeringDistanceFromCenter;
            if (mag < 0.0f)
                vec = -fromCenter.normalized * mag;

            m_steering += (UnityEngine.Random.insideUnitCircle + 0.8f * m_steeringWeight) * m_randomSteeringFactor + vec * m_randomSteeringFactor * 15.0f;
            m_steering = Vector2.ClampMagnitude(m_steering, m_randomSteeringMaxThrottle);

            throttleForce += m_steering;
        }
        
        //Avoid nearby asteroids
        {
            Vector2 avoidanceForce = Vector2.zero;

            foreach (var asteroid in m_sensor.AsteroidList)
            {
                Vector2 vec = ship.m_rigidBody2d.position - asteroid.m_predictedPos;
                float distSq = vec.sqrMagnitude;

                float radius = asteroid.m_radius + m_asteroidAvoidanceRadiusOffset;
                float radiusSq = radius * radius;

                if (distSq < radiusSq)
                {
                    float dist = Mathf.Sqrt(distSq);
                    vec /= dist;

                    float t = 1.0f - dist / radius;
                    Vector2 force = t * m_asteroidAvoidanceStrength * vec * asteroid.m_mass;
                    avoidanceForce += force;

                    m_steering += force * 1.0f;
                    m_steering = Vector2.ClampMagnitude(m_steering, m_randomSteeringMaxThrottle);
                }
            }

            avoidanceForce = Vector2.ClampMagnitude(avoidanceForce, m_asteroidAvoidanceMaxThrottle);
        }

        //Avoid incoming asteroids
        {
            Vector2 avoidanceForce = Vector2.zero;

            foreach (var asteroid in m_sensor.AsteroidList)
            {
                Vector2 predictedPos = asteroid.m_position + asteroid.m_velocity * (m_predictedAsteroidAvoidanceTime + m_sensor.TimeSinceLastAsteroidUpdate);
                Vector2 vec = ship.m_rigidBody2d.position - predictedPos;
                float distSq = vec.sqrMagnitude;

                float radius = asteroid.m_radius + m_predictedAsteroidAvoidanceRadiusOffset;
                float radiusSq = radius * radius;

                if (distSq < radiusSq)
                {
                    float dist = Mathf.Sqrt(distSq);

                    Vector2 perpVel = new Vector2(-asteroid.m_velocity.y, asteroid.m_velocity.x).normalized;

                    float push = Vector2.Dot(vec, perpVel);

                    float t = 1.0f - push / radius;
                    Vector2 force = -t * m_predictedAsteroidAvoidanceStrength * perpVel;
                    avoidanceForce += force;

                    m_steering += force * 1.0f;
                    m_steering = Vector2.ClampMagnitude(m_steering, m_randomSteeringMaxThrottle);
                }
            }

            avoidanceForce = Vector2.ClampMagnitude(avoidanceForce, m_predictedAsteroidAvoidanceMaxThrottle);
        }

        m_throttle = Vector2.ClampMagnitude(throttleForce, 1.0f);
    }

  
}
