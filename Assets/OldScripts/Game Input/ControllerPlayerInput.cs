﻿using UnityEngine;
using System.Collections;
using System;

public class ControllerPlayerInput : PlayerInput
{
    IController m_controller;

    bool m_prevCombinedTriggerButtonState = false;
    bool m_prevRightTriggerButtonState = false;
    bool m_rightTrigger = false;
    bool m_prevDpadUp = false;
    bool m_prevDpadDown = false;

    public IController GetRawController()
    {
        return m_controller;
    }

    public override Vector2 Throttle
    {
        get
        {
            return m_controller.GetJoystick(Xbox360ControllerJoystickId.Left);
        }
    }

    public override Vector2 Aim
    {
        get
        {
            return m_controller.GetJoystick(Xbox360ControllerJoystickId.Right);
        }
    }

    public override float Boost
    {
        get
        {
            return Mathf.Max(m_controller.GetButton(Xbox360ControllerButtonId.LB) ? 1.0f : 0.0f);
        }
    }

    public override event Action<PlayerInput, Vector2> OnFireRope;
    public override event Action<PlayerInput> OnReleaseRope;
    public override event Action<PlayerInput> OnJoin;
    public override event Action<PlayerInput> OnUnjoin;
    public override event Action<PlayerInput> OnStart;
    public override event Action<PlayerInput> OnMenu;
    public override event Action<PlayerInput> OnSpecial;
    public override event Action<PlayerInput> OnChangeColor;
    public override event Action<PlayerInput, bool> OnToggleZoom;
    public override event Action<PlayerInput, MenuDirection> OnMenuNavigation;


    public void Init(IController controller)
    {
        SetUniqueId();
        m_controller = controller;
    }

    Vector2 m_prevLeftStick = Vector2.zero;

    void Update()
    {
        Vector2 leftStick = m_controller.GetJoystick(Xbox360ControllerJoystickId.Left);
        leftStick.x += m_controller.GetJoystick(Xbox360ControllerJoystickId.DPad).x;
        leftStick.y -= m_controller.GetJoystick(Xbox360ControllerJoystickId.DPad).y;
        leftStick = Vector2.ClampMagnitude(leftStick, 1);

        if (m_prevLeftStick.x < 0.8f && leftStick.x > 0.8f)
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Right);
        }
        if (m_prevLeftStick.x > -0.8f && leftStick.x < -0.8f)
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Left);
        }
        if (m_prevLeftStick.y < 0.8f && leftStick.y > 0.8f)
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Up);
        }
        if (m_prevLeftStick.y > -0.8f && leftStick.y < -0.8f)
        {
            if (OnMenuNavigation != null)
                OnMenuNavigation(this, MenuDirection.Down);
        }

        m_prevLeftStick = leftStick;

        /*
        bool combinedTrigger = m_controller.GetTrigger(Xbox360ControllerTriggerId.Combined) < -0.5f;
        if (combinedTrigger && !m_prevCombinedTriggerButtonState)
        {
            if (OnFireRope != null)
                OnFireRope(this, Aim);
        }
        if (!combinedTrigger && m_prevCombinedTriggerButtonState)
        {
            if (OnReleaseRope != null)
                OnReleaseRope(this);
        }
        m_prevCombinedTriggerButtonState = combinedTrigger;*/

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.A))
        {
            if (OnJoin != null)
                OnJoin(this);
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.B))
        {
            if (OnUnjoin != null)
                OnUnjoin(this);
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.Start))
        {
            if (OnStart != null)
                OnStart(this);
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.Back))
        {
            if (OnMenu != null)
                OnMenu(this);
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.RB))
        {
            if (OnFireRope != null)
                OnFireRope(this, Aim);
        }
        if (m_controller.GetButtonUp(Xbox360ControllerButtonId.RB))
        {
            if (OnReleaseRope != null)
                OnReleaseRope(this);
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.LB) || m_controller.GetButtonDown(Xbox360ControllerButtonId.LStick))
        {
            if (OnSpecial != null)
                OnSpecial(this);
        }

        if (m_controller.GetButtonDown(Xbox360ControllerButtonId.Y) || m_controller.GetButtonDown(Xbox360ControllerButtonId.LStick))
        {
            if (OnChangeColor != null)
                OnChangeColor(this);
        }

        bool dpadUpPressed = m_controller.GetJoystick(Xbox360ControllerJoystickId.DPad).y < -0.5f;
        if (dpadUpPressed && !m_prevDpadUp)
        {

            if (OnToggleZoom!= null)
                OnToggleZoom(this, true);
        }
        m_prevDpadUp = dpadUpPressed;

        bool dpadDownPressed = m_controller.GetJoystick(Xbox360ControllerJoystickId.DPad).y > 0.5f;
        if (dpadDownPressed && !m_prevDpadDown)
        {
            if (OnToggleZoom != null)
                OnToggleZoom(this, false);
        }
        m_prevDpadDown = dpadDownPressed;
    }
}
