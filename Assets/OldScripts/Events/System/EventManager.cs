﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum EventDelayCategory
{
	Immidiate,
	NextFrame,
	NextFixedUpdate
}

public class EventBase
{

}

public class EventManager
{
	#region Singleton implementation
	public static EventManager Instance
	{
		get
		{
			if(s_instance == null)
				s_instance = new EventManager();

			return s_instance;
		}
	}

	private static EventManager s_instance = null;
    #endregion

    #region Static
    public static void RegisterEvent<T>(Action<T> eventHandler) where T : EventBase
    {
        Instance.Register<T>(eventHandler);
    }

    public static void UnregisterEvent<T>(Action<T> eventHandler) where T : EventBase
    {
        Instance.Unregister<T>(eventHandler);
    }

    public static void SendEvent<T>(T eventArgs, EventDelayCategory delayCategory = EventDelayCategory.Immidiate) where T : EventBase
    {
        Instance.Send<T>(eventArgs, delayCategory);
    }

    public static void SendDelayedEvents(EventDelayCategory category)
    {
        Instance.SendDelayed(category);
    }
    #endregion

    Dictionary<System.Type, Dictionary<int, Action<EventBase>>> m_eventHandlers;
	Dictionary<EventDelayCategory, List<EventBase>> m_delayedEvents;

	private EventManager()
	{
        m_eventHandlers = new Dictionary<Type, Dictionary<int, Action<EventBase>>>();
        m_delayedEvents = new Dictionary<EventDelayCategory, List<EventBase>>();
	}
	
	public void Register<T> (Action<T> eventHandler) where T : EventBase
	{
        Type type = typeof(T);
        Dictionary<int, Action<EventBase>> eventEmitters;
        
		if(!m_eventHandlers.TryGetValue(type, out eventEmitters))
		{
			eventEmitters = new Dictionary<int, Action<EventBase>>();
			m_eventHandlers[type] = eventEmitters;
		}

        Action<EventBase> eventEmitter = (eventArgs) => { eventHandler((T)eventArgs); };
        eventEmitters.Add(eventHandler.GetHashCode(), eventEmitter);
	}
	
	public void Unregister<T> (Action<T> eventHandler) where T : EventBase
    {
        Type type = typeof(T);
		var eventEmitters = m_eventHandlers[type];

        if (eventEmitters == null)
        {
            throw new Exception("[EventManager] Tried to remove handler, but it was not registered: " + type.ToString());
        }
        else
        {
            eventEmitters.Remove(eventHandler.GetHashCode());
        }
	}

	public void Send<T>(T eventArgs, EventDelayCategory delayCategory = EventDelayCategory.Immidiate) where T : EventBase
	{
		if(delayCategory == EventDelayCategory.Immidiate)
		{
            SendImmidiate(eventArgs);
		}
		else
		{
			var delayEventList = m_delayedEvents[delayCategory];

			if(delayEventList == null)
			{
				delayEventList = new List<EventBase>();
				m_delayedEvents[delayCategory] = null;
			}

			delayEventList.Add(eventArgs);
		}
	}

	public void SendDelayed(EventDelayCategory category)
	{
		List<EventBase> delayedEvents;
		
		if(m_delayedEvents.TryGetValue(category, out delayedEvents))
		{
			foreach (var eventArgs in delayedEvents) 
			{
				SendImmidiate(eventArgs);
			}
		}
	}

	private void SendImmidiate(EventBase eventArgs)
	{
		var type = eventArgs.GetType();
		Dictionary<int, Action<EventBase>> eventEmitters;

        if (m_eventHandlers.TryGetValue(type, out eventEmitters))
		{
            foreach (var eventEmitter in eventEmitters) 
			{
                eventEmitter.Value(eventArgs);
			}
		}
	}
}
