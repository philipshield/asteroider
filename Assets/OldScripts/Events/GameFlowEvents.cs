﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameStartedEventArgs : EventBase
{
    public GameStartedEventArgs(Dictionary<int, Player> players)
    {
        Players = players;
    }

    public Dictionary<int, Player> Players { get; private set; }
}

public class GameEndedEventArgs : EventBase
{
    public GameEndedEventArgs(int winner)
    {
        Winner = winner;
    }

    public int Winner { get; private set; }
}