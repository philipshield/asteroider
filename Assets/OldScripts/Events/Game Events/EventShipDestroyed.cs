﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventShipDestroyed : EventBase
{
    Player m_player;
    Spaceship m_ship;
    List<int> m_destroyedBy;

    public EventShipDestroyed(Player _player, Spaceship _ship, List<int> _destroyedBy)
    {
        m_player = _player;
        m_ship = _ship;
        m_destroyedBy = _destroyedBy;
    }

    public List<int> DestroyedBy
    {
        get { return m_destroyedBy; }
    }

    public Player Player
    {
        get { return m_player; }
    }

    public Spaceship Ship
    {
        get { return m_ship; }
    }
}
