﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RopeVisual : MonoBehaviour 
{
    public Rigidbody2D m_nodePrefab;
    public int m_numNodes = 10;

    public float m_straightnessFactor = 0.3f;
    public float m_straightnessLimit = 0.1f;

    List<Rigidbody2D> m_nodes = new List<Rigidbody2D>();

    Rigidbody2D m_first;
    Rigidbody2D m_last;

    LineRenderer m_line;

    public float RestLength 
    { 
        get { return m_restLength; } 
        set 
        { 
            m_restLength = value; 
            m_partRestLength = m_restLength / (m_nodes.Count - 1);

			UpdateRestLength();
        } 
    }

    float m_restLength = 5.0f;
    float m_partRestLength;

    public Vector2 StartPos { get; set; }
    public Vector2 EndPos { get; set; }

	void UpdateRestLength()
	{
		for (int i = 0; i < m_nodes.Count - 1; i++)
		{
			m_nodes[i].GetComponent<DistanceJoint2D>().distance = m_partRestLength;
		}
	}

	void Start () 
    {
        m_line = GetComponent<LineRenderer>();

        for (int i = 0; i < m_numNodes; i++)
        {
            m_nodes.Add((Rigidbody2D)Instantiate(m_nodePrefab, transform.position + (Vector3)Random.insideUnitCircle * 0.1f, Quaternion.identity));
        }

        m_first = m_nodes[0];
        m_last = m_nodes[m_nodes.Count - 1];

        m_first.isKinematic = true;
        m_last.isKinematic = true;

        for (int i = 0; i < m_nodes.Count - 1; i++)
        {
            /*
            var joint = m_nodes[i].gameObject.AddComponent<SpringJoint2D>();
            joint.distance = m_partRestLength;
            joint.collideConnected = false;
            joint.connectedBody = m_nodes[i + 1];
            joint.dampingRatio = 20.2f;
            joint.frequency = 4;*/
            var joint = m_nodes[i].gameObject.AddComponent<DistanceJoint2D>();
            joint.distance = m_partRestLength;
            joint.enableCollision = false;
            joint.maxDistanceOnly = true;
            joint.connectedBody = m_nodes[i + 1];
            m_nodes[i].GetComponent<Rigidbody2D>().position += Random.insideUnitCircle * 0.1f;
        }

        StartPos = new Vector2(0, 0);
        EndPos = new Vector2(0, 5);
        RestLength = 5.0f;

        m_line.SetVertexCount(m_numNodes);

		UpdateRestLength();
		UpdateLine();
	}

    void UpdateLine()
    {
        int i = 0;
        foreach (var node in m_nodes)
        {
            m_line.SetPosition(i, node.transform.position);

            ++i;
        }
    }

    void OnDestroy()
    {
        foreach (var node in m_nodes)
        {
            if(node != null)
                Destroy(node.gameObject);
        }
    }

    void Update()
    {
        UpdateLine();
    }

	void FixedUpdate () 
    {
        m_first.velocity = (StartPos - m_first.position) / Time.fixedDeltaTime;
        m_last.velocity = (EndPos - m_last.position) / Time.fixedDeltaTime;


        float dist = Vector2.Distance(m_first.position, m_last.position);
        float t = Mathf.Clamp01( dist / m_restLength);
        float st = Mathf.Clamp01((t - (1.0f - m_straightnessLimit)) / m_straightnessLimit) * m_straightnessFactor;
        for (int i = 1; i < m_nodes.Count - 1; i++)
        {
            float p = (float)i / (float)(m_nodes.Count - 1);
            Vector2 pos = Vector2.Lerp(m_first.position, m_last.position, p);
            Vector2 vel = Vector2.Lerp(m_first.velocity, m_last.velocity, p);
            m_nodes[i].position = Vector2.Lerp(m_nodes[i].position, pos, st);
            m_nodes[i].velocity = Vector2.Lerp(m_nodes[i].velocity, vel, st);
        }
	}

    public void Straighten(float factor)
    {
        for (int i = 0; i < m_nodes.Count; i++)
        {
            float t = (float)i / (float)(m_nodes.Count - 1);
            m_nodes[i].position = Vector2.Lerp(m_nodes[i].position, Vector2.Lerp(StartPos, EndPos, t), factor);
        }
    }
}
