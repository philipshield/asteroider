﻿using UnityEngine;
using System.Collections;

public class SpriteExplosion : MonoBehaviour 
{
    public float m_startRadius = 0.1f;
    public float m_endRadius = 2.0f;
    public float m_duration = 0.3f;

    public Color m_color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

    float m_timer = 0.0f;

    SpriteRenderer m_renderer;
	void Start () 
    {
        m_renderer = GetComponent<SpriteRenderer>();

        m_color.a = 0.0f;
        m_renderer.material.color = m_color;
	}
	
	void Update () 
    {
        m_timer += Time.deltaTime;

        if (m_timer > m_duration)
        {
            Destroy(this.gameObject);
        }
        else
        {
            float t = Mathf.Clamp01(m_timer / m_duration);

            m_color.a = 1.0f - t * t;
            m_renderer.material.color = m_color;

            float s = Mathf.Lerp(m_startRadius, m_endRadius, t);
            transform.localScale = new Vector3(s, s, s);
        }
	}
}
