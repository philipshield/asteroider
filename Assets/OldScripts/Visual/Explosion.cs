﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{
    public float m_startRadius = 0.0f;
    public float m_endRadius = 5.0f;
    public float m_duration = 0.5f;

    float m_timer = 0.0f;
    Material m_material;
    Color m_color;

    Vector2 m_velocity;

	void Awake() 
    {
        m_material = GetComponent<MeshRenderer>().material;
	}

    public void Init(Color color, Vector2 velocity)
    {
        m_color = color;
        m_material.SetColor("_TintColor", color);
        m_velocity = velocity;
    }
	
    void Update () 
    {
        m_timer += Time.deltaTime;
        float t = Mathf.Clamp01(1.0f - m_timer / m_duration);

        transform.position = transform.position + (Vector3)m_velocity * Time.deltaTime;

        if (t <= 0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            m_color.a = t;
            m_material.SetColor("_TintColor", m_color);

            float s = Mathf.Lerp(m_endRadius, m_startRadius, t);
            transform.localScale = new Vector3(s,s,s);
        }
	}
}
