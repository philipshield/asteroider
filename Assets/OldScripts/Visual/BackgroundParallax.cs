﻿using UnityEngine;
using System.Collections;

public class BackgroundParallax : MonoBehaviour {

    public float m_parallaxFactor = 0.0003f;
    public GameObject m_background;

    private Material m_material;
    private Transform m_transform;
    private Camera m_camera;

	void Start () 
    {
        m_material = m_background.GetComponent<Renderer>().material;
        m_camera = GetComponent<Camera>();
        m_transform = m_background.transform;
	}
	
	void Update () 
    {
        float o = 155.0f;
        float s = (45.0f + o) / (m_camera.orthographicSize + o);
        m_transform.localScale = new Vector3(s * m_camera.orthographicSize * 2.0f, s * m_camera.orthographicSize * 2.0f, 1.0f);

        m_material.SetTextureOffset("_MainTex", (Vector2)transform.position * m_parallaxFactor);
	}
}
