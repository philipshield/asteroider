﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Spaceship : MonoBehaviour 
{
    public Rigidbody2D m_rigidBody2d;

    public float m_maxSpeed = 20.0f;
    public float m_throttleForce = 20.0f;
    public float m_throttleDamping = 5.0f;

    public float m_playerMass = 1.0f;

    public float m_boostMaxSpeed = 30.0f;
    public float m_boostThrottleForce = 30.0f;

    public float m_angularForce = 20.0f;
    public float m_angularDamping = 5.0f;

    public Vector2 Throttle;
    public Vector2 Aim;
    public float Boost = 0.0f;
    float BoostStamina = 0.0f;
    public float MaxBoostStamina = 100.0f;
    public float BoostStaminaRegenRate = 50.0f;
    public float BoostStaminaDepletionRate = 100.0f;

    public GrapplingTrigger m_grapplingPrefab;

    public event Action<List<int>> OnSpaceshipDestroyed;

    public float m_grapplingFireSpeed = 50.0f;
    public float m_grapplingMaxLength = 10.0f;

    public Explosion m_explosionPrefab;
    public TrailRenderer m_trailRenderer;
    public float m_trailOffsetSpeed = 0.1f;
    public Transform m_aimVisual;

    public MeshRenderer m_boostVisual;
    public TrailRenderer m_boostTrailVisual;
    public int PlayerId = -1;


    Vector2 m_direction;
    DistanceJoint2D m_grapplingJoint;
    GrapplingTrigger m_grapplingTrigger;
    RopeVisual m_ropeVisual;
    Shatter m_shatter;
    private int m_crushCounter = 0;
    public float m_secondBreakVelocity = 17;

    public InvulernabilityOutline m_invulerabilityOutline;

    public RopeVisual m_ropeVisualPrefab;
    public SpriteExplosion m_partialBreakExplosionPrefab;

    private float m_spawnProtectionTimer;

    public void FireGrappling(Vector2 dir)
    {
        if (m_grapplingJoint == null && m_grapplingTrigger == null)
        {
            m_ropeVisual = (RopeVisual)Instantiate(m_ropeVisualPrefab, transform.position, Quaternion.identity);

            m_grapplingTrigger = (GrapplingTrigger)Instantiate(m_grapplingPrefab, transform.position, Quaternion.identity);

            m_grapplingTrigger.GetComponent<Rigidbody2D>().velocity = dir * m_grapplingFireSpeed + m_rigidBody2d.velocity;
            m_grapplingTrigger.OnHit += OnGrapplingHit;

            m_ropeVisual.StartPos = m_rigidBody2d.position;
            m_ropeVisual.EndPos = m_rigidBody2d.position;
            m_ropeVisual.RestLength = 0.0f;

            SoundManager.Instance.PlaySound(SoundEffectType.GrapplingFired, UnityEngine.Random.Range(0.85f, 1.3f));
        }
    }

    private void OnGrapplingHit(Collider2D collider, Vector2 worldPoint)
    {
        if (m_grapplingTrigger != null)
            Destroy(m_grapplingTrigger.gameObject);

        if (m_grapplingJoint == null)
        {
            m_grapplingJoint = this.gameObject.AddComponent<DistanceJoint2D>();
            m_grapplingJoint.connectedBody = collider.attachedRigidbody;
            m_grapplingJoint.connectedAnchor = collider.attachedRigidbody.GetPoint(worldPoint);
            m_grapplingJoint.distance = Vector2.Distance(transform.position, worldPoint) * 1.0f;
            m_grapplingJoint.maxDistanceOnly = true;
            m_grapplingJoint.enableCollision = true;

            LastTouched touched = collider.attachedRigidbody.GetComponent<LastTouched>();
            if (touched != null)
            {
                touched.StartTouch(PlayerId);
            }

            SoundManager.Instance.PlaySound(SoundEffectType.GrapplingHit, UnityEngine.Random.Range(0.85f, 1.3f));
        }
    }

    void OnDrawGizmos()
    {
        if (m_grapplingJoint != null && m_grapplingJoint.connectedBody != null)
        {
            Gizmos.color = Color.gray;
            Gizmos.DrawWireSphere(transform.position, m_grapplingJoint.distance);
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, m_grapplingJoint.connectedBody.GetRelativePoint(m_grapplingJoint.connectedAnchor));
        }
    }

    public void ReleaseGrappling()
    {
        if (m_grapplingJoint != null)
        {
            LastTouched touched = m_grapplingJoint.connectedBody.GetComponent<LastTouched>();
            if (touched != null)
            {
                touched.EndTouch(PlayerId);
            }

            Destroy(m_grapplingJoint);
        }

        if (m_ropeVisual != null)
            Destroy(m_ropeVisual.gameObject);

        if (m_grapplingTrigger != null)
            Destroy(m_grapplingTrigger.gameObject);
    }

	void Awake () 
    {
        m_rigidBody2d = GetComponent<Rigidbody2D>();

        Throttle = Vector2.zero;
        m_direction = Vector2.up;

        m_shatter = GetComponent<Shatter>();
        //m_shatter.SetMassFromDensity(1.0f);
        m_rigidBody2d.mass = m_playerMass;

        m_shatter.OnCrush += OnCrush;

        BoostStamina = MaxBoostStamina;
        
        //destroy if borderdeath
        GetComponent<BorderDeath>().onDeath += () => DestroyShip(null);

        
	}

    public void OnCrush(Shatter shatter)
    {
        m_crushCounter++;
        if(m_crushCounter == 1)
        {
            var polymesh = shatter.GetComponent<PolyMesh>();
            polymesh.m_outlineThickness = 0.06f;
            polymesh.m_color *= 0.95f;
            shatter.m_breakVelocity = m_secondBreakVelocity;
            m_rigidBody2d.mass = m_playerMass;

            var explosion = (SpriteExplosion)Instantiate(m_partialBreakExplosionPrefab);
            SoundManager.Instance.PlaySound(SoundEffectType.Explosion, 2.0f, 0.4f);

            explosion.transform.parent = this.transform;
            explosion.transform.localPosition = Vector3.zero;

            explosion.m_color = GetComponent<MeshRenderer>().material.GetColor("_TintColor");
        }
    }

    //Not nice, but works
    //Should not instantiate stuff in ondestroy
    bool m_isBeingDestroyed = false;
    void OnApplicationQuit()
    {
        m_isBeingDestroyed = true;
    }

    public void SupressExplosion()
    {
        m_isBeingDestroyed = true;
    }

    public void DestroyShip(LastTouched _source)
    {
        Debug.Log("Destroy Spaceship");

        ReleaseGrappling();

        if (m_grapplingTrigger != null)
            Destroy(m_grapplingTrigger.gameObject);

        if (m_ropeVisual != null)
            Destroy(m_ropeVisual.gameObject);

        if (OnSpaceshipDestroyed != null)
        {
            List<int> touchedList;

            if (_source != null)
                touchedList = _source.GetLastTouched();
            else
                touchedList = new List<int>();

            OnSpaceshipDestroyed(touchedList);
        }

        SpawnExplosion();

        Destroy(this.gameObject);
    }

    void OnDestroy()
    {

        Debug.Log("OnDestroy Spaceship");
    }

    public void ActivateInvulnerability(float time)
    {
        m_shatter.m_isInvulnerable = true;
        m_invulerabilityOutline.IsBlinking = true;
        m_spawnProtectionTimer = time;
    }

    public void SpawnExplosion()
    {
      //  if (!m_isBeingDestroyed)
            SpawnExplosion(this.transform.position, m_rigidBody2d.velocity);
    }

    public void SpawnExplosion(Vector3 position, Vector2 velocity)
    {
        var explosion = (Explosion)Instantiate(m_explosionPrefab, position, Quaternion.identity);
        explosion.Init(GetComponent<MeshRenderer>().material.GetColor("_TintColor"), velocity);
    }

    void OnBecameInvisible()
    {
        //Destroy(this.gameObject);
    }

    void Update()
    {
        if (Aim.magnitude > 0.4f)
        {
            m_aimVisual.gameObject.SetActive(true);
            Vector2 dir = Aim.normalized;
			Vector3 pos = dir * 2.5f + (Vector2)transform.position;
			pos.z = -1.0f;
			m_aimVisual.transform.position = pos;
            m_aimVisual.transform.rotation = Quaternion.Euler(0, 0, 180.0f * Mathf.Atan2(dir.y, dir.x) / Mathf.PI);
        }
        else
        {
            m_aimVisual.gameObject.SetActive(false);
        }

        float trailoffset = -(Time.time * m_trailOffsetSpeed);
        m_trailRenderer.materials[0].SetTextureOffset("_MainTex", new Vector2(trailoffset, 0));

        if(m_crushCounter > 0)
        {
            Vector2 trailpos = m_trailRenderer.transform.localPosition;
            trailpos.x = Mathf.Sin(Time.time * 85.0f) * 0.14f;
            m_trailRenderer.transform.localPosition = trailpos;
            //m_boostTrailVisual.transform.localPosition = trailpos;
        }
        
    }
	
	void FixedUpdate()
    {
        float prev = m_spawnProtectionTimer;
        m_spawnProtectionTimer -= Time.fixedDeltaTime;

        if (m_spawnProtectionTimer < 0.0f && prev >= 0.0f)
        {
            m_shatter.m_isInvulnerable = false;
            m_invulerabilityOutline.IsBlinking = false;
        }

        Vector2 pos = Vector2.zero;
        float len = 1.0f;

        if (m_grapplingJoint != null)
        {
            if (m_grapplingJoint.connectedBody == null)
            {
                Destroy(m_grapplingJoint);
                Destroy(m_ropeVisual.gameObject);
            }
            else
            {
                pos = m_grapplingJoint.connectedBody.GetRelativePoint(m_grapplingJoint.connectedAnchor);
                len = m_grapplingJoint.distance;
            }
        }

        if (m_grapplingTrigger != null)
        {
            pos = m_grapplingTrigger.transform.position;
            len = Vector2.Distance(pos, m_rigidBody2d.position);

            if (Vector2.Distance(m_grapplingTrigger.transform.position, transform.position) > m_grapplingMaxLength)
            {
                Destroy(m_grapplingTrigger.gameObject);
                Destroy(m_ropeVisual.gameObject);
            }
        }

        if (m_ropeVisual != null)
        {
                m_ropeVisual.StartPos = m_rigidBody2d.position;
                m_ropeVisual.EndPos = pos;
                m_ropeVisual.RestLength = len * 0.97f;
                m_ropeVisual.Straighten(0.05f);
        }

        float boost = Mathf.Clamp01(Boost);

        if (boost <= 0.0001f)
        {
            BoostStamina = Mathf.Min(BoostStamina + BoostStaminaRegenRate * Time.fixedDeltaTime, MaxBoostStamina);
        }

        BoostStamina -= boost * BoostStaminaDepletionRate * Time.fixedDeltaTime;
        if (BoostStamina < 0.0f)
        {
            BoostStamina = 0.0f;
            boost = 0.0f;
        }

        var color = m_boostVisual.material.GetColor("_TintColor");
        color.a = boost * 0.5f;
        m_boostVisual.material.SetColor("_TintColor", color);

        m_boostTrailVisual.material.SetColor("_TintColor", color);
       
        float maxSpeed = Mathf.Lerp(m_maxSpeed, m_boostMaxSpeed, boost);
        float throttleForce = Mathf.Lerp(m_throttleForce, m_boostThrottleForce, boost);

        Vector2 desiredVel = Throttle * maxSpeed;
        Vector2 relativeVel = desiredVel - m_rigidBody2d.velocity;

        m_rigidBody2d.AddForce(relativeVel * m_throttleForce);
        m_rigidBody2d.AddForce(-m_rigidBody2d.velocity * m_throttleDamping);

        if(Throttle.magnitude > 0.1f)
            m_direction = Throttle.normalized;

        Vector2 shipDir = transform.rotation * Vector2.up;
        Vector2 shipPerp = new Vector2(-shipDir.y, shipDir.x);

        float side = Vector2.Dot(m_direction, shipPerp);
        float front = (1.0f - Vector2.Dot(m_direction, shipDir)) * 0.5f;

        m_rigidBody2d.AddTorque(m_angularForce * front * (side > 0.0f ? 1.0f : -1.0f));
        m_rigidBody2d.AddTorque(-m_angularDamping * m_rigidBody2d.angularVelocity);

	}

}
