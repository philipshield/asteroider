﻿using UnityEngine;

public class ShrinkAndDestroy : MonoBehaviour
{
	public float duration = 1f;

	float time = 0f;

	void Update()
	{
		var t = Mathf.Clamp01(1f - time / duration);
		transform.localScale = Vector3.one *  Mathf.Sqrt(t);

		time += Time.deltaTime;
		if (time > duration)
		{
			Destroy(gameObject);
		}
	}
}
