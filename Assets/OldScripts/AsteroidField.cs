﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class AsteroidField : MonoBehaviour
{


    public Camera m_camera;
    private Vector2 m_origin;
    private SmoothCamera m_cameraControl;

    public GameObject m_asteroidNotBreakablePrefab;
    public GameObject m_asteroidFullyBreakablePrefab;
    public GameObject m_asteroidPartiallyBreakablePrefab;
    public float m_radiusFactor = 1.2f;

    private float m_radius = 60.0f;

    List<Func<IEnumerator>> m_events;
    List<Func<IEnumerator>> m_extraEvents;

    void Start()
    {
        m_cameraControl = m_camera.GetComponent<SmoothCamera>();

        Vector2 cameraSize = m_camera.ViewportToWorldPoint(Vector2.one) - m_camera.ViewportToWorldPoint(Vector2.zero);
        m_radius = cameraSize.magnitude * 0.5f * m_radiusFactor;

        DestroyOutside.Radius = m_radius * 2.0f;

        // Events 
        m_events = new List<Func<IEnumerator>>();

        addEvent(RandomDirections, 5);
        //addEvent(HugeAsteroid, 3);
        //addEvent(RandomLine, 4);
        //addEvent(Vortex, 4);
        addEvent(TargetPlayers, 4);
        addEvent(Sticks, 3);
        addEvent(UpStream, 2);
        addEvent(Tunnel, 2);
        //addEvent(ZigZagTunnel, 1);
        //addEvent(Crashing, 1);

        StartCoroutine(DoRandomEvents());

        // Extra Events
        // TODO: Between-time is intensity dependant
        m_extraEvents = new List<Func<IEnumerator>>();
        
        addExtraEvent(RandomLine);
        addExtraEvent(TargetPlayers);
        addExtraEvent(Sticks);        
        //StartCoroutine(DoRandomExtraEvents());

        updateOrigin();
    }


    void addEvent(Func<IEnumerator> asteroidevent, int weight = 1)
    {
        for (int i = 0; i < weight; i++)
            m_events.Add(asteroidevent);
    }

    void addExtraEvent(Func<IEnumerator> asteroidevent, int weight = 1)
    {
        for (int i = 0; i < weight; i++)
            m_extraEvents.Add(asteroidevent);
    }

    private void updateOrigin()
    {
        m_origin = (Vector2)m_camera.transform.position;
    }

    void Update()
    {
        updateOrigin();
        m_origin = (Vector2)m_camera.transform.position;
    }


    private string m_currentEventName;
    private string m_currentExtraEventName;
    public bool m_debugShowEvents = false;



    IEnumerator DoRandomEvents()
    {
        //TODO: Additional delay for each event on easier difficulties?

        while (true)
        {
            var nextEvent = m_events[UnityEngine.Random.Range(0, m_events.Count)];
            m_currentEventName = nextEvent.Method.Name;
            yield return StartCoroutine(nextEvent());
        }
    }
    IEnumerator DoRandomExtraEvents()
    {
        while (true)
        {
            var nextEvent = m_extraEvents[UnityEngine.Random.Range(0, m_extraEvents.Count)];
            m_currentExtraEventName = nextEvent.Method.Name;
            yield return StartCoroutine(nextEvent());
            yield return new WaitForSeconds(UnityEngine.Random.Range(3.0f, 10.0f));
        }
    }


    /*
     * Events
     */

    IEnumerator RandomDirections()
    {
        var intensity = Systems.Instance.IntensitySystem;

        //Camera Control
        m_cameraControl.SetState(SmoothCamera.CameraState.Still);

        // count (lower is harder, faster until next event)
        int minCount = (int)intensity.LinearRel(8, 3);
        int maxCount = (int)intensity.LinearRel(4, 1);
        int count = UnityEngine.Random.Range(minCount, maxCount);


        for (int i = 0; i < count; i++)
        {
            Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
            Vector2 pos = (-dir * m_radius);
            GameObject obj = (GameObject)Instantiate(m_asteroidPartiallyBreakablePrefab, m_origin + pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
            CreateRandomAsteroid(obj, AsteroidType.Simple);

            // velocity
            float minSpeed = intensity.Exp(3, 30);
            float maxSpeed = intensity.Exp(8, 50);
            Vector2 velocity = UnityEngine.Random.Range(minSpeed, maxSpeed) * (dir + UnityEngine.Random.insideUnitCircle * 0.5f);
            obj.GetComponent<Rigidbody2D>().velocity = velocity;

            // angular velocity
            minSpeed = intensity.Exp(-150, -300);
            maxSpeed = intensity.Exp(150, 300);
            float angularVelocity = UnityEngine.Random.Range(minSpeed, maxSpeed);

            if (intensity.IsRelIntense())
                angularVelocity = Mathf.Sign(angularVelocity) * Mathf.Clamp(maxSpeed - 50, maxSpeed, Mathf.Abs(angularVelocity));
            obj.GetComponent<Rigidbody2D>().angularVelocity = angularVelocity;


            // wait until next
            float minTime = intensity.Exp(1.5f, 0.3f);
            float maxTime = intensity.LinearRel(4.5f, 2.0f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(minTime, maxTime));
        }
    }

    IEnumerator HugeAsteroid()
    {
        var intensity = Systems.Instance.IntensitySystem;

        // spawn
        Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 pos = (-dir * m_radius * 1.9f);
        GameObject obj = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, m_origin + pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
        CreateRandomAsteroid(obj, AsteroidType.SimpleHuge);

        // velocity 
        float minSpeed = intensity.Exp(3, 20);
        float maxSpeed = intensity.Exp(10, 60);
        obj.GetComponent<Rigidbody2D>().velocity = UnityEngine.Random.Range(minSpeed, maxSpeed) * (dir + UnityEngine.Random.insideUnitCircle * 0.5f);

        // angular velocity
        minSpeed = intensity.Exp(-150, -220);
        maxSpeed = intensity.Exp(150, 220);
        float angularVelocity = UnityEngine.Random.Range(minSpeed, maxSpeed);

        if (intensity.IsRelIntense())
            angularVelocity = Mathf.Sign(angularVelocity) * Mathf.Clamp(maxSpeed - 50, maxSpeed, Mathf.Abs(angularVelocity));
        obj.GetComponent<Rigidbody2D>().angularVelocity = angularVelocity;

        // wait 
        float minTime = intensity.Exp(1.0f, 0.3f);
        float maxTime = intensity.LinearRel(3.5f, 2.0f);
        yield return new WaitForSeconds(UnityEngine.Random.Range(minTime, maxTime));

    }

    IEnumerator RandomLine()
    {
        var intensity = Systems.Instance.IntensitySystem;

        // count (higher is harder)
        int minCount = (int)intensity.Exp(6, 25);
        int maxCount = (int)intensity.Exp(10, 40);
        int count = UnityEngine.Random.Range(minCount, maxCount);

        // velocity (closer and faster is harder)
        float minSpeed = intensity.Exp(10, 30);
        float maxSpeed = intensity.Exp(14, 30);

        // velocity innaccuracy (lower is harder)
        float targetdirinacc = intensity.Exp(0.15f, 0.05f);

        // angular velocity
        float minAngSpeed = intensity.Exp(-150, -300);
        float maxAngSpeed = intensity.Exp(150, 300);

        // spacing (lower is harder) 
        // TODO: make sure not too close
        float minSpacing = intensity.Linear(4, 1.5f);
        float maxSpacing = intensity.Linear(2, 0.5f);
        float spacing = UnityEngine.Random.Range(minSpacing, maxSpacing);

        // line skew
        // TODO: harder with more variation here?
        float linedirinnac = 0.2f;

        // mass
        float mass = intensity.Exp(0.7f, 0.9f);


        Vector2 targetdir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 linedir = (new Vector2(-targetdir.y, targetdir.x).normalized +
            (UnityEngine.Random.insideUnitCircle.normalized * linedirinnac)).normalized;
        Vector2 step = linedir * spacing;

        Vector2 pos = m_origin + (-targetdir * m_radius);
        pos -= step * ((count / 2) + 1); // go back half of count

        for (int i = 0; i < count; i++)
        {
            GameObject asteroid = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
            CreateRandomAsteroid(asteroid, AsteroidType.SimpleSmall);

            asteroid.GetComponent<Rigidbody2D>().velocity = UnityEngine.Random.Range(minSpeed, maxSpeed) * (targetdir + UnityEngine.Random.insideUnitCircle * targetdirinacc);
            float angularVelocity = UnityEngine.Random.Range(minAngSpeed, maxAngSpeed);

            asteroid.GetComponent<Shatter>().SetMassFromDensity(mass);

            pos += step;
        }

        // wait
        float minTime = intensity.Exp(5.0f, 3.0f);
        float maxTime = intensity.Exp(7.5f, 5.0f);
        yield return new WaitForSeconds(UnityEngine.Random.Range(minTime, maxTime));
    }

    IEnumerator Vortex()
    {
        var intensity = Systems.Instance.IntensitySystem;

        // wait for things to clear a bit (harder if fast but still pretty clear?)
        // TODO: add a "wait until calm thing?"
        yield return new WaitForSeconds(8.0f);

        // count 
        int mincount = (int)intensity.Exp(5.0f, 30.0f);
        int maxcount = (int)(intensity.Exp(10.0f, 50.0f));
        int N = UnityEngine.Random.Range(mincount, maxcount);

        float angle = UnityEngine.Random.Range(0, 360);

        // radial span
        float minradspan = intensity.Linear(180, 360);
        float maxradspan = intensity.Linear(270, 360 * 1.75f);
        float radialspan = UnityEngine.Random.Range(minradspan, maxradspan);
        float step = radialspan / N;

        // spacing
        float seconds = intensity.Linear(2.0f, 0.7f); // total time for vortex wave
        float pause = (seconds / N);

        // speed
        float speed = UnityEngine.Random.Range(13.0f, 20.0f);

        for (int i = 0; i < N; i++)
        {
            Quaternion radialrot = Quaternion.Euler(0, 0, angle);
            Vector2 dir = radialrot * Vector2.up;
            Vector2 pos = m_origin + dir * m_radius;
            Vector2 vel = -dir * speed;
            float angvel = UnityEngine.Random.Range(0.1f, 6.0f);

            GameObject asteroid = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
            CreateRandomAsteroid(asteroid, AsteroidType.SimpleSmall);


            asteroid.GetComponent<Rigidbody2D>().velocity = vel;
            asteroid.GetComponent<Rigidbody2D>().angularVelocity = angvel;
            asteroid.GetComponent<Shatter>().SetMassFromDensity(0.8f);

            angle += step;
            yield return new WaitForSeconds(pause);
        }

        yield return new WaitForSeconds(UnityEngine.Random.Range(intensity.Exp(4.0f, 0.0f), 5.0f));
    }

    IEnumerator Crashing()
    {
        // slow down before start
        m_cameraControl.SetState(SmoothCamera.CameraState.Still);
        float currentvel = m_cameraControl.GetVelocity().magnitude;
        yield return new WaitForSeconds(currentvel * 0.3f); //ish

        //smaller blocks
        float boxwidth = 40;
        float boxheight = 100;
        float spawnmargin = 10;

        //create two big boxes
        Vector2[] boxpoints = new Vector2[4];
        boxpoints[0] = new Vector2(boxwidth / 2, boxheight / 2);
        boxpoints[1] = new Vector2(boxwidth / 2, -boxheight / 2);
        boxpoints[2] = new Vector2(-boxwidth / 2, -boxheight / 2);
        boxpoints[3] = new Vector2(-boxwidth / 2, boxheight / 2);

        // from the right, tunnelwidth apart
        float angle = UnityEngine.Random.Range(0, 360);
        Quaternion rot = Quaternion.Euler(0, 0, angle);
        Vector2 dir = rot * new Vector2(1, 0);

        Vector2 odir = new Vector2(-dir.y, dir.x);
        Vector2 pos = m_origin + -dir * (boxheight + spawnmargin);

        Vector2 vel = dir * UnityEngine.Random.Range(16.0f, 19.8f);
        Vector2 cameravel = vel * 1.05f;


        // The event:
        m_cameraControl.SetState(SmoothCamera.CameraState.ExternalControl);

        //Begin Fall
        Debug.Log("Begin Fall");
        m_cameraControl.SetDesiredVelocity(cameravel);
        yield return new WaitForSeconds(2.0f); //get up to speed

        // Fall 
        Debug.Log("Falling");
        float tiltangle = 0;
        GameObject box = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, rot);
        Quaternion tilt = Quaternion.Euler(0, 0, -tiltangle);
        box.transform.rotation *= tilt;
        var poly = box.AddComponent<PolygonCollider2D>();
        poly.SetPath(0, boxpoints);
        box.GetComponent<Rigidbody2D>().velocity = vel;
        box.GetComponent<Rigidbody2D>().angularVelocity = tiltangle * 0.1f;
        var body = poly.GetComponent<Rigidbody2D>();
        poly.GetComponent<Shatter>().SetMassFromDensity(5.0f);

        yield return new WaitForSeconds(10.0f);

        // Escape 
        Debug.Log("Escape");
        cameravel *= 0.9f;                          // slow down
        Vector2 escapedir = UnityEngine.Random.Range(0, 2) == 0 ? odir : -odir;
        cameravel += escapedir * cameravel.magnitude;    // move camera to side
        m_cameraControl.SetDesiredVelocity(cameravel);
        yield return new WaitForSeconds(10.0f);

        // Calm down
        Debug.Log("Calm down");
        m_cameraControl.SetState(SmoothCamera.CameraState.Still);
        yield return new WaitForSeconds(3.0f);
    }

    IEnumerator Tunnel()
    {
        var intensity = Systems.Instance.IntensitySystem;

        //slow down before start
        m_cameraControl.SetState(SmoothCamera.CameraState.Still);
        yield return new WaitForSeconds(4.0f);

        float tunnelwidth = 50 - intensity.Linear(0, 30);
        float boxwidth = 100;
        float boxheight = 40;
        float spawnmargin = m_radius * 1.6f;

        //create two big boxes
        List<Vector2[]> boxes = new List<Vector2[]>();
        for (int i = 0; i < 2; i++)
        {
            // 2 ----- 1
            // |   x   |
            // 3 ----- 0

            boxes.Add(new Vector2[4]);
            boxes[i][0] = new Vector2(boxwidth / 2, boxheight / 2);
            boxes[i][1] = new Vector2(boxwidth / 2, -boxheight / 2);
            boxes[i][2] = new Vector2(-boxwidth / 2, -boxheight / 2);
            boxes[i][3] = new Vector2(-boxwidth / 2, boxheight / 2);
        }

        // from the right, tunnelwidth apart
        float angle = UnityEngine.Random.Range(0, 360);
        Quaternion rot = Quaternion.Euler(0, 0, angle);
        Vector2 dir = rot * new Vector2(1, 0);

        Vector2 odir = new Vector2(-dir.y, dir.x);
        Vector2 pos = m_origin + -dir * (spawnmargin) - odir * (boxheight / 2 + tunnelwidth / 2);
        Vector2 vel = dir * UnityEngine.Random.Range(16.0f, 23.3f);


        //control camera
        m_cameraControl.SetState(SmoothCamera.CameraState.ExternalControl);
        m_cameraControl.SetDesiredVelocity(vel * UnityEngine.Random.Range(-1.0f, 1.0f) * 0.9f);
        yield return new WaitForSeconds(1.3f); //more camera just before the tunnel appears

        float closinginspeed = intensity.Linear(1.3f, 1.7f);
        vel += odir * closinginspeed;

        float maxTilt = intensity.Linear(1f, 4f);
        float tiltangle = UnityEngine.Random.Range(-maxTilt, maxTilt);

        Vector2 tunneloffset = odir * (boxheight / 2 + tunnelwidth + boxheight / 2);
        foreach (Vector2[] path in boxes)
        {
            GameObject box = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, rot);
            Quaternion tilt = Quaternion.Euler(0, 0, -tiltangle);
            box.transform.rotation *= tilt;

            var poly = box.AddComponent<PolygonCollider2D>();
            poly.SetPath(0, boxes[0]);

            box.GetComponent<Rigidbody2D>().velocity = vel;
            box.GetComponent<Rigidbody2D>().angularVelocity = 
                Mathf.Sign(tiltangle) * intensity.Linear(0f, 2f) * UnityEngine.Random.Range(0.07f, 0.3f);

            pos += tunneloffset;
            tiltangle = -tiltangle;
            vel -= 2 * odir * closinginspeed;

            //add mass
            var body = poly.GetComponent<Rigidbody2D>();
            poly.GetComponent<Shatter>().SetMassFromDensity(5.0f);
        }

        //during the epic "fall", create some more events:

        yield return new WaitForSeconds(3.0f);
        m_cameraControl.SetState(SmoothCamera.CameraState.Still);

        yield return new WaitForSeconds(10f); // "idle" before next event        
    }

    IEnumerator ZigZagTunnel()
    {

        float min_thick = 20f;
        float y_top = 70f;
        float y_bottom = 70f;

        float x_spacing = 2f;
        float gap = 15f;
        float margin = gap / 2f;

        float max_y = y_top - min_thick - margin;
        float min_y = -y_bottom + min_thick + margin;

        // Spawn all the things!
        Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 odir = new Vector2(-dir.y, dir.x);
        Vector2 vel = -dir * 30f;
        Vector2 x_pos = m_origin + dir * m_radius;

        float event_camera_speed = 18f;
        m_cameraControl.SetState(SmoothCamera.CameraState.ExternalControl);
        m_cameraControl.SetDesiredVelocity(dir * event_camera_speed);
        yield return new WaitForSeconds(1.3f); // get some speed

        float max_w = 10f;
        float min_w = 2f;
        float avg_w = (min_w + min_w) / 2.0f;

        float A = UnityEngine.Random.Range(10f, 20f);
        float phase = UnityEngine.Random.Range(0, 2*Mathf.PI);
        
        float wavelenth = 60f;
        float sin_step = (2 * Mathf.PI) / (wavelenth/avg_w);
        float t = phase;
        int N = 10;    

        for (int i = 0; i < N; i++)
        {
            float curr_y = A * Mathf.Sin(t);
            float next_y = A * Mathf.Sin(t + sin_step);
            t += sin_step;

            float w = UnityEngine.Random.Range(min_w, max_w);
            Vector2 y_pos = odir * curr_y;
            Vector2 y_pos_next = odir * next_y;
            Vector2[] top_box = { y_pos + odir*margin,
                                  y_pos + odir*y_top,
                                  y_pos + odir*y_top + dir * w,
                                  y_pos_next + odir*margin + dir * w };

            Vector2[] bottom_box = { y_pos - odir*margin,
                                    y_pos - odir*y_bottom,
                                    y_pos - odir*y_bottom + dir * w,
                                    y_pos_next - odir*margin + dir * w };

            // spawn boxes
            List<Vector2[]> boxes = new List<Vector2[]> { top_box, bottom_box };
            foreach (var vertexgroup in boxes)
            {
                GameObject box = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, x_pos, Quaternion.identity);
                var poly = box.AddComponent<PolygonCollider2D>();
                poly.SetPath(0, vertexgroup);

                box.GetComponent<Rigidbody2D>().velocity = vel;
                //vel += odir * UnityEngine.Random.Range(0.01f, 0.1f);

                var body = poly.GetComponent<Rigidbody2D>();
                poly.GetComponent<Shatter>().SetMassFromDensity(5.0f);
            }
            

            x_pos += dir * (w);
        }


        m_cameraControl.SetState(SmoothCamera.CameraState.Still);
        yield return new WaitForSeconds(5f);
    }


    //IEnumerator TrashCompactor()
    //{
    //    var intensity = Systems.Instance.IntensitySystem;
    //
    //    Vector2 center = UnityEngine.Random.insideUnitCircle * m_radius * 0.1f + m_origin;
    //    float radius = 50f;
    //    float thickness = 30f;
    //    Vector2 axis = UnityEngine.Random.insideUnitCircle.normalized;
    //    Vector2 axis2 = new Vector2(-axis.y, axis.x);
    //
    //    Vector2[] shape = new Vector2[4];
    //    shape[0] = axis * radius;
    //    shape[1] = shape[0] + axis * thickness * Mathf.Sqrt(2);
    //    shape[2] = shape[2] + axis2 * thickness;
    //    shape[3] = axis2 * radius;
    //
    //    //Quaternion rotation = Quaternion.identity;
    //    //for (int i = 0; i < 4; i++)
    //    //{   
    //    //    rotation *= 
    //    //}
    //    yield return new WaitForSeconds(10f); // "idle" before next event        
    //}

    IEnumerator UpStream()
    {
        yield return null;
        var intensity = Systems.Instance.IntensitySystem;

        bool downstream = UnityEngine.Random.Range(0, 3) == 0;
        Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
        Vector2 cameradir = downstream ? -dir : dir;

        float camMinSpeed = intensity.Exp(10.0f, 18.0f);
        float camMaxSpeed = intensity.Exp(16.0f, 22.0f);
        float camSpeed = UnityEngine.Random.Range(camMinSpeed, camMaxSpeed);

        m_cameraControl.SetState(SmoothCamera.CameraState.ExternalControl);
        float prevSpeed = m_cameraControl.m_speed;
        m_cameraControl.SetSpeed(prevSpeed * intensity.Exp(1.0f, 20.0f));
        m_cameraControl.SetDesiredVelocity(cameradir * camSpeed);

        yield return new WaitForSeconds(4.0f);

        Vector2 perp = new Vector2(-dir.y, dir.x);
        float width = m_radius / (2.3f * Mathf.PI);
        int count = UnityEngine.Random.Range(9, 13);

        for (int i = 0; i < count; i++)
        {
            int asteroidCount = Mathf.RoundToInt(intensity.Linear(1, 3));
            for (int j = 0; j < asteroidCount; j++)
            {
                float side = UnityEngine.Random.Range(-1.0f, 1.0f);
                Vector2 pos = m_origin + dir * m_radius + UnityEngine.Random.Range(-1.0f, 1.0f) * m_radius * perp;
                GameObject asteroid = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
                CreateRandomAsteroid(asteroid, UnityEngine.Random.Range(0, 2) == 0 ? AsteroidType.Simple : AsteroidType.SimpleSmall);

                Rigidbody2D body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = dir * UnityEngine.Random.Range(-23, -10.0f) + perp * 2.0f * (UnityEngine.Random.Range(-3.0f, 3.0f) - side);
                body.angularVelocity = UnityEngine.Random.Range(-50.0f, 50.0f);
                if (downstream) body.velocity *= 2;
            }

            yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f, 2.0f));
        }

        m_cameraControl.SetSpeed(prevSpeed);
        m_cameraControl.SetDesiredVelocity(Vector2.zero);
        yield return new WaitForSeconds(2.0f);
    }

    IEnumerator Sticks()
    {
        var intensity = Systems.Instance.IntensitySystem;
        
        yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 2.0f));

        float vel = UnityEngine.Random.Range(10.0f, 25.0f);

        // Fewer passes is harder because different stuff happens sooner.
        int count = 8 - Mathf.RoundToInt(intensity.Exp(0, 7));

        for (int i = 0; i < count; i++)
        {
            Vector2 target = UnityEngine.Random.insideUnitCircle * m_radius * 0.1f + m_origin;
            Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
            var pos = target + dir * m_radius;
            float angle = Mathf.Atan2(dir.y, dir.x) * 180.0f / Mathf.PI;

            {
                GameObject asteroid = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, Quaternion.Euler(0, 0, angle));
                CreateRandomAsteroid(asteroid, AsteroidType.Stick);
                asteroid.GetComponent<Shatter>().SetMassFromDensity(0.5f);
                var body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = vel * (target - pos).normalized + m_cameraControl.GetDesiredVelocity();
                body.angularVelocity = UnityEngine.Random.Range(-3f, 3f);
            }

            if (UnityEngine.Random.Range(0, 2) == 1)
            {
                pos = target - dir * m_radius;
                GameObject asteroid = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, Quaternion.Euler(0, 0, angle));
                CreateRandomAsteroid(asteroid, AsteroidType.Stick);
                asteroid.GetComponent<Shatter>().SetMassFromDensity(0.5f);
                var body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = vel * (target - pos).normalized + m_cameraControl.GetDesiredVelocity();
                body.angularVelocity = UnityEngine.Random.Range(-3f, 3f);
            }

            float waitTime = 4.0f - intensity.Exp(0.0f, 4.0f);
            yield return new WaitForSeconds(waitTime);
        }

        yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 2.0f));
    }

    IEnumerator TargetPlayers()
    {
        var intensity = Systems.Instance.IntensitySystem;
        var players = Game.Instance.players;

        //wait before event
        float minWait = intensity.Linear(5.0f, 0.0f);
        float maxWait = intensity.Linear(10.0f, 2.0f);
        yield return new WaitForSeconds(UnityEngine.Random.Range(minWait, maxWait));

        float minVel = intensity.Exp(20.0f, 40.0f);
        float maxVel = intensity.Exp(30.0f, 60.0f);
        float vel = UnityEngine.Random.Range(minVel, maxVel);

        float skipProbability = Mathf.Clamp01((float)(players.Count(p => p.Value.Alive()) - 4) / 10.0f);

        foreach (var player in players.Values)
        {
            if (player.Alive() && UnityEngine.Random.Range(0.0f, 1.0f) > skipProbability)
            {
                var ppos = player.GetPosition();

                Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
                var pos = m_origin + dir * m_radius;

                GameObject asteroid = (GameObject)Instantiate(m_asteroidFullyBreakablePrefab, pos, Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)));
                CreateRandomAsteroid(asteroid, AsteroidType.Simple);

                asteroid.GetComponent<Shatter>().SetMassFromDensity(0.5f);
                var body = asteroid.GetComponent<Rigidbody2D>();
                body.velocity = vel * (ppos - pos).normalized + m_cameraControl.GetDesiredVelocity();


                float minAngSpeed = intensity.Linear(0.0f, 500.0f);
                float maxAngSpeed = intensity.Linear(100.0f, 900.0f);
                body.angularVelocity = UnityEngine.Random.Range(minAngSpeed, maxAngSpeed);
            }
        }

        yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 2.0f));
    }

    enum AsteroidType
    {
        Simple,
        SimpleSmall,
        SimpleHuge,
        RandomConvex,
        Weird,
        Stick,
        Concave
    }

    Vector2[] simplePolygonPath(float minsize, float maxsize, Vector2? scale = null)
    {
        if (scale == null) scale = Vector2.one;

        int count = UnityEngine.Random.Range(5, 10);
        float size = UnityEngine.Random.Range(minsize, maxsize);
        Vector2[] path = new Vector2[count];

        float anglerand = (((2.0f * Mathf.PI) / count) / 2.0f) * 0.8f;
        for (int i = 0; i < count; i++)
        {
            float angle = 2.0f * Mathf.PI * i / (float)count;
            angle += UnityEngine.Random.Range(-anglerand, anglerand);

            path[i] = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * size;
        }

        for (int i = 0; i < count; i++)
        {
            path[i].Scale(scale.Value);
        }

        return path;
    }



    List<Vector2> convexHull(List<Vector2> P)
    {
        int N = P.Count;
        List<Vector2> hull = new List<Vector2>();
        if (N == 1)
        {
            return new List<Vector2>() { P[0] };
        }
        if (N == 2)
        {
            if (P[0] == P[1])
            {
                hull.Add(P[0]);
                return hull;
            }
            hull.Add(P[0]);
            hull.Add(P[1]);
            return hull;
        }

        //find lowest point
        Vector2 start = new Vector2(0, float.MaxValue / 2);
        List<Vector2> points = new List<Vector2>();
        for (int i = 0; i < N; i++)
        {
            points.Add(P[i]);
            if (P[i].y < start.y)
            {
                start = P[i];
                continue;
            }
            if ((P[i]).y == start.y)
            {
                if ((P[i]).x < start.x)
                {
                    start = P[i];
                    continue;
                }
            }
        }

        // Sort by angle (or distance)
        points.Sort(new FuncComparer<Vector2>((a, b) =>
        {
            return Vector2.Angle(start, a).CompareTo(Vector2.Angle(start, b));
            //if (Vector2.Angle(start, a) == Vector2.Angle(start,b))
            //{
            //    return Vector2.Distance(start, a).CompareTo(Vector2.Distance(start, b));
            //}
            //return Vector2.Angle(start, a).CompareTo(Vector2.Angle(start, b));
        }));

        //helper. get secondmost top item
        Func<Stack<Vector2>, Vector2> top2 = (stack) =>
        {
            Vector2 first = stack.Pop();
            Vector2 second = stack.Peek();
            stack.Push(first);
            return second;
        };

        Func<Vector2, Vector2, Vector2, PointOrientation> orientation = (p, q, r) =>
        {
            int orien = (int)((q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y));
            if (orien == 0) return PointOrientation.COLINEAR;
            return (orien > 0) ? PointOrientation.CLOCKWISE : PointOrientation.COUNTERCLOCKWISE; // clockwise or counterclockwise
        };

        //Find convex hull
        Stack<Vector2> S = new Stack<Vector2>();
        S.Push(points[0]);
        S.Push(points[1]);
        S.Push(points[2]);

        for (int i = 3; i < N; i++)
        {
            while (orientation(top2(S), S.Peek(), points[i]) != PointOrientation.COUNTERCLOCKWISE)
            {
                if (S.Count != 0)
                {
                    S.Pop();
                }
            }
            S.Push(points[i]);
        }

        //stack, S, holds the hull
        return S.ToList();
    }

    void createAsteroid(GameObject obj, List<Vector2[]> paths)
    {
        foreach (var vertices in paths)
        {
            var poly = obj.AddComponent<PolygonCollider2D>();
            poly.SetPath(0, vertices);
        }

        obj.GetComponent<Shatter>().SetMassFromDensity(0.2f);
    }

    void CreateRandomAsteroid(GameObject obj, AsteroidType type)
    {
        switch (type)
        {
            case AsteroidType.Concave:
                {
                    List<Vector2[]> paths = CreateVoronoiAsteroid(UnityEngine.Random.Range(3, 7), UnityEngine.Random.Range(5.0f, 15.0f));
                    createAsteroid(obj, paths);
                }
                break;
            case AsteroidType.Simple:
                {
                    Vector2[] path = simplePolygonPath(2.0f, 7.0f);
                    createAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            case AsteroidType.SimpleSmall:
                {
                    Vector2[] path = simplePolygonPath(0.6f, 1.7f);
                    createAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            case AsteroidType.RandomConvex:
                {
                    float maxR = 5.0f;
                    int N = 5; //UnityEngine.Random.Range(5, 24);
                    List<Vector2> P = new List<Vector2>();
                    for (int i = 0; i < N; i++)
                        P.Add(UnityEngine.Random.insideUnitCircle * maxR);

                    //find lowest point
                    Vector2 start = new Vector2(0, float.MaxValue / 2);
                    for (int i = 0; i < N; i++)
                    {
                        if (P[i].y < start.y)
                        {
                            start = P[i];
                            continue;
                        }
                        if ((P[i]).y == start.y)
                        {
                            if ((P[i]).x < start.x)
                            {
                                start = P[i];
                                continue;
                            }
                        }
                    }

                    P.Sort(new FuncComparer<Vector2>((a, b) =>
                    {
                        return Vector2.Angle(start, a).CompareTo(Vector2.Angle(start, b));
                    }));


                    //List<Vector2> hull = convexHull();
                    //createAsteroid(obj, P.ToArray());
                }
                break;
            case AsteroidType.SimpleHuge:
                {
                    Vector2[] path = simplePolygonPath(15.0f, 25.0f);
                    createAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            case AsteroidType.Stick:
                {
                    float w = 0.5f * UnityEngine.Random.Range(7.0f, 16.0f);
                    float h = 0.5f * UnityEngine.Random.Range(2.0f, 4.0f);
                    //Vector2[] path = new Vector2[4];
                    //path[0].x = -w;
                    //path[0].y = -h;
                    //path[1].x = w;
                    //path[1].y = -h;
                    //path[2].x = w;
                    //path[2].y = h;
                    //path[3].x = -w;
                    //path[3].y = h;
                    //
                    //for (int i = 0; i < path.Length; i++)
                    //{
                    //    path[i] += UnityEngine.Random.insideUnitCircle * h * 0.45f;
                    //}



                    Vector2[] path = simplePolygonPath(1, 1, new Vector2(w, h));

                    createAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
            default:
                {
                    throw new NotImplementedException();
                }
        }
    }

    List<Voronoi2.GraphEdge> edges;
    Dictionary<int, List<Vector2>> sites = new Dictionary<int, List<Vector2>>();
    SortedDictionary<float, List<Vector2>> sortedSites = new SortedDictionary<float, List<Vector2>>();
    List<Vector2[]> CreateVoronoiAsteroid(int numParts, float size)
    {
        Voronoi2.Voronoi voronoi = new Voronoi2.Voronoi(0.1);

        int numPoints = numParts * 10;

        double[] x = new double[numPoints];
        double[] y = new double[numPoints];

        for (int i = 0; i < numPoints; i++)
        {
            x[i] = UnityEngine.Random.Range(-size, size);
            y[i] = UnityEngine.Random.Range(-size, size);
        }

        sortedSites.Clear();
        sites.Clear();
        edges = voronoi.generateVoronoi(x, y, -size * 1.4f, size * 1.4f, -size * 1.4f, size * 1.4f);
        List<Vector2> points = new List<Vector2>();
        foreach (var edge in edges)
        {

            List<Vector2> l1;
            if (!sites.TryGetValue(edge.site1, out l1))
            {
                l1 = new List<Vector2>();
                sites[edge.site1] = l1;
            }
            l1.Add(new Vector2((float)edge.x1, (float)edge.y1));
            l1.Add(new Vector2((float)edge.x2, (float)edge.y2));

            List<Vector2> l2;
            if (!sites.TryGetValue(edge.site2, out l2))
            {
                l2 = new List<Vector2>();
                sites[edge.site2] = l2;
            }
            l2.Add(new Vector2((float)edge.x1, (float)edge.y1));
            l2.Add(new Vector2((float)edge.x2, (float)edge.y2));
        }

        foreach (var entry in sites)
        {
            var list = entry.Value;
            Vector2 center = Vector2.zero;
            int centerCount = 0;
            List<Vector2> newList = new List<Vector2>();

            for (int i = 0; i < list.Count; i++)
            {
                bool exists = false;
                for (int j = 0; j < i; j++)
                {
                    if (Mathf.Approximately(list[i].x, list[j].x) && Mathf.Approximately(list[i].x, list[j].x))
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists)
                    continue;

                newList.Add(list[i]);
                center += list[i];
                centerCount++;
            }

            center /= centerCount;
            float distSq = center.magnitude;
            sortedSites.Add(distSq, newList);
        }

        List<Vector2[]> result = new List<Vector2[]>();
        {
            int i = 0;
            foreach (var site in sortedSites)
            {
                if (i >= numParts)
                    break;

                SortCCW(site.Value);
                result.Add(site.Value.ToArray());


                ++i;
            }
        }
        return result;
    }

    void SortCCW(List<Vector2> vertices)
    {
        Vector2 center = Vector2.zero;

        foreach (var v in vertices)
            center += v;

        center /= vertices.Count;

        vertices.Sort((v1, v2) =>
        {
            Vector2 v = v1 - center;
            float a = Mathf.Atan2(v.y, v.x);
            v = v2 - center;
            float b = Mathf.Atan2(v.y, v.x);

            return a.CompareTo(b);
        });
    }

    void OnGUI()
    {
        if (m_debugShowEvents)
        {
            GUI.Label(new Rect(200, 1, 100, 20), m_currentEventName);
            GUI.Label(new Rect(200, 21, 100, 30), m_currentExtraEventName);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(m_origin, 1.0f);

        if (edges != null)
        {

            int i = 0;
            foreach (var entry in sortedSites)
            {
                var site = entry.Value;

                if (i < 6)
                    Gizmos.color = new Color(1.0f, 0, 0, 0.5f);
                else
                    break;

                for (int j = 0; j < site.Count; j++)
                {
                    int n = (j + 1) % site.Count;

                    Gizmos.DrawLine(site[j], site[n]);
                }

                ++i;
            }
        }
    }

    public class FuncComparer<T> : IComparer<T>
    {
        private readonly Func<T, T, int> func;
        public FuncComparer(Func<T, T, int> comparerFunc)
        {
            this.func = comparerFunc;
        }

        public int Compare(T x, T y)
        {
            return this.func(x, y);
        }
    }
}
