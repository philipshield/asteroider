﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Model
{
    public event System.Action OnModelChanged;

    Dictionary<string, object> m_data = new Dictionary<string, object>();
    bool m_isDirty = false;

    public bool ContainsKey(string _key)
    {
        return m_data.ContainsKey(_key);
    }

    public bool GetBool(string _key, bool _default = default(bool))
    {
        return GetObject<bool>(_key, _default);
    }

    public int GetInt(string _key, int _default = default(int))
    {
        return GetObject<int>(_key, _default);
    }

    public float GetFloat(string _key, float _default = default(float))
    {
        return GetObject<float>(_key, _default);
    }

    public string GetString(string _key, string _default = default(string))
    {
        return GetObject<string>(_key, _default);
    }

    public object GetObject(string _key, object _default = default(object))
    {
        if (m_data.ContainsKey(_key))
            return (object)m_data[_key];
        else
            return _default;
    }

    public T GetObject<T>(string _key, T _default = default(T)) 
    {
        return (T)GetObject(_key, (object)_default);
    }

    public void SetInt(string _key, int _value)
    {
        SetObject(_key, _value);
    }

    public void SetFloat(string _key, float _value)
    {
        SetObject(_key, _value);
    }

    public void SetString(string _key, string _value)
    {
        SetObject(_key, _value);
    }

    public void SetBool(string _key, bool _value)
    {
        SetObject(_key, _value);
    }

    public void SetObject(string _key, object _value)
    {
        object oldValue;
        if (m_data.TryGetValue(_key, out oldValue))
        {
            if (oldValue != _value)
            {
                m_data[_key] = _value;
                m_isDirty = true;
            }
        }
        else
        {
            m_data.Add(_key, _value);
            m_isDirty = true;
        }
    }

    public void SetObject<T>(string _key, T _value)
    {
        SetObject(_key, (object)_value);
    }

    public void Notify()
    {
        if (m_isDirty)
        {
            if (OnModelChanged != null)
                OnModelChanged();

            m_isDirty = false;
        }
    }
}
