﻿using UnityEngine;
using System.Collections;

public class ViewController : MonoBehaviour
{
    public ViewBase View { get; private set; }
    public ControllerBase Controller { get; private set; }

	void Awake ()
    {
        View = GetComponent<ViewBase>();
        Controller = GetComponent<ControllerBase>();
	}
}
