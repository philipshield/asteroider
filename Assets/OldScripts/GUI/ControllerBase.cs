﻿using UnityEngine;
using System.Collections;

public class ControllerBase : MonoBehaviour
{
    public Model Model { get; private set; }

    public void Init(Model _model)
    {
        Model = _model;
    }
}
