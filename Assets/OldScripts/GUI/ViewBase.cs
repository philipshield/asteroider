﻿using UnityEngine;
using System.Collections;

public abstract class ViewBase : MonoBehaviour
{
    public Model Model { get; private set; }

    public void Init(Model _model)
    {
        Model = _model;
        Model.OnModelChanged += OnModelChanged;
    }

    public abstract void OnModelChanged();
}

