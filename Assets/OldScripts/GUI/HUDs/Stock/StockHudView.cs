﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class StockHudView : ViewBase
{
    [SerializeField]
    List<Text> m_scoreText;

    public override void OnModelChanged()
    {
        for (int i = 0; i < m_scoreText.Count; i++)
        {
            bool active = Model.ContainsKey("num_lives_" + i);
            m_scoreText[i].gameObject.SetActive(active);

            if (active)
            {
                m_scoreText[i].text = Model.GetInt("num_lives_" + i).ToString();

                var color = Model.GetObject<Color>("color_" + i, Color.white);
                color.a = 1.0f;
                m_scoreText[i].color = color;
            }
        }
    }
}
