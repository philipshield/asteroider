﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TimeHudView : ViewBase
{
    [SerializeField]
    List<Text> m_scoreText;

    [SerializeField]
    Text m_timeLeft;

    float m_startTimeStamp;
    float m_timeLimit;

    void Start()
    {
        m_startTimeStamp = Time.time;
        m_timeLimit = (float)Model.GetInt("time_limit");
    }

    void Update()
    {
        float timeLeft = m_timeLimit - (Time.time - m_startTimeStamp);
        timeLeft = Mathf.Max(timeLeft, 0.0f);

        int totalSeconds = (int)timeLeft;
        m_timeLeft.text = string.Format("{0:00}:{1:00}", totalSeconds / 60, totalSeconds % 60);
    }

    public override void OnModelChanged()
    {
        for (int i = 0; i < m_scoreText.Count; i++)
        {
            bool active = Model.ContainsKey("score_" + i);
            m_scoreText[i].gameObject.SetActive(active);

            if (active)
            {
                m_scoreText[i].text = Model.GetInt("score_" + i).ToString();

                var color = Model.GetObject<Color>("color_" + i, Color.white);
                color.a = 1.0f;
                m_scoreText[i].color = color;
            }
        }
    }
}
