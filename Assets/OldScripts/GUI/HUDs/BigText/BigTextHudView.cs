﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BigTextHudView : ViewBase
{
    [SerializeField]
    Text m_text;

    public override void OnModelChanged()
    {
        m_text.text = Model.GetString("text");
        m_text.color = Model.GetObject<Color>("color", Color.white);
    }
}
