﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FootballScoreView : ViewBase
{
    [SerializeField]
    List<Text> m_scoreText;

    public override void OnModelChanged()
    {
        for (int i = 0; i < m_scoreText.Count; i++)
        {
            m_scoreText[i].text = Model.GetInt("score_" + i).ToString();
        }
    }
}
