﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WinnerHudView : ViewBase
{
    [SerializeField]
    Text m_winnerText;

    string m_winnerRawText;

    void Awake()
    {
        m_winnerRawText = m_winnerText.text;
    }

    public override void OnModelChanged()
    {
        m_winnerText.text =  string.Format(m_winnerRawText, Model.GetInt("winner", 0));

        var color = Model.GetObject<Color>("color", Color.white);
        color.a = 1.0f;
        m_winnerText.color = color;
    }
}
