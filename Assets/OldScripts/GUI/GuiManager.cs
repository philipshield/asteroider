﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GuiManager : MonoBehaviour
{
    [System.Serializable]
    class Entry
    {
        public string key;
        public ViewController prefab;
    }

    [SerializeField]
    Canvas m_mainCanvas;

    [SerializeField]
    List<Entry> m_hudEntries;

    Dictionary<string, ViewController> m_activeHUDs = new Dictionary<string, ViewController>();
    Dictionary<string, Entry> m_huds = new Dictionary<string, Entry>();

    void Awake()
    {
        foreach (var entry in m_hudEntries)
            m_huds.Add(entry.key, entry);
    }

    public ViewController ShowHUD(string _key, Model _model)
    {
        if (!m_huds.ContainsKey(_key))
        {
            Debug.LogError("GuiManager does not contain an entry for: " + _key);
            return null;
        }

        if (m_activeHUDs.ContainsKey(_key))
        {
            Debug.LogError("Trying to show already visible HUD: " + _key);
            return null;
        }

        var prefab = m_huds[_key].prefab;
        var hud = (ViewController)Instantiate(prefab);
        hud.transform.SetParent(m_mainCanvas.transform, false);
        //hud.transform.parent = m_mainCanvas.transform;
        //
        //hud.transform.localScale = Vector3.one;
        //hud.transform.localPosition = Vector3.zero;

        hud.View.Init(_model);
        hud.Controller.Init(_model);

        m_activeHUDs.Add(_key, hud);

        return hud;
    }

    public void HideHUD(string _key)
    {
        if (!m_activeHUDs.ContainsKey(_key))
        {
            Debug.LogError("Trying to hide HUD which are not visible: " + _key);
            return;
        }

        var hud = m_activeHUDs[_key];
        m_activeHUDs.Remove(_key);

        Destroy(hud.gameObject);
    }

    void LateUpdate()
    {
        foreach (var hud in m_activeHUDs)
        {
            hud.Value.View.Model.Notify();
        }
    }
}
