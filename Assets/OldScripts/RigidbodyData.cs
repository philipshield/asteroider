﻿using UnityEngine;
using System.Collections;

public class RigidbodyData : MonoBehaviour 
{
    public Vector2 PrevVelocity { get; private set; }
    public float PrevAngularVelocity { get; private set; }

    private Rigidbody2D m_rigidBody2D;

    void Start () 
    {
        m_rigidBody2D = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate () 
    {
        PrevVelocity = m_rigidBody2D.velocity;
        PrevAngularVelocity = m_rigidBody2D.angularVelocity;
	}
}
