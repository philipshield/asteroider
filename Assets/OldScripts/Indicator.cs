﻿using UnityEngine;
using System.Collections;

public class Indicator : MonoBehaviour {

	private float m_flickerSpeed;
    private float m_maxFlickerSpeed = 0.1f;
    private float m_minFlickerSpeed = 0.02f;
    
	void Start () 
    {
        m_flickerSpeed = 0.1f;
        StartCoroutine(flicker());
	}

    void SetFlickerSpeed(float speed)
    {
        m_flickerSpeed = speed;        
    }

    public void LerpFlickerSpeed(float t)
    {
        m_flickerSpeed = Mathf.Lerp(m_minFlickerSpeed, m_maxFlickerSpeed, t);
    }

    private IEnumerator flicker()
    {
       while(true)
       {
            var sprite = GetComponent<SpriteRenderer>();
            sprite.enabled = !sprite.enabled;

            if (sprite.enabled)
                SoundManager.Instance.PlaySound(SoundEffectType.OutsideScreenIndicatorBlink);
      
            yield return new WaitForSeconds(m_flickerSpeed);
       }
    }
	
}
