﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum AsteroidPrefabType
{
    NotBreakable,
    FullyBreakable,
    PartiallyBreakable
}

public class NewAsteroidField : MonoBehaviour
{
    public class Context
    {
        public IntensitySystem intensiy;
        public SmoothCamera camera;
        public Vector2 center;
        public float radius;
        public Dictionary<AsteroidPrefabType, GameObject> asteroidPrefabs;
    }

    [System.Serializable]
    class AsteroidPrefabEntry
    {
        public AsteroidPrefabType type;
        public GameObject prefab;
    }

    [System.Serializable]
    class Delay
    {
        public IntensitySystem.IntensityLevel intensity;
        public float minDelay;
        public float maxDelay;
    }

    [System.Serializable]
    class EventCollection
    {
        public GameObject asteroidEventsPrefab;
        public List<Delay> delays;
    }

    class ActiveAsteroidEventCollection
    {
        public List<AsteroidEvent> asteroidEventPool;
        public float minDelay;
        public float maxDelay;
    }

    [SerializeField]
    List<AsteroidPrefabEntry> m_asteroidPrefabs;

    [SerializeField]
    float m_radiusFactor = 1.2f;

    [SerializeField]
    float m_startDelay = 0.0f;

    [SerializeField]
    List<EventCollection> m_eventCollections;

    List<ActiveAsteroidEventCollection> m_activeEventCollections = new List<ActiveAsteroidEventCollection>();
    Context m_context;

    void Start()
    {
        SetupContext();
        SetupEvents();
        SetupOtherSystems();

        StartEvents();
    }

    void Update()
    {
        UpdateCenter();
    }

    void UpdateCenter()
    {
        m_context.center = m_context.camera.transform.position;
    }

    void SetupOtherSystems()
    {
        DestroyOutside.Radius = m_context.radius * 2.0f;
    }

    void SetupContext()
    {
        var asteroidPrefabs = new Dictionary<AsteroidPrefabType, GameObject>();
        foreach (var pair in m_asteroidPrefabs)
        {
            asteroidPrefabs.Add(pair.type, pair.prefab);
        }

        var camera = Camera.main;
        Vector2 cameraSize = camera.ViewportToWorldPoint(Vector2.one) - camera.ViewportToWorldPoint(Vector2.zero);

        m_context = new Context()
        {
            intensiy = Systems.Instance.IntensitySystem,
            camera = camera.GetComponent<SmoothCamera>(),
            asteroidPrefabs = asteroidPrefabs,
            center = Vector2.zero,
            radius = cameraSize.magnitude * 0.5f * m_radiusFactor
        };
    }

    void StartEvents()
    {
        foreach (var evt in m_activeEventCollections)
        {
            StartCoroutine(ManageEventCollection(evt));
        }
    }

    void SetupEvents()
    {
        var intensity = Systems.Instance.IntensitySystem.CurrentLevel;

        foreach (var eventCollection in m_eventCollections)
        {
            var asteroidEventsGameObject = (GameObject.Instantiate(eventCollection.asteroidEventsPrefab));
            asteroidEventsGameObject.transform.parent = transform;

            //var allowedFlags = Systems.Instance.GameModeManager.CurrentGameMode.AllowedAsteroidEventCategories; // & with Categories
            var allowedAsteroidEvents = asteroidEventsGameObject.GetComponents<AsteroidEvent>().Where(e => (e.Categories) != 0).ToList();
            var eventProbabilities = asteroidEventsGameObject.GetComponent<ProbabilityWeight>();

            var asteroidEventPool = new List<AsteroidEvent>();

            foreach (var evt in allowedAsteroidEvents)
            {
                int weight = eventProbabilities.GetWeight(evt, intensity);
                for (int i = 0; i < weight; i++)
                {
                    asteroidEventPool.Add(evt);
                }
            }

            if (asteroidEventPool.Count != 0)
            {
                float minDelay = 0.0f;
                float maxDelay = 0.0f;

                var delay = eventCollection.delays.FirstOrDefault(d => d.intensity == intensity);
                if (delay != null)
                {
                    minDelay = delay.minDelay;
                    maxDelay = delay.maxDelay;
                }

                m_activeEventCollections.Add(new ActiveAsteroidEventCollection()
                {
                    asteroidEventPool = asteroidEventPool,
                    minDelay = minDelay,
                    maxDelay = maxDelay
                });
            }
        }
    }

    IEnumerator ManageEventCollection(ActiveAsteroidEventCollection _entry)
    {
        yield return new WaitForSeconds(m_startDelay);

        while (true)
        {
            yield return new WaitForSeconds(Random.Range(_entry.minDelay, _entry.maxDelay));

            var randomEvent = _entry.asteroidEventPool[Random.Range(0, _entry.asteroidEventPool.Count)];
            yield return StartCoroutine(randomEvent.PerformEvent(m_context));
        }
    }
}
