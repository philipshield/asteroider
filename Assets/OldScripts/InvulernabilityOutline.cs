﻿using UnityEngine;
using System.Collections;

public class InvulernabilityOutline : MonoBehaviour 
{
    public float m_blinkFrequency = 0.4f;
        
    MeshRenderer m_meshRenderer;

    bool m_isBlinking = false;
    public bool IsBlinking 
    {
        get { return m_isBlinking; }
        set 
        {
            if (value != m_isBlinking)
            {
                m_isBlinking = value;

                m_meshRenderer.enabled = m_isBlinking;

                if (m_isBlinking)
                {
                    StartCoroutine(BlinkToggle());
                }
            }
        }
    }

	void Awake () 
    {
        var poly = GetComponent<PolyMesh>();
        poly.Generate();
        Destroy(poly);
        GetComponent<PolygonCollider2D>().enabled = false;
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_meshRenderer.enabled = false;
	}

    IEnumerator BlinkToggle()
    {
         yield return new WaitForSeconds(m_blinkFrequency);
         m_meshRenderer.enabled = IsBlinking && !m_meshRenderer.enabled;

         if (IsBlinking)
             StartCoroutine(BlinkToggle());
    }
}
