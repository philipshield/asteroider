﻿using UnityEngine;
using System.Collections;

public class PolyMesh : MonoBehaviour
{
	private Vector3[] m_vertices = new Vector3[0];
	private Vector3[] m_normals = new Vector3[0];
	private Vector2[] m_UVs = new Vector2[0];
	private int[] m_triangles = new int[0];
	private Color[] m_colors = new Color[0];

	[SerializeField]
	private float m_textureScale = 1.0f;

    public float m_outlineThickness = 0.15f;
    public Color m_color = Color.gray;
    public Color m_outlineColor = Color.white;

    public void SetStyleFromOther(PolyMesh other)
    {
        m_outlineThickness = other.m_outlineThickness;
        m_color = other.m_color;
        m_outlineColor = other.m_outlineColor;
        m_textureScale = other.m_textureScale;
    }


	void Start()
	{
		Generate();
	}

	void Update()
	{
		if (GetComponents<PolygonCollider2D>().Length == 0)
		{
            var ship = GetComponent<Spaceship>();
            if (ship != null)
            {
                LastTouched touched = null;
                var shatter = GetComponent<Shatter>();
                if (shatter != null)
                    touched = shatter.GetLatestLastTouched();

                ship.DestroyShip(touched);
            }
            else
                Destroy(this.gameObject);
		}

	}

	public void GenerateNextFrame()
	{
		StartCoroutine(GenerateOnNextFrame());
	}

	private IEnumerator GenerateOnNextFrame()
	{
		yield return null;
		Generate();
	}

	public void Generate()
	{
		var colliders = GetComponents<PolygonCollider2D>();

		int pointCount = 0;
		int triangleCount = 0;

		foreach (var collider in colliders)
		{
			int points = collider.GetPath(0).Length;
			pointCount += points;
			triangleCount += points - 2;
		}

        triangleCount += pointCount * 2;
        pointCount *= 3;

		if (m_vertices.Length < pointCount)
			m_vertices = new Vector3[pointCount];

		if (m_normals.Length < pointCount)
			m_normals = new Vector3[pointCount];

		if (m_UVs.Length < pointCount)
			m_UVs = new Vector2[pointCount];

		if (m_colors.Length < pointCount)
			m_colors = new Color[pointCount ];

		if (m_triangles.Length != 3 * triangleCount)
			m_triangles = new int[triangleCount * 3];

		int i = 0;
		int t = 0;
		foreach (var collider in colliders)
		{
			int si = i;
			var path = collider.GetPath(0);

            var norms = new Vector2[path.Length];
            for (int j = 0; j < path.Length; j++)
            {
                var prev = path[j] - path[(j - 1 + path.Length) % path.Length];
                var next = path[j] - path[(j + 1) % path.Length];
                norms[j] = (prev.normalized + next.normalized).normalized;
            }

            for (int j = 0; j < path.Length; j++)
            {
                var p = path[j];

				m_vertices[i] = p - norms[j] * m_outlineThickness;
				m_UVs[i] = p * m_textureScale;
				m_colors[i] = m_color;
				m_normals[i] = Vector3.back;
				++i;
			}

			Vector2 v = path[1] - path[0];
			Vector2 perp = new Vector2(-v.y, v.x);
			v =  path[2] - path[0];
			float d = Vector2.Dot(perp, v);

			bool flip = d < 0.0f;

			for (int j = si + 1; j < i - 1; ++j)
			{
				m_triangles[t++] = si;
				m_triangles[t++] = j + (flip ? 0 : 1);
				m_triangles[t++] = j + (flip ? 1 : 0);
			}

            si = i;
            for (int j = 0; j < path.Length; j++)
            {
                var p = path[j];

                m_vertices[i] = p - norms[j] * m_outlineThickness;
                m_UVs[i] = p * m_textureScale;
                m_colors[i] = m_outlineColor;
                m_normals[i] = Vector3.back;
                ++i;

                m_vertices[i] = p;
                m_UVs[i] = p * m_textureScale;
                m_colors[i] = m_outlineColor;
                m_normals[i] = Vector3.back;
                ++i;
            }

            int L = i - si;
            for (int j = 0; j < L; j += 2)
            {
                m_triangles[t++] = si + j;
                m_triangles[t++] = si + (j + (flip ? 1 : 3)) % L;
                m_triangles[t++] = si + (j + (flip ? 3 : 1)) % L;

                m_triangles[t++] = si + j;
                m_triangles[t++] = si + (j + (flip ? 3 : 2)) % L;
                m_triangles[t++] = si + (j + (flip ? 2 : 3)) % L;
            }
		}

		Mesh mesh = GetComponent<MeshFilter>().mesh;

		mesh.Clear();
		mesh.vertices = m_vertices;
		mesh.colors = m_colors;
		mesh.normals = m_normals;
		mesh.uv = m_UVs;
		mesh.triangles = m_triangles;
		
	}
}
