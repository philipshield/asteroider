﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Shatter : MonoBehaviour
{
	List<Voronoi2.GraphEdge> m_edges = null;
	List<Voronoi2.GraphEdge> m_fancyEdges = new List<Voronoi2.GraphEdge>();
	Vector3 m_shatterSource;

	public float m_safeTime = 0.1f;

	List<Vector2>[] vertices;
	List<int>[] bodyid;
	List<Vector2> points = new List<Vector2>();

	[SerializeField]
	public GameObject m_polygonPrefab;

	Rigidbody2D m_rigidBody2D;
    RigidbodyData m_rigidBodyData;

	public float  m_momentum = 0.0f;
    float m_density = 0.1f;

	Vector2 m_prevVel = Vector2.zero;
    LastTouched m_latestLastTouched;

    public LastTouched GetLatestLastTouched()
    {
        return m_latestLastTouched;
    }

    public float m_breakVelocity = 10;
	public int breakLimit = 100;

    public float Area = 1.1f;

    public bool m_extraDurable = false;
    public float m_minBreakArea = 1.0f;
    public bool m_isInvulnerable = false;

    public event Action<Shatter> OnCrush;

    public bool IsDangerous { get { return Area > 1.0f; } }

	void Awake()
	{
		m_rigidBody2D = GetComponent<Rigidbody2D>();
        m_rigidBodyData = GetComponent<RigidbodyData>();
	}

    float PolyArea(PolygonCollider2D poly)
    {
        float area = 0.0f;
        Vector2[] path = poly.GetPath(0);
        for (int i = 1; i < path.Length - 1; i++)
        {
            Vector2 v1 = path[i] - path[0];
            Vector2 v2 = path[i + 1] - path[0];
            Vector2 v1n = v1.normalized;

            float l = v1.magnitude;
            float h = Mathf.Abs(Vector2.Dot(new Vector2(-v1n.y, v1n.x), v2));

            area += l * h * 0.5f;
        }

        return area;
    }

    public float SetMassFromDensity(float density)
    {
        float area = 0.0f;

        foreach (var c in GetComponents<PolygonCollider2D>())
        {
            area += PolyArea(c);
        }

        Area = area;
        m_density = density;
        m_rigidBody2D.mass = area * density;

        if (area <= 1.0f)
        {
            var renderer = GetComponent<Renderer>();
            var material = new Material(renderer.material);
            var color = material.GetColor("_TintColor");
            color.r *= 0.9f;
            color.g *= 0.9f;
            color.b *= 0.9f;
            material.SetColor("_TintColor", color);
            renderer.material = material;

            GetComponent<PolyMesh>().m_outlineThickness /= 1.3f; // outline half as thick
        }

        return area;
    }

	void OnCollisionEnter2D(Collision2D collision)
	{
        var otherShatter = collision.rigidbody.GetComponent<Shatter>();
        if (m_extraDurable)
        {
            if (otherShatter == null)
                return;
            if (otherShatter.Area < m_minBreakArea)
                return;
        }

        if (m_isInvulnerable)
        {
            //m_rigidBody2D.AddForce((m_rigidBody2D.position - collision.rigidbody.position).normalized * 100, ForceMode2D.Impulse);
            //m_rigidBody2D.AddTorque(UnityEngine.Random.Range(-25, 25), ForceMode2D.Impulse);
            return;
        }

	    float scale = m_safeTime < 0.0f ? 1 : Mathf.Max(1, 1 + m_safeTime * 20.0f);

        Vector2 oldVelocity = m_rigidBody2D.velocity;
        float oldAngularVelocity = m_rigidBody2D.angularVelocity;

        m_rigidBody2D.velocity = m_rigidBodyData.PrevVelocity;
        m_rigidBody2D.angularVelocity = m_rigidBodyData.PrevAngularVelocity;

        float impulse = 0.0f;

        foreach (var c in collision.contacts)
        {
            Vector2 v1 = m_rigidBody2D.GetPointVelocity(c.point);
            Vector2 v2 = collision.rigidbody.GetPointVelocity(c.point);

            float vm1 = Vector2.Dot(c.normal, v1);
            float vm2 = Vector2.Dot(c.normal, v2);

            float vel = vm2 - vm1;
            impulse += vel * collision.rigidbody.mass / (m_rigidBody2D.mass + collision.rigidbody.mass);
        }

        m_rigidBody2D.velocity = oldVelocity;
        m_rigidBody2D.angularVelocity = oldAngularVelocity;

        var football = collision.gameObject.GetComponent<Football>();
        if (football != null)
        {
            if (collision.relativeVelocity.magnitude > 15.0f)
            {
                impulse = 5000.0f;
                scale *= 0.0f;
            }
        }

        if (impulse > m_breakVelocity * scale || (otherShatter != null && otherShatter.m_isInvulnerable))
		{
            m_latestLastTouched = collision.rigidbody.GetComponent<LastTouched>();

			Crush(transform.worldToLocalMatrix.MultiplyPoint(collision.contacts[0].point));

            if(Area < 2.0f)
            {
                SoundManager.Instance.PlaySound(SoundEffectType.SmallCrack, 1.0f);
            }

            {
                float minarea = 4.0f;
                float maxarea = 14.0f;
                float areat = (Mathf.Clamp(Area, minarea, maxarea) - minarea)/(maxarea - minarea);
                float pitch = Mathf.Lerp(4.0f, 0.3f, areat);
                float volume = Mathf.Lerp(0.1f, 1.1f, areat);

                SoundManager.Instance.PlaySound(SoundEffectType.BigCrack, pitch, volume);
            }
                

		}
	}

	void FixedUpdate()
	{
		m_prevVel = m_rigidBody2D.velocity;
	}

	void Crush(Vector2 localSource)
	{
		m_rigidBody2D.velocity = (m_prevVel * m_momentum + m_rigidBody2D.velocity * (1.0f - m_momentum));

		m_shatterSource = localSource;
		var colliders = GetComponents<PolygonCollider2D>();

		List<Vector2>[] bodyLists = new List<Vector2>[colliders.Length];
		for (int i = 0; i < bodyLists.Length; i++)
		{
			bodyLists[i] = new List<Vector2>();
		}

		Voronoi2.Voronoi v = new Voronoi2.Voronoi(0.01);

		int N = 40;
		double[] x = new double[N];
		double[] y = new double[N];

		float rad = 15;

		points.Clear();
        if (m_extraDurable)
        {
            Quaternion rot = Quaternion.Euler(0, 0, UnityEngine.Random.Range(0,360));
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Vector2 p = new Vector2(i, j) - new Vector2(3.0f, 2.5f);
                    p *= UnityEngine.Random.Range(0.4f, 0.6f);
                    p = rot * (Vector3)p;
                    points.Add( p + UnityEngine.Random.insideUnitCircle * 0.2f);
                    if (points.Count == N)
                        break;
                }
                if (points.Count == N)
                    break;
            }
        }
        else
        {
            for (int i = 0; i < N; i++)
            {
                Vector2 p = UnityEngine.Random.insideUnitCircle * 0.9f;
                float mag = p.magnitude;
                p *= mag * mag * mag * rad;
                points.Add(p);
            }       
        }
		
		points.Sort(new FuncComparer<Vector2>((a, b) => 
		{
			if(a.sqrMagnitude < b.sqrMagnitude)
				return -1;
			else 
				return 1;
		}));

		for (int i = 0; i < N; i++)
		{
			x[i] = (double)points[i].x;
			y[i] = (double)points[i].y;

			points[i] += (Vector2)m_shatterSource;
		}

		float minX = float.MaxValue;
		float maxX = float.MinValue;
		float minY = float.MaxValue;
		float maxY = float.MinValue;

		foreach (var collider in colliders)
		{
			foreach (var p in collider.GetPath(0))
			{
				minX = Mathf.Min(minX, p.x);
				maxX = Mathf.Max(maxX, p.x);
				minY = Mathf.Min(minY, p.y);
				maxY = Mathf.Max(maxY, p.y);
			}
		}

        double f = m_extraDurable ? 3.0 : 1.0;

		m_edges = v.generateVoronoi(x, y,
        (double)-(maxX - minX) * f,
        (double)(maxX - minX) * f,
        (double)-(maxY - minY) * f,
        (double)(maxY - minY) * f);

		/*foreach (var e in m_edges)
		{
			if (e.y1 > e.y2)
			{
				double tmp = e.y1;
				e.y1 = e.y2;
				e.y2 = tmp;
				tmp = e.x1;
				e.x1 = e.x2;
				e.x2 = tmp;
			}
		}

		m_edges.Sort();
		*/
		//int numCells = m_edges.Max(edge => { return Mathf.Max(edge.site1, edge.site2); }) + 1;
		vertices = new List<Vector2>[N];
		bodyid = new List<int>[N];
		for (int i = 0; i < vertices.Length; i++)
		{
			vertices[i] = new List<Vector2>();
			bodyid[i] = new List<int>();
		}

		foreach (var edge in m_edges)
		{
			Vector2 p1 = new Vector2((float)edge.x1, (float)edge.y1) + (Vector2)m_shatterSource;
			Vector2 p2 = new Vector2((float)edge.x2, (float)edge.y2) + (Vector2)m_shatterSource;


			int i = 0;
			foreach(var collider in colliders)
			{
				if (collider.OverlapPoint(transform.localToWorldMatrix.MultiplyPoint(p1)))
				{
					vertices[edge.site1].Add(p1);
					bodyid[edge.site1].Add(i);
					vertices[edge.site2].Add(p1);
					bodyid[edge.site2].Add(i);
				}
				if (collider.OverlapPoint(transform.localToWorldMatrix.MultiplyPoint(p2)))
				{
					vertices[edge.site1].Add(p2);
					bodyid[edge.site1].Add(i);
					vertices[edge.site2].Add(p2);
					bodyid[edge.site2].Add(i);
				}
			
				++i;
			}
		}

		int c = 0;
		foreach (var collider in colliders)
		{
			var path = collider.GetPath(0);

			for (int i = 0; i < path.Length; i++)
			{
				Vector2 p1 = path[i];
				Vector2 p2 = path[(i + 1) % path.Length];

				foreach (var edge in m_edges)
				{
					Vector2 e1 = new Vector2((float)edge.x1, (float)edge.y1) + (Vector2)m_shatterSource;
					Vector2 e2 = new Vector2((float)edge.x2, (float)edge.y2) + (Vector2)m_shatterSource;

					if ((e1 - e2).sqrMagnitude <= 0.00001f)
						continue;

					Vector2 ip;
					try
					{
						ip = LineIntersectionPoint(p1, p2, e1, e2);
					}
					catch (Exception)
					{
						continue;
					}

					if (Vector2.Dot(p1 - p2, ip - p2) > 0.0f && Vector2.Dot(p2 - p1, ip - p1) > 0.0f
					&& Vector2.Dot(e1 - e2, ip - e2) > 0.0f && Vector2.Dot(e2 - e1, ip - e1) > 0.0f)
					{
						vertices[edge.site1].Add(ip);
						bodyid[edge.site1].Add(c);
						vertices[edge.site2].Add(ip);
						bodyid[edge.site2].Add(c);
					}
				}
			}

			for (int i = 0; i < path.Length; i++)
			{
				float minValue = float.MaxValue;
				int minIndex = -1;

				for (int j = 0; j < points.Count; j++)
				{
					float dist = (points[j] - path[i]).sqrMagnitude;
					if (dist < minValue)
					{
						minValue = dist;
						minIndex = j;
					}
				}

				if (minIndex == -1)
					continue;
					
				vertices[minIndex].Add(path[i]);
				bodyid[minIndex].Add(c);
			}

			++c;
		}

		foreach(var col in colliders)
		{
			Destroy(col);
		}

		for (int i = 0; i < vertices.Length; i++)
		{
			var allverts = vertices[i];
			var bi = bodyid[i];

			if (allverts.Count <= 2)
				continue;

			foreach (var bl in bodyLists)
			{
				bl.Clear();
			}

			for (int j = 0; j < allverts.Count; j++)
			{
				bodyLists[bi[j]].Add(allverts[j]);
			}

			GameObject polyobj;

			if (i < breakLimit)
			{
				polyobj = (GameObject)Instantiate(m_polygonPrefab, this.transform.position, this.transform.localRotation);
                polyobj.GetComponent<PolyMesh>().SetStyleFromOther(GetComponent<PolyMesh>());
			}
			else
			{
				polyobj = this.gameObject;
			}

			polyobj.GetComponent<Shatter>().m_polygonPrefab = m_polygonPrefab;
            polyobj.GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().material;

			Vector2 minBounds = new Vector2(float.MaxValue, float.MaxValue);
			Vector2 maxBounds = new Vector2(float.MinValue, float.MinValue);
			foreach (var verts in bodyLists)
			{
				Vector2 center = Vector2.zero;
				foreach (var vertex in verts)
				{
					center += vertex;
					minBounds.x = Mathf.Min(vertex.x, minBounds.x);
					minBounds.y = Mathf.Min(vertex.y, minBounds.y);
					maxBounds.x = Mathf.Max(vertex.x, maxBounds.x);
					maxBounds.y = Mathf.Max(vertex.y, maxBounds.y);
				}
				center /= verts.Count;

				verts.Sort(new FuncComparer<Vector2>((a, b) =>
				{
					Vector2 dir = a - center;
					float av = Mathf.Atan2(dir.y, dir.x);
					dir = b - center;
					float bv = Mathf.Atan2(dir.y, dir.x);

					if (av < bv)
						return -1;
					else
						return 1;
				}));

				Dictionary<int, int> ignore = new Dictionary<int,int>();
				for (int k = 0; k < verts.Count; k++)
				{
					for (int l = k + 1; l < verts.Count; l++)
					{
						if((verts[l] - verts[k]).sqrMagnitude < 0.001f)
						{
							ignore.Add(k,k);
							break;
						}
					}
				}

				int idx = 0;
				Vector2[] path = new Vector2[verts.Count - ignore.Count];
				for (int k = 0; k < verts.Count; k++)
				{
					var vertex = verts[k];
					if(!ignore.ContainsKey(k))
					{
						path[idx] = vertex;
						++idx;
					}
				}

				if(idx < 3)
				{
					continue;
				}

				var poly = polyobj.AddComponent<PolygonCollider2D>();
				poly.SetPath(0, path);
			}

            if (i < breakLimit)
            {
                var body = polyobj.GetComponent<Rigidbody2D>();
                body.velocity = m_rigidBody2D.GetRelativePointVelocity(body.centerOfMass);
                polyobj.GetComponent<Shatter>().SetMassFromDensity(m_density);

                float size = (maxBounds.x - minBounds.x) * (maxBounds.y - minBounds.y);

				if (size < 0.12f)
				{
					Destroy(polyobj);
				}
				else if (size < 1.7f)
				{
					Destroy(polyobj.GetComponent<Shatter>());
					polyobj.AddComponent<ShrinkAndDestroy>().duration = Mathf.Pow(UnityEngine.Random.value, 2f) * 0.6f + 0.3f;
				}
			}
		}

        if (m_extraDurable)
        {
            var polys = GetComponents<PolygonCollider2D>();

            foreach (var poly in polys)
            {
                float area = PolyArea(poly);

                if (area < 0.04f)
                {
                    Destroy(poly);
                }
            }
        }

        if (GetComponents<PolygonCollider2D>().Length == 0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            GetComponent<PolyMesh>().GenerateNextFrame();
            StartCoroutine(CalculateMassNextFrame());
            breakLimit = 100;
        }
		//GetComponent<MeshRenderer>().enabled = false;
		//GetComponent<PolygonCollider2D>().enabled = false;
		//Destroy(m_rigidBody2D);
		//Destroy(this.gameObject);

	}

    IEnumerator CalculateMassNextFrame()
    {
        yield return null;
        GetComponent<Shatter>().SetMassFromDensity(m_density);

        if (OnCrush != null)
            OnCrush(this);
    }

	int targetIndex = 0;

	void Update()
	{
		m_safeTime -= Time.deltaTime;
	}

	Vector2 LineIntersectionPoint(Vector2 ps1, Vector2 pe1, Vector2 ps2, Vector2 pe2)
	{
		// Get A,B,C of first line - points : ps1 to pe1
		float A1 = pe1.y - ps1.y;
		float B1 = ps1.x - pe1.x;
		float C1 = A1 * ps1.x + B1 * ps1.y;

		// Get A,B,C of second line - points : ps2 to pe2
		float A2 = pe2.y - ps2.y;
		float B2 = ps2.x - pe2.x;
		float C2 = A2 * ps2.x + B2 * ps2.y;

		// Get delta and check if the lines are parallel
		float delta = A1 * B2 - A2 * B1;
		if (delta == 0)
			throw new System.Exception("Lines are parallel");

		// now return the Vector2 intersection point
		return new Vector2(
			(B2 * C1 - B1 * C2) / delta,
			(A1 * C2 - A2 * C1) / delta
		);
	}

	public class FuncComparer<T> : IComparer<T>
	{
		private readonly Func<T, T, int> func;
		public FuncComparer(Func<T, T, int> comparerFunc)
		{
			this.func = comparerFunc;
		}

		public int Compare(T x, T y)
		{
			return this.func(x, y);
		}
	}
}
