﻿using UnityEngine;
using System.Collections;

public class BoostOutline : MonoBehaviour
{
    MeshRenderer m_meshRenderer;
    public float m_speed = 3.0f;

    void Awake()
    {
        var poly = GetComponent<PolyMesh>();
        poly.Generate();
        Destroy(poly);
        GetComponent<PolygonCollider2D>().enabled = false;
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_meshRenderer.enabled = true;
    }

    void Update()
    { 
        if(m_meshRenderer.enabled)
        {
            m_meshRenderer.material.mainTextureOffset = new Vector2(Time.time * m_speed, Time.time * m_speed);
        }
    }
}
