﻿using UnityEngine;
using System.Collections;

public class SmoothCamera : MonoBehaviour 
{
    public enum CameraState
    {
        Still,
        Random,
        FollowPlayers,
        ExternalControl,
    }

    public GameManager m_gameManager;
    public CameraState m_state;
    public float m_speed = 1.1f;
    public bool m_overrideControls = false;

    private Vector2 m_desiredVelocity;
    private Camera m_camera;

    private float m_accum;
    private float m_randomChangeTime;
    private const float m_randomMinChange = 5;
    private const float m_randomMaxChange = 20;
    private Vector2 m_velocity;

	void Start () 
    {
        m_velocity = Vector2.zero;
        m_camera = this.GetComponent<Camera>();
        m_accum = 0;
        m_randomChangeTime = UnityEngine.Random.Range(m_randomMinChange, m_randomMaxChange);
	}
	
	void Update () 
    {
        if (!m_overrideControls)
        {
            switch (m_state)
            {
                case CameraState.Still:
                    lerpVelocity(Vector2.zero);
                    break;
                case CameraState.FollowPlayers:
                    if (m_gameManager)
                        steerFollow();
                    break;
                case CameraState.Random:
                    steerRandom();
                    break;
                case CameraState.ExternalControl: //lerp towards desired
                    externalControl();
                    break;
            }
        }
	}

    private void steerFollow()
    {
        Vector3 newpos = transform.position;
        Vector2 avg = m_gameManager.GetPlayerAveragePosition();
        newpos.x = avg.x;
        newpos.y = avg.y;
        m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, newpos, Time.deltaTime * m_speed);
    }

    private void steerRandom()
    {
        if(m_accum > m_randomChangeTime)
        {
            Debug.Log("Change random direction");
            m_desiredVelocity = UnityEngine.Random.insideUnitCircle.normalized * UnityEngine.Random.Range(0.1f, 9.0f);

            //new random time
            m_randomChangeTime = UnityEngine.Random.Range(m_randomMinChange, m_randomMaxChange);
            m_accum = 0;
        }

        lerpVelocity(m_desiredVelocity);
        m_accum += Time.deltaTime;
    }

    private void externalControl()
    {
        lerpVelocity(m_desiredVelocity);
    }

    private void lerpVelocity(Vector2 desired)
    {
        m_velocity = Vector2.Lerp(m_velocity, desired, m_speed * Time.deltaTime);
        m_camera.transform.position = m_camera.transform.position + (Vector3)m_velocity * Time.deltaTime;
    }

    /* 
     * Controls
     */
 
    public void SetState(CameraState state)
    {
        m_state = state;
    }

    public void SetSpeed(float speed)
    {
        m_speed = speed;
    }

    public Vector2 GetVelocity()
    {
        return m_velocity;
    }

    public void SetDesiredVelocity(Vector2 velocity)
    {
        m_desiredVelocity = velocity;
    }

    public Vector2 GetDesiredVelocity()
    {
        return m_desiredVelocity;
    }
}
