﻿using UnityEngine;
using System.Collections;

public class DestroyOutside : MonoBehaviour 
{
    private Camera m_camera;
    public static float Radius 
    {
        set
        {
            s_radiusSq = value * value;
        }
    }

    private static float s_radiusSq;

    Rigidbody2D m_rigidBody2D;
	void Start () 
    {
        m_rigidBody2D = GetComponent<Rigidbody2D>();
        m_camera = Camera.main;

        DestroyManager.Instance.Add(this);
	}
	
    public bool EvaluateDestruction()
    {
        return Vector2.SqrMagnitude(m_rigidBody2D.position - (Vector2)m_camera.transform.position) > s_radiusSq;
    }
}
