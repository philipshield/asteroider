﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

using UnityEngine.UI;

public class SplashStart : MonoBehaviour
{
    public GameObject m_playerPrefab;

    public Text m_helpText;
    private bool m_showingMenu;
    
    private bool[] connected;
    private int nPlayers = 4;
    private int nConnected;

    public string m_level;
    
    public Spaceship m_shipPrefab;
    public List<Transform> m_playerPos;
    public GameObject m_playerIndicatorExplosionPrefab;
    Dictionary<int, GameObject> m_playerShips = new Dictionary<int, GameObject>();
	Dictionary<int, PlayerInput> m_activePlayerInputs = new Dictionary<int, PlayerInput>();

    public List<Color> m_colors = new List<Color>();
    private LinkedList<Color> m_availableColors = new LinkedList<Color>();
    Dictionary<Color, int> m_colorId = new Dictionary<Color, int>();
    Dictionary<int, Color> m_playerColors = new Dictionary<int, Color>();
    public Menu m_menu;
    public Transform m_title;
    public int m_menuOwner = 0;
    Dictionary<int, float> m_playerPitch = new Dictionary<int, float>();

	GameInput m_gameInput;

    public float[] m_zoomLevels;

    void Start()
    {
        Systems.Instance.GameModeManager.Reset();

        nPlayers = m_playerPos.Count;
        var game = Game.Instance;
        connected = new bool[nPlayers];
        nConnected = 0;

        m_showingMenu = false;

        //colors
        foreach (var c in m_colors)
        {
            m_availableColors.AddLast(c);
        }

        if (Game.Instance.m_playerColors == null)
        {
            Game.Instance.m_playerColors = new Dictionary<int, Color>();
        }

		m_gameInput = Systems.Instance.Input;
		
		foreach (var input in m_gameInput.PlayerInputs)
		{
			input.OnJoin 	+= OnJoin;
			input.OnUnjoin 	+= OnUnjoin;
			input.OnStart 	+= OnStart;
            input.OnMenu += OnMenu;
            input.OnChangeColor += OnChangeColor;
            input.OnMenuNavigation += OnMenuNavigation;
        }

        Camera.main.orthographicSize = game.m_zoomState;
       
        m_menu.gameObject.SetActive(m_showingMenu);

        m_menu.OnChange += OnMenuChange;


        for (int i = 0; i < 8; i++)
        {
            m_playerPitch[i] = UnityEngine.Random.Range(0.6f, 1.9f);
        }
    }

    void OnMenuChange()
    {
        var game = Game.Instance;

        SoundManager.Instance.MusicVolume = (float)game.m_musicVolume / 10.0f;

        Camera.main.orthographicSize = game.m_zoomState;
    }

	void OnDestroy()
	{
		foreach (var input in m_gameInput.PlayerInputs)
		{	
			input.OnJoin 	-= OnJoin;
			input.OnUnjoin 	-= OnUnjoin;
			input.OnStart 	-= OnStart;
			input.OnMenu 	-= OnMenu;
            input.OnChangeColor -= OnChangeColor;
            input.OnMenuNavigation -= OnMenuNavigation;
		}
        m_menu.OnChange -= OnMenuChange;

	}
	
	void OnJoin(PlayerInput input)
	{
        if (m_showingMenu)
        {
            if (input.Id != m_menuOwner)
                return;

            m_menu.Input(Menu.MenuAction.Press);
            return;
        }

		if(input.Id < nPlayers)
		{
            if (!connected[input.Id])
            {
                SoundManager.Instance.PlaySound(SoundEffectType.Join, m_playerPitch[input.Id]);
                nConnected++;
                connected[input.Id] = true;
                m_activePlayerInputs[input.Id] = input;
                ShowPlayer(input.Id);

                
            }
            else
            {
                SoundManager.Instance.PlaySound(SoundEffectType.JoinAgain, m_playerPitch[input.Id]);
                var go = (GameObject)Instantiate(m_playerIndicatorExplosionPrefab);
                go.transform.parent = m_playerShips[input.Id].transform;
                go.transform.localPosition = Vector3.zero;
                go.GetComponent<SpriteExplosion>().m_color = m_playerShips[input.Id].GetComponent<MeshRenderer>().material.GetColor("_TintColor");

                
            }
		}
	}

    void OnMenuNavigation(PlayerInput input, PlayerInput.MenuDirection dir)
    {
        if (input.Id != m_menuOwner || !m_showingMenu)
            return;

        Dictionary<PlayerInput.MenuDirection, Menu.MenuAction> map = new Dictionary<PlayerInput.MenuDirection, Menu.MenuAction>()
        {
            { PlayerInput.MenuDirection.Down, Menu.MenuAction.Down },
            { PlayerInput.MenuDirection.Up, Menu.MenuAction.Up },
            { PlayerInput.MenuDirection.Left, Menu.MenuAction.Left },
            { PlayerInput.MenuDirection.Right, Menu.MenuAction.Right }
        };

        m_menu.Input(map[dir]);
    }


	void OnUnjoin(PlayerInput input)
	{
        if (m_showingMenu)
        {
            if (input.Id != m_menuOwner)
                return;

            ShowMenu(false);
            return;
        }

		if(input.Id < nPlayers && connected[input.Id])
		{
            SoundManager.Instance.PlaySound(SoundEffectType.Unjoin, 0.7f);
			nConnected--;
			connected[input.Id] = false;
			m_activePlayerInputs.Remove(input.Id);
			HidePlayer(input.Id);
        }
	}

	void OnStart(PlayerInput input)
	{
        if (m_showingMenu)
        {
            if (input.Id != m_menuOwner)
                return;

            ShowMenu(false);
            return;
        }

		if (nConnected < 1)
		{
			Debug.Log("Too few players");
		}
		else
		{
			doStart();
		}
	}
	
	void OnMenu(PlayerInput input)
	{
        if(!connected[input.Id])
            return;

        if (m_showingMenu && input.Id != m_menuOwner)
            return;

        m_menuOwner = input.Id;

        Color c = m_playerColors[input.Id];
        c.a = 1.0f;
        m_menu.Init(c);

        ShowMenu(!m_showingMenu);
	}

    void ShowMenu(bool show)
    {
        if(show)
        {
            SoundManager.Instance.PlaySound(SoundEffectType.OpenMenu, 2.0f);
        }
        else
        {
            SoundManager.Instance.PlaySound(SoundEffectType.CloseMenu, 1.3f);
        }

        m_showingMenu = show;

        m_menu.gameObject.SetActive(m_showingMenu);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_showingMenu)
        {
            m_title.localPosition = Vector3.Lerp(m_title.localPosition, new Vector3(m_title.localPosition.x, 145, 0), Time.deltaTime * 6.0f);
        }
        else
        {
            m_title.localPosition = Vector3.Lerp(m_title.localPosition, new Vector3(m_title.localPosition.x, 30, 0), Time.deltaTime * 6.0f);
        }

        foreach (var key in m_playerShips.Keys)
        {
            m_playerShips[key].GetComponent<Spaceship>().Aim = m_activePlayerInputs[key].Aim;
            m_playerShips[key].transform.position = (Vector2)m_playerPos[key].transform.position;
        }
    }

    void ShowPlayer(int id)
    {
        if (m_playerShips.ContainsKey(id))
            return;

        var ship = (Spaceship)Instantiate(m_shipPrefab, (Vector2)m_playerPos[id].transform.position, Quaternion.identity);
        m_playerShips.Add(id, ship.gameObject);

        //change to last color, if nobody else took it
        if (Game.Instance.m_playerColors.ContainsKey(id))
        {
            Debug.Log("player already has color");
            Color playercolor = Game.Instance.m_playerColors[id];
            if (m_playerColors.ContainsValue(playercolor))
            {
                changeColor(id);
            }
            else
            {
                m_availableColors.Remove(playercolor);
                setPlayerColor(id, playercolor);
            }
        }
        else
        {
            Debug.Log("new color");
            changeColor(id);
        }

    }

    void HidePlayer(int id)
    {
        if (!m_playerShips.ContainsKey(id))
            return;

        var obj = m_playerShips[id];
        var ship = obj.GetComponent<Spaceship>();

        if (ship != null)
            ship.DestroyShip(null);
        else
            Destroy(obj);

        m_playerShips.Remove(id);
        m_availableColors.AddLast(m_playerColors[id]);
        m_playerColors.Remove(id);
    }

    void OnChangeColor(PlayerInput input)
    {
        if (connected[input.Id])
        {
            SoundManager.Instance.PlaySound(SoundEffectType.ChangeColor, 1.6f + m_playerPitch[input.Id]*0.2f);
            changeColor(input.Id);
        }
    }

    void changeColor(int id)
    {
        //pick new color from others
        Color color = Color.white;
         color = m_availableColors.First.Value;
        m_availableColors.RemoveFirst();


        //remove old one
        if (m_playerColors.ContainsKey(id))
        {
            m_availableColors.AddLast(m_playerColors[id]);
            m_playerColors.Remove(id);
        }

        setPlayerColor(id, color);
    }

    void setPlayerColor(int id, Color color)
    {
        //recolor
        Spaceship ship = m_playerShips[id].GetComponent<Spaceship>();
        var mesh = m_playerShips[id].GetComponent<MeshRenderer>();
        mesh.material = new Material(mesh.material);
        mesh.material.color = color;
        ship.m_aimVisual.GetComponent<MeshRenderer>().material = mesh.material;
        Color tintColor = new Color(color.r * 0.5f, color.g * 0.5f, color.b * 0.5f, 0.5f);
        mesh.material.SetColor("_TintColor", tintColor);
        m_playerColors.Add(id, color);
        Game.Instance.m_playerColors.Remove(id);
        Game.Instance.m_playerColors.Add(id, color);

        ship.SpawnExplosion();
    }

    void doStart()
    {
        Game game = Game.Instance;

        //supress explosions
        foreach (var ship in m_playerShips.Values)
        {
            ship.GetComponent<Spaceship>().SupressExplosion();
        }

        //keep colours
        game.m_playerColors = m_playerColors;

        //create players
        game.players.Clear();
        for(int i = 0, id = 0; i < nPlayers; ++i, ++id)
        {
            if (!connected[i]) continue;
            
            //create controllers
            GameObject player = GameObject.Instantiate(m_playerPrefab) as GameObject;
            var p = player.GetComponent<Player>();
			p.setId(id);
            
            p.m_color = m_playerColors[id];

			p.SetPlayerInput(m_activePlayerInputs[id]);

            player.transform.parent = game.transform;
            player.transform.position = m_playerPos[id].position;

            game.players.Add(id, player.GetComponent<Player>());
        }

        game.m_cameraSize = Camera.main.orthographicSize;

        SoundManager.Instance.PlaySound(SoundEffectType.StartGame, 0.4f);

        var gameMode = Systems.Instance.GameModeManager.PrepareGameMode();
        gameMode.Init(game.players.Values.ToList());

        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        Debug.Log("Loading Game...");
       // yield return new WaitForSeconds(1.0f);

        //do load
        Application.LoadLevel(m_level);
        yield break;
    }


}
