﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour 
{
    public Dictionary<int, Player> players;
    public Dictionary<int, Color> m_playerColors;
    public float m_cameraSize = 25.0f;
    public int m_zoomState = 25;
    public int m_soundVolume = 10;
    public int m_musicVolume = 10;

    public static Game Instance 
    {
        get
        {
            if (s_instance == null)
            {
                GameObject obj = new GameObject();
                obj.name = "Game";
                s_instance = obj.AddComponent<Game>();
                DontDestroyOnLoad(obj);
                
#if USE_PHONE_UI
                var phoneUIPrefab = Resources.Load<GameObject>("PhoneUI");
                var phoneUI = (GameObject)Instantiate(phoneUIPrefab);
                DontDestroyOnLoad(phoneUI);
#endif
            }

            return s_instance;
        }
    }

    private static Game s_instance = null;

    void Awake()
    {
        players = new Dictionary<int, Player>();
    }
}
