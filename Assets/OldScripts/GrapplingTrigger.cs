﻿using UnityEngine;
using System.Collections;
using System;

public class GrapplingTrigger : MonoBehaviour 
{
    public event Action<Collider2D, Vector2> OnHit;

    CircleCollider2D m_collider;

    void Start()
    {
        m_collider = GetComponent<CircleCollider2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var shatter = other.GetComponent<Shatter>();
        if (shatter == null || shatter.Area <= 1.0f)
            return;

        if (other.tag == "Destructible")
        {
            Vector2 dir = (other.transform.position - transform.position);
            float mag = dir.magnitude;
            dir /= mag;

            Vector2 pos = transform.position;
            pos += 1.5f * m_collider.radius * dir;

            if (OnHit != null)
                OnHit(other, pos);
        }
    }
}
