﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;

public class Menu : MonoBehaviour 
{
    public enum MenuAction
    {
        Left, Right, Up, Down, Press
    }

    [Serializable]
    public class Line
    {
        [SerializeField]
        private string m_text;

        [SerializeField]
        List<int> m_availableValues;

        public List<int> AvailableValues { get { return m_availableValues; } }
    
        public string Text
        {
            get
            {
                return string.Format(m_text, Value);
            }
        }

        private string m_value;
        public string Value 
        { 
            get 
            {
                return m_value;
            } 
            set 
            {
                m_value = value;

                if (OnNotify != null)
                    OnNotify();
            } 
        }

        protected event Action OnNotify;
    }

    public class MenuEntry
    {
        int m_current;
        List<int> m_values;
        Action<int> m_updateValueFunc;
        Func<int, int, string> m_valueToString;

        public MenuEntry(int start, List<int> values, Action<int> updateValueFunc, Func<int, int, string> valueToString)
        {
            m_current = 0;
            m_values = values;
            m_updateValueFunc = updateValueFunc;
            m_valueToString = valueToString;

            for (int i = 0; i < values.Count; i++)
            {
                if (m_values[i] == start)
                {
                    m_current = i;
                    break;
                }
            }
        }

        public void PerformAction(MenuAction action)
        {
            if (action == MenuAction.Left)
                --m_current;
            else if (action == MenuAction.Right)
                ++m_current;
            else
                return;

            m_current = (m_current + m_values.Count) % m_values.Count;
            m_updateValueFunc(m_values[m_current]);
        }

        public override string ToString()
        {
            return m_valueToString(m_current, m_values[m_current]);
        }
    }

    public List<Line> m_lines = new List<Line>();
    public List<MenuEntry> m_entries = new List<MenuEntry>();

    private int m_currentLine = 0;
    UnityEngine.UI.Text m_text;

    public void Init(Color color)
    {
        m_text.color = color;
        m_currentLine = 0;
        RefreshText();
    }

    public void Input(MenuAction action)
    {
        if (action == MenuAction.Down)
            ++m_currentLine;
        else if (action == MenuAction.Up)
            --m_currentLine;
        else
        {
            if (m_currentLine != m_entries.Count || action == MenuAction.Press)
            {
                m_entries[m_currentLine].PerformAction(action);
                m_lines[m_currentLine].Value = m_entries[m_currentLine].ToString();
            }
        }

        m_currentLine = (m_currentLine + m_lines.Count) % m_lines.Count;

        if (action == MenuAction.Down || action == MenuAction.Up)
        {
            m_blinkState = true;
            m_blinkTimer = -0.1f;
        }

        SoundManager.Instance.PlaySound(SoundEffectType.MenuClick, 3.0f);

        RefreshText();
    }

    void Awake()
    {
        m_text = GetComponent<UnityEngine.UI.Text>();
    }

    void Start()
    {
        var game = Game.Instance;

        //Hardcoded stuff

        //Game mode
        var gameModeMgr = Systems.Instance.GameModeManager;
        var modes = gameModeMgr.AvailableGameModes;
        int startIndex = modes.IndexOf(gameModeMgr.SelectedGameMode);

        //m_entries.Add(new MenuEntry(startIndex, Enumerable.Range(0, modes.Count).ToList(),
        //(value) =>
        //{
        //    gameModeMgr.SelectGamemode(modes[value]);
        //},
        //(index, value) => { return modes[value].name; }));

        //Intensity
        //var intensitySystem = Systems.Instance.IntensitySystem;
        //var intenistyValues = Enum.GetValues(typeof(IntensitySystem.IntensityLevel)).Cast<int>().ToList();
        //m_entries.Add(new MenuEntry((int)intensitySystem.CurrentLevel, intenistyValues,
        //(value) =>
        //{
        //    intensitySystem.CurrentLevel = (IntensitySystem.IntensityLevel)value;
        //},
        //(index, value) => { return ((IntensitySystem.IntensityLevel)value).ToString(); }));

        //Zoom
        //m_entries.Add(new MenuEntry(game.m_zoomState, m_lines[2].AvailableValues, (value) =>
        //{
        //    Game.Instance.m_zoomState = value;
        //},
        //(index, value) => { return new string[] { "100%", "150%", "50%", "75%" }[index]; }));

        /*   m_entries.Add(new MenuEntry(game.m_musicVolume, m_lines[2].AvailableValues, (value) =>
           {
               Game.Instance.m_musicVolume = value;
           },
           (index, value) => { return (value * 10).ToString() + "%"; }));
           */

        m_entries.Add(new MenuEntry(game.m_soundVolume, m_lines[0].AvailableValues, (value) =>
        {
            Game.Instance.m_soundVolume = value;
        },
        (index, value) => { return (value * 10).ToString() + "%"; }));

        m_entries.Add(new MenuEntry(0, m_lines[1].AvailableValues, (value) =>
        {
            Application.Quit();
        },
        (index, value) => { return ""; }));

        for (int i = 0; i < m_entries.Count; i++)
        {
            m_lines[i].Value = m_entries[i].ToString();
        }

        RefreshText();
    }

    void RefreshText()
    {
        StringBuilder builder = new StringBuilder();

        int i = 0;
        foreach (var line in m_lines)
        {
            if (i == m_currentLine)
            {
                builder.Append(m_blinkState ? "< " : "  ");
                builder.Append(line.Text);
                builder.AppendLine(m_blinkState ? " >" : "  ");
            }
            else
            {
                builder.AppendLine(line.Text);
            }

            ++i;
        }

        m_text.text = builder.ToString();

        if (OnChange != null)
            OnChange();
    }
    public event Action OnChange;

    public float m_blinkInterval = 0.5f;
    float m_blinkTimer = 0.0f;
    bool m_blinkState = false;
    void Update()
    {
        m_blinkTimer += Time.deltaTime;
        if (m_blinkTimer > m_blinkInterval)
        {
            m_blinkTimer -= m_blinkInterval;
            m_blinkState = !m_blinkState;
            RefreshText();
        }
    }
}
