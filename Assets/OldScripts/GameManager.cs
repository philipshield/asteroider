﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
    Game m_game;
    public AsteroidField m_asteroids;
    public Camera m_camera;
    public float CameraSpeed;

    /*
     * Gameflow
     */
    public enum GameMode
    {
        Stock,
        Time
    }
    public GameMode m_mode;

    float m_startTime;
    
    /*
     * Logging
     */
    private Vector2 m_playerAvgPosition;

    void Awake()
    {
        m_game = Game.Instance;

        Camera.main.orthographicSize = m_game.m_cameraSize;
    }

	void Start ()
    {
        m_startTime = Time.time;

        m_mode = GameMode.Stock;
        m_playerAvgPosition = new Vector2(0, 0);

        Debug.Log("Starting game");
        EventManager.SendEvent(new GameStartedEventArgs(m_game.players));
        EventManager.SendEvent(new EventGameStarted());
	}

    IEnumerator SlowmoThing()
    {
        float minTimeScale = 0.2f;
        float t;

        //Fade in
        t = 0;
        while (t < 0.8f)
        {
            t += Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Lerp(1.0f, minTimeScale, t / 0.8f);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return null;
        }

        //Stay
        t = 0;
        while (t < 2.5f)
        {
            t += Time.unscaledDeltaTime;
            yield return null;
        }

        //Fade in
        t = 0;
        while (t < 0.8f)
        {
            t += Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Lerp(minTimeScale, 1.0f, t / 0.8f);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return null;
        }

        Time.timeScale = 1.0f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
/*
    IEnumerator ShowWinnerTextAndGoBackToSplash(int winnerId)
    {
        EventManager.SendEvent(new GameEndedEventArgs(winnerId));
        SoundManager.Instance.PlaySound(SoundEffectType.Victory);

        Color textColor = Color.white;

        if (winnerId == -1)
        {
            int sec = ((int)(Time.time - m_startTime)) % 60;
            int min = ((int)(Time.time - m_startTime)) / 60;
            m_winText.text = String.Format("You survived for{0}{1}", min == 0 ? "" : " " + min + String.Format(" minute{0}", min == 1 ? "" : "s") + " and", " " + sec + String.Format(" second{0}", sec == 1 ? "" : "s"));
            textColor = m_game.players.Values.ToArray()[0].m_color;
        }
        else
        {
            m_winText.text = String.Format(m_winText.text, new string[] { "one", "two", "three", "four", "five", "six", "seven", "eight" }[winnerId]);
            textColor = m_game.players[winnerId].m_color;
        }

        textColor.a = 1.0f;
        m_winText.color = textColor;
        m_winText.enabled = true;

        //show new score before end of round
        yield return new WaitForEndOfFrame();

        yield return StartCoroutine(SlowmoThing());
        yield return new WaitForSeconds(0.3f);

        foreach (var player in m_game.players.Values)
        {
            if (player.Ship != null)
                player.Ship.SupressExplosion();
        }

        Application.LoadLevel(0);
        //Restart();
    }*/
    
    void updatePlayerAvgPosition()
    {
        //average
        Vector2 center = Vector2.zero;
        int playerCount = 0;
        foreach (var player in m_game.players.Values)
        {
            if (player.Alive() && player.Ship != null)
            {
                center += player.GetPosition();
                playerCount++;
            }
        }
        if (playerCount != 0)
        {
            center /= playerCount;
            m_playerAvgPosition = center;
        }
    }

    public Vector2 GetPlayerAveragePosition()
    {
        return m_playerAvgPosition;
    }
   

    void Update() 
    {
        updatePlayerAvgPosition();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            foreach (var player in m_game.players.Values)
            {
                if (player.Ship != null)
                    player.Ship.SupressExplosion();
            }
            Application.LoadLevel(0);
        }
            
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(m_playerAvgPosition, 0.7f);
    }

}
