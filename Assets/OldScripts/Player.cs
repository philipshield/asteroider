﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Player : MonoBehaviour
{
    public Spaceship m_spaceshipPrefab;
    public Color m_color = Color.red;
    public Explosion m_spawnExplosionPrefab;

    Spaceship m_ship = null;
    private bool m_alive = false;
	PlayerInput m_playerInput;
    Vector2 m_lastPosition;

    public float m_spawnInvulnerabilityTime = 2.0f;
    public float m_specialInvulnerabilityTime = 0.25f;


    public int Id { get; private set; }

    public Spaceship Ship { get { return m_ship; } }

    public event Action<Player> OnPlayerDeath;

	public void SetPlayerInput(PlayerInput input)
	{
		m_playerInput = input;
	}

    void Start()
    {
		m_playerInput.OnFireRope += OnFireRope;
		m_playerInput.OnReleaseRope += OnReleaseRope;
        m_playerInput.OnSpecial += OnSpecial;
    }

    void OnDestroy()
	{
		m_playerInput.OnFireRope -= OnFireRope;
		m_playerInput.OnReleaseRope -= OnReleaseRope;
        m_playerInput.OnSpecial -= OnSpecial;

    }

	void OnFireRope(PlayerInput input, Vector2 aim)
	{
		if (m_ship != null)
		{
			if (aim.magnitude < 0.4f)
			{
				if (input.Throttle.magnitude > 0.2f)
					m_ship.FireGrappling(input.Throttle.normalized);
				else
					m_ship.FireGrappling(m_ship.transform.localToWorldMatrix * Vector2.up);
			}
			else
			{
				m_ship.FireGrappling(aim.normalized);
			}
		}
	}

	void OnReleaseRope(PlayerInput input)
	{
		if (m_ship != null)
		{
			m_ship.ReleaseGrappling();
		}
	}

    void OnSpecial(PlayerInput input)
    {
        if (m_ship != null)
        {
            //m_ship.ActivateInvulnerability(m_specialInvulnerabilityTime);   
        }
    }

    void Update()
    {
        if (m_ship != null)
        {
            m_ship.Throttle = m_playerInput.Throttle;
            m_ship.Aim = m_playerInput.Aim;
            m_ship.Boost = m_playerInput.Boost;
            m_lastPosition = m_ship.m_rigidBody2d.position;
        }
    }

    void OnSpaceShipDestroyed(List<int> _lastTouched)
    {
        m_alive = false;
        SoundManager.Instance.PlaySound(SoundEffectType.Explosion);

        if (OnPlayerDeath != null)
            OnPlayerDeath(this);

        EventManager.SendEvent(new EventShipDestroyed(this, m_ship, _lastTouched));
    }

    public Vector2 GetPosition()
    {
        return m_ship == null ? m_lastPosition : (Vector2)m_ship.transform.position;
    }

    public bool Dead()
    {
        return !m_alive;
    }

    public bool Alive()
    {
        return m_alive;
    }

    public void setId(int id)
    {
        this.Id = id;
    }

    public void Spawn(Vector2 origin, float randomRadius, bool firstSpawn)
    {
        if (m_ship == null)
        {
            //TODO: dont spawn inside asteroid
            Vector2 spawnPos = origin + UnityEngine.Random.insideUnitCircle * randomRadius;
            m_ship = (Spaceship)Instantiate(m_spaceshipPrefab, spawnPos, Quaternion.identity);
            m_ship.PlayerId = Id;
            var mesh = m_ship.GetComponent<MeshRenderer>();
            mesh.material = new Material(mesh.material);
            mesh.material.color = m_color;
            Color tintColor = new Color(m_color.r * 0.5f, m_color.g * 0.5f, m_color.b * 0.5f, 0.5f);
            mesh.material.SetColor("_TintColor", tintColor);
            m_ship.m_aimVisual.GetComponent<MeshRenderer>().material = mesh.material;
            m_ship.OnSpaceshipDestroyed += OnSpaceShipDestroyed;
            m_alive = true;
            m_ship.ActivateInvulnerability(m_spawnInvulnerabilityTime);

            m_ship.m_trailRenderer.materials[0].SetColor("_TintColor", tintColor);

            m_ship.GetComponent<BorderDeath>().setColor(m_color);

            if (!firstSpawn)
            {
                var explosion = (Explosion)Instantiate(m_spawnExplosionPrefab, spawnPos, Quaternion.identity);
                explosion.Init(tintColor, Vector2.zero);
            }
        }
    }

}
