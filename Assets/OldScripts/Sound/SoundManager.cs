﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundEffectType 
{
    Join,
    Unjoin,
    BigCrack, 
    SmallCrack, 
    Explosion,
    ChangeColor,
    JoinAgain, 
    OutsideScreenIndicatorBlink,
    GrapplingFired,
    GrapplingHit,
    Victory,
    StartGame,
    MenuClick,
    OpenMenu,
    CloseMenu
}

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour 
{
    public static SoundManager Instance;
    public AudioSource m_source { get; private set;}

    public Dictionary<SoundEffectType, SoundEffectData> m_soundEffects = new Dictionary<SoundEffectType,SoundEffectData>();
    public SoundEffectData[] m_effectsArray;

    float m_musicVolume = 1.0f;
    public float MusicVolume
    {
        get { return m_musicVolume; }
        set { m_musicVolume = value; }
    }

    public void Awake()
    {
        if (Instance == null) Instance = this;
        else if (Instance != this) Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        // audio stuff
        m_source = GetComponent<AudioSource>();
        foreach(SoundEffectData effect in m_effectsArray)
        {
            m_soundEffects.Add(effect.m_type, effect);

        }
    }

    public void PlaySound(SoundEffectType type, float pitch = 1.0f, float volume = 1.0f)
    {
        if(!m_soundEffects.ContainsKey(type))
        {
            //Debug.Log("sound effect not found : " + type.ToString());
            return;
        }

        SoundEffectData effectdata = m_soundEffects[type];
        
        volume *= effectdata.volume;
        m_source.pitch = pitch;
        m_source.PlayOneShot(effectdata.m_clip, volume * (float)Game.Instance.m_soundVolume / 10.0f);
    }

}
