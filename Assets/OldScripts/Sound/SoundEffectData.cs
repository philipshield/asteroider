﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SoundEffectData 
{
    public SoundEffectType m_type;
    public AudioClip m_clip;
    public float volume = 1.0f;
}
