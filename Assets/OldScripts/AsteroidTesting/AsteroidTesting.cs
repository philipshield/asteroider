﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsteroidTesting : MonoBehaviour 
{
    public GameObject m_asteroidPartiallyBreakablePrefab;

    public GameObject m_container;
    public Camera m_camera;

    float m_targetZoom = 25.0f;

	void Start () {
        DestroyOutside.Radius = 100.0f;

	}
	
	void Update () 
    {
        //Asteroids
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            LaunchAsteroid(AsteroidType.Medium, Vector2.one * -30.0f, Vector2.one * 25.0f);
            LaunchAsteroid(AsteroidType.Medium, Vector2.one * 30.0f, -Vector2.one * 25.0f);
        }

        //Camera
        if (Input.GetKeyDown(KeyCode.Q))
        {
            m_targetZoom = 25.0f;
        } 
        if (Input.GetKeyDown(KeyCode.W))
        {
            m_targetZoom = 15.0f;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_targetZoom = 10.0f;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            m_targetZoom = 5.0f;
        }

        //Time
        if (Input.GetKeyDown(KeyCode.A))
        {
            Time.timeScale = 1.0f;
            Time.fixedDeltaTime = 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Time.timeScale = 0.5f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Time.timeScale = 0.25f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Time.timeScale = 0.125f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            Time.timeScale = 0.125f * 0.5f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            Time.timeScale = 0.125f * 0.25f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            Time.timeScale = 0.125f * 0.125f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            Time.timeScale = 0.125f * 0.125f * 0.5f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Time.timeScale = 0.0f;
            Time.fixedDeltaTime = Time.timeScale * 1.0f / 60.0f;
        }

        //Cleanup
        if (Input.GetKeyDown(KeyCode.C))
        {
            var t = m_container.GetComponentsInChildren<Transform>();
            foreach (var item in t)
            {
                if(item.gameObject != m_container)
                    Destroy(item.gameObject);
            }

            var poly = GameObject.FindObjectsOfType<PolyMesh>();
            foreach (var p in poly)
            {
                Destroy(p.gameObject);
            }
        }

        //Update stuff
        m_camera.orthographicSize += (m_targetZoom - m_camera.orthographicSize) * Time.unscaledDeltaTime * 10.0f;
	}

    void LaunchAsteroid(AsteroidType type, Vector2 pos, Vector2 vel, float rot = 0, float angVel = 0)
    {
        GameObject obj = (GameObject)Instantiate(m_asteroidPartiallyBreakablePrefab, pos, Quaternion.Euler(0, 0, rot));
        obj.transform.parent = m_container.transform;

        CreateRandomAsteroid(obj, type);

        obj.GetComponent<Rigidbody2D>().velocity = vel;
        obj.GetComponent<Rigidbody2D>().angularVelocity = angVel;
    }

    enum AsteroidType
    { 
        Small,
        Medium,
        Large
    }

    void createAsteroid(GameObject obj, List<Vector2[]> paths)
    {
        foreach (var vertices in paths)
        {
            var poly = obj.AddComponent<PolygonCollider2D>();
            poly.SetPath(0, vertices);
        }

        obj.GetComponent<Shatter>().SetMassFromDensity(0.2f);
    }

    void CreateRandomAsteroid(GameObject obj, AsteroidType type)
    {
        switch (type)
        {
            case AsteroidType.Medium:
                {
                    Vector2[] path = simplePolygonPath(7, 5.0f);
                    createAsteroid(obj, new List<Vector2[]> { path });
                }
                break;
        }
    }
    Vector2[] simplePolygonPath(int count, float size, float randScale = 0.0f, Vector2? scale = null)
    {
        if (scale == null) 
            scale = Vector2.one;

        Vector2[] path = new Vector2[count];

        float anglerand = randScale *  (((2.0f * Mathf.PI) / count) / 2.0f) * 0.8f;
        for (int i = 0; i < count; i++)
        {
            float angle = 2.0f * Mathf.PI * i / (float)count;
            angle += UnityEngine.Random.Range(-anglerand, anglerand);

            path[i] = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * size;
        }

        for (int i = 0; i < count; i++)
        {
            path[i].Scale(scale.Value);
        }

        return path;
    }
}
