﻿using UnityEngine;
using System.Collections;

public class OpenURL : MonoBehaviour
{
    public string URL = "http://gameawards.se/competition_entries/1142";

    public void GoToURL()
    {
        Application.OpenURL(URL);
    }
}
