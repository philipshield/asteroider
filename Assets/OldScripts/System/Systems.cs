﻿using System;
using UnityEngine;

public class Systems : MonoBehaviour 
{
    public static Systems Instance =>  s_systems ?? throw new Exception("Systems singleton not constructed.");
    private static Systems s_systems;

    void Awake()
    {
        if(s_systems = null)
        {
            Destroy(gameObject);
        }

        s_systems = this;
        DontDestroyOnLoad(gameObject);

        Input = GetComponent<GameInput>();
        GuiManager = GetComponent<GuiManager>();
        GameModeManager = GetComponent<GameModeManager>();
        IntensitySystem = GetComponent<IntensitySystem>();
    }

    public GameInput Input { get; private set; }
    public GuiManager GuiManager { get; private set; }
    public GameModeManager GameModeManager { get; private set; }
    public IntensitySystem IntensitySystem { get; private set; }
}
