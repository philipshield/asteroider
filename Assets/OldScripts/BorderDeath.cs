﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class BorderDeath : MonoBehaviour 
{
    public GameObject m_indicatorPrefab;
    private GameObject m_indicator;
    private Color m_color;

    public float m_deathTime = 1.4f;
    private float m_accum;
    private bool m_visible = true;

    public event Action onDeath;

	void Start () 
    {
        m_accum = 0;
	}

    public void setColor(Color color)
    {
        m_color = color;
        m_color.a = 1.0f;
        m_color *= 1.1f; //little brighter
    }

    void Update()
    {
        Vector2 screenpos = Camera.main.WorldToViewportPoint(transform.position);

        // on-screen bailout
        if (screenpos.x > 0.0f && screenpos.x < 1.0f && screenpos.y > 0.0f && screenpos.y < 1.0f)
        {
            //just entered?
            if (m_indicator != null)
            {
                GameObject.Destroy(m_indicator);
            }

            m_accum = 0;
            m_visible = true;
            return;
        }

        //just went off screen?
        bool wentOffscreen = m_visible;
        m_visible = false;

        //how long outside
        m_accum += Time.deltaTime;
        float t_dead = Mathf.Max(0, m_deathTime - m_accum);

        // find a suitable point near border
        float margin = 15.0f;
        screenpos.x = Mathf.Clamp(screenpos.x, margin/Screen.width, 1.0f-margin/Screen.width);
        screenpos.y = Mathf.Clamp(screenpos.y, margin/Screen.height, 1.0f-margin/Screen.height);

        Vector3 pos = Camera.main.ViewportToWorldPoint(screenpos);
        pos.z = 0;
        Vector2 diff = transform.position - pos;

        float startscale = 3.0f;
        float shrinkdist = 5.0f;
        float shrinkspeed = 0.1f;
        float scalef = startscale;
        if(diff.magnitude > shrinkdist)
            scalef /= (diff.magnitude+shrinkdist)*shrinkspeed;
        Vector2 scale = new Vector2(scalef, scalef);

        float angle = Mathf.Atan2(diff.y, diff.x);
        angle *= 180 / Mathf.PI;
        angle += 90;
        Quaternion rot = Quaternion.Euler(0, 0, angle);

        float howdead = t_dead / m_deathTime;
        float ermagerwd = Mathf.Min(howdead + 0.4f, 1.0f);
        m_color.a = ermagerwd;

        if (wentOffscreen) //spawn a new indicator
        {
            m_indicator = (GameObject)GameObject.Instantiate(m_indicatorPrefab, pos, rot);
            m_indicator.transform.localScale = scale;
            m_indicator.GetComponent<SpriteRenderer>().color = m_color;
            return;
        }

        if(m_indicator != null)
        {
            m_indicator.GetComponent<Indicator>().LerpFlickerSpeed(ermagerwd);
            m_indicator.GetComponent<SpriteRenderer>().color = m_color;
            m_indicator.transform.position = pos;
            m_indicator.transform.rotation = rot;
            m_indicator.transform.localScale = scale;
        }

        //dead?
        if(m_accum >= m_deathTime)
        {
            //spawn explosion at border
            var ship = GetComponent<Spaceship>();
            ship.SupressExplosion();
            Vector2 borderpos = (Vector2)m_indicator.transform.position + diff.normalized*2.0f;
            ship.SpawnExplosion(borderpos, Camera.main.GetComponent<SmoothCamera>().GetVelocity()); 

            GameObject.Destroy(m_indicator);
            if (onDeath != null)
                onDeath();
        }

    }

    void OnDestroy()
    {
        if (m_indicator != null)
        {
            //spawn explosion at border
            var ship = GetComponent<Spaceship>();
            ship.SupressExplosion();
            var p = Camera.main.WorldToViewportPoint(ship.transform.position);
            p.x = Mathf.Clamp01(p.x);
            p.y = Mathf.Clamp01(p.y);
            p = Camera.main.ViewportToWorldPoint(p);

            ship.SpawnExplosion(p, Camera.main.GetComponent<SmoothCamera>().GetVelocity());

            GameObject.Destroy(m_indicator);
        }
    }

        
}
