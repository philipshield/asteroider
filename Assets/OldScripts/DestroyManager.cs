﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyManager : MonoBehaviour 
{
    public static DestroyManager Instance { get; private set; }
    public int m_numObjectsToCheckEveryFrame = 1;

    List<DestroyOutside> m_objects = new List<DestroyOutside>();

    private int m_index = 0;

    void Awake()
    {
        Instance = this;
    }

    public void Add(DestroyOutside obj)
    {
        m_objects.Add(obj);    
    }

    void Update()
    {
        if (m_objects.Count != 0)
        {
            for (int i = 0; i < m_numObjectsToCheckEveryFrame; i++)
            {
                if (m_objects.Count == 0)
                    break;

                m_index = (m_index + 1) % m_objects.Count;

                var obj = m_objects[m_index];
                if (obj == null || obj.EvaluateDestruction())
                {
                    if(obj != null)
                        Destroy(obj.gameObject);

                    m_objects.RemoveAt(m_index);
                }
            }
        }
    }
}
