﻿using FakeItEasy;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Editor.Tests
{
	public class ScreenTests
	{
		IScreenFactory screenFactory;
		ScreenManager screenManager;

		[SetUp]
		public void SetUp()
		{
			screenFactory = A.Fake<IScreenFactory>();
			screenManager = new ScreenManager(screenFactory);
		}

		[Test]
		public void Start_ShouldQuit_WhenQuit()
		{
			IScreen bootstrap = A.Fake<IScreen>();
			A.CallTo(() => bootstrap.Run())
				.Returns(Task.FromResult(
					new ScreenOutcome()
					{
						Target = ScreenType.Menu,
						Transition = ScreenType.Loading
					}));

			IScreen menu = A.Fake<IScreen>();
			A.CallTo(() => menu.Run())
				.Returns(Task.FromResult(
					new ScreenOutcome()
					{
						Quit = true
					}));
			A.CallTo(() => screenFactory.Create(ScreenType.Menu))
				.Returns(menu);

			ScreenOutcome outcome = bootstrap.Run().Result;
			ScreenOutcome nextOutcome = screenManager.Step(outcome).Result;

			nextOutcome.Quit.Should().BeTrue();
		}
	}
}
