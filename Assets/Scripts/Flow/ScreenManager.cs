﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class ScreenManager
{
	IScreenFactory screenFactory;
	IScreen currentScreen;
	Dictionary<ScreenType, IScreen> screens = new Dictionary<ScreenType, IScreen>();

	public ScreenManager(IScreenFactory screenFactory)
	{
		this.screenFactory = screenFactory;
	}

	public async Task Start(ScreenType startScreenType)
	{
		currentScreen = GetOrCreateScreen(startScreenType);
		await currentScreen.Load();
		ScreenOutcome outcome = await currentScreen.Run();

		while (!outcome.Quit)
		{
			outcome = await Step(outcome);
		}
	}

	public async Task<ScreenOutcome> Step(ScreenOutcome outcome)
	{
		IScreen transitionScreen = GetOrCreateScreen(outcome.Transition);

		Task transition = transitionScreen.Load().ContinueWith(load => transitionScreen.Run());
		await Task.WhenAll(currentScreen.Close(), transition);

		await currentScreen.Unload();

		IScreen nextScreen = GetOrCreateScreen(outcome.Target);
		currentScreen = nextScreen;
		await currentScreen.Load();

		return await currentScreen.Run();
	}

	private IScreen GetOrCreateScreen(ScreenType type)
	{
		if (!screens.TryGetValue(type, out IScreen screen))
		{
			screen = screenFactory.Create(type);
			screens.Add(type, screen);
		}

		return screen;
	}
}
