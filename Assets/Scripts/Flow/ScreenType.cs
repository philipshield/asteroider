﻿public enum ScreenType
{
	None,
	Startup,
	Loading,
	Menu,
	Ingame,
	Pause,
	GameOver,
}
