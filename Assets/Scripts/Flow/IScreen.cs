﻿using System.Threading.Tasks;

public interface IScreen
{
	ScreenType Type { get; }
	Task Load();
	Task<ScreenOutcome> Run();
	Task Close();
	Task Unload();
}
