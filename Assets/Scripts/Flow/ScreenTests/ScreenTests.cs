﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class ScreenTests : MonoBehaviour
{    
    async Task Start()
    {
        var screenFactory = new ScreenFactory();
        var screenManager = new ScreenManager(screenFactory);
        await screenManager.Start(ScreenType.Startup);
    }
}
