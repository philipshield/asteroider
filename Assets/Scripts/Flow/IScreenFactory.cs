﻿public interface IScreenFactory
{
	IScreen Create(ScreenType type);
}