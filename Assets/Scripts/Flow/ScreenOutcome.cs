﻿public class ScreenOutcome
{
	public ScreenType Target { get; set; }
	public ScreenType Transition { get; set; } = ScreenType.None;
	public bool Quit { get; set; }
}
