﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class LoadingScreen : IScreen
{
    public ScreenType Type => ScreenType.Loading;

    public async Task Load()
    {
        Debug.Log("Loading LoadingScreen");
        await Task.Delay(1000);
    }

    public async Task<ScreenOutcome> Run()
    {
        Debug.Log("Showing some cool LoadingScreen");
        await Task.Delay(1000);
        return null;
    }

    public async Task Close()
    {
        Debug.Log("Closing LoadingScreen");
        await Task.Delay(1000);
    }

    public async Task Unload()
    {
        Debug.Log("Unloading LoadingScreen");
        await Task.Delay(1000);
    }

}
