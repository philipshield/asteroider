﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFactory : IScreenFactory
{
    public IScreen Create(ScreenType type)
    {
        switch (type)
        {
            case ScreenType.None:
                return new NullTransitionScreen();
            case ScreenType.Startup:
                return new StartupScreen();
            case ScreenType.Loading:
                return new LoadingScreen();
            case ScreenType.Menu:
                return new MenuScreen();
            case ScreenType.Ingame:
                return new IngameScreen();
            case ScreenType.Pause:
            case ScreenType.GameOver:
            default:
                throw new System.Exception($"Factory can't construct screen of type {type}");
        }
    }
}
