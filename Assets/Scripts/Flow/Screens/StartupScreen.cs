﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupScreen : IScreen
{
    public ScreenType Type => ScreenType.Startup;

    public Task Load()
    {
        Debug.Log($"{GetType().Name}: Load");
        return Task.CompletedTask;
    }

    public async Task<ScreenOutcome> Run()
    {
        Debug.Log($"{GetType().Name}: Run");

        await SceneManager.LoadSceneAsync("StartupScene");

        await Task.Delay(2000);
        return new ScreenOutcome()
        {
            Target = ScreenType.Menu,
        };
    }

    public async Task Close()
    {
        Debug.Log("Closing StartupScreen");
        await Task.Delay(2000);
    }

    public async Task Unload()
    {
        Debug.Log("Unloading StartupScreen");
        await Task.Delay(1000);
    }
}