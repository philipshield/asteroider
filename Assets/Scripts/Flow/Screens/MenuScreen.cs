﻿using FluentAssertions;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScreen : IScreen
{
    public ScreenType Type => ScreenType.Menu;

    AsyncOperation menuLoad;

    public async Task Load()
    {
        Debug.Log("Loading MenuScreen");

        menuLoad = SceneManager.LoadSceneAsync("MenuScene");
        menuLoad.allowSceneActivation = false;

        await Task.Delay(10000);
    }

    public async Task<ScreenOutcome> Run()
    {
        Debug.Log("Showing Menu");
        menuLoad.allowSceneActivation = true;

        // TODO: litsen for screen change events in scene

        await Task.Delay(10000);
        return new ScreenOutcome()
        {
            Quit = true
        };
    }   

    public async Task Close()
    {
        Debug.Log("Closing StartupScreen");
        await Task.Delay(1000);
    }

    public async Task Unload()
    {
        Debug.Log("Unloading StartupScreen");
        await Task.Delay(1000);
    }

}
