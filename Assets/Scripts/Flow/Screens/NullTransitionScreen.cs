﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class NullTransitionScreen : IScreen
{
    public ScreenType Type => ScreenType.None;

    public Task Close()
    {
        return Task.CompletedTask;
    }

    public Task Load()
    {
        return Task.CompletedTask;
    }

    public Task<ScreenOutcome> Run()
    {
        return Task.FromResult<ScreenOutcome>(null);
    }

    public Task Unload()
    {
        return Task.CompletedTask;
    }
}
