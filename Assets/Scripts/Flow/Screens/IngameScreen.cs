﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class IngameScreen : IScreen
{
    public ScreenType Type => ScreenType.Ingame;

    public async Task Load()
    {
        Debug.Log("Loading IngameScreen");
        await Task.Delay(1000);
    }

    public async Task<ScreenOutcome> Run()
    {
        Debug.Log("Showing some cool IngameScreen");
        await Task.Delay(1000);
        return new ScreenOutcome()
        {
            Target = ScreenType.Menu,
        };
    }

    public async Task Close()
    {
        Debug.Log("Closing IngameScreen");
        await Task.Delay(1000);
    }

    public async Task Unload()
    {
        Debug.Log("Unloading IngameScreen");
        await Task.Delay(1000);
    }

}
